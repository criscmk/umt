package com.umt.partyapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionResultsResponseDto;

import java.util.ArrayList;
import java.util.List;

public class ElectionResultsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ElectionResultsResponseDto> electionResults;
    private Context context;
    private Integer totalVotes = 0;

    public ElectionResultsAdapter(Context context) {
        this.context = context;
        electionResults = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_election_candidate_tally, viewGroup, false);
        viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        ElectionResultsResponseDto tally = electionResults.get(i);

        final MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.candidateName.setText(tally.getCandidateName());

        String votes = tally.getVoteCount() + " votes";
        myViewHolder.tvTally.setText(votes);

        int voteTally = tally.getVoteCount();
        int candidateVotesPercentage = (int)((voteTally * 100.0f) / totalVotes);

        myViewHolder.electionTallyPb.setProgress(candidateVotesPercentage);
    }

    public void add(ElectionResultsResponseDto r) {
        electionResults.add(r);
        notifyItemInserted(electionResults.size() - 1);

        int voteCount = r.getVoteCount();
        totalVotes = totalVotes + voteCount;
    }

    public void addAll(List<ElectionResultsResponseDto> results) {
        for (ElectionResultsResponseDto electionResult : results) {
            add(electionResult);

        }
    }

    @Override
    public int getItemCount() {
        Log.d("result", "size " + electionResults.size());
        return electionResults.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView candidateName, tvTally;
        private ProgressBar electionTallyPb;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            candidateName = itemView.findViewById(R.id.candidate);
            tvTally = itemView.findViewById(R.id.tv_tally);
            electionTallyPb = itemView.findViewById(R.id.progress_election_tally);

        }
    }
}
