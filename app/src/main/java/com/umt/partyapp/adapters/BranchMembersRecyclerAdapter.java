package com.umt.partyapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.BranchMemberDao;
import com.umt.partyapp.model.GroupMemberDto;
import com.umt.partyapp.util.PaginationAdapterCallback;

import java.util.ArrayList;
import java.util.List;

public class BranchMembersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BranchMemberDao> membersArrayList;
    private Context context;

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    public BranchMembersRecyclerAdapter(Context context) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        membersArrayList = new ArrayList<>();
    }

    public List<BranchMemberDao> getMembers() {
        return membersArrayList;
    }

    public void setBranchMembers(List<BranchMemberDao> membersArrayList) {
        this.membersArrayList = membersArrayList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_member_list, viewGroup, false);
        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.group_member_list, viewGroup, false);
                viewHolder = new MembersVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        BranchMemberDao member = membersArrayList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final MembersVH membersVH = (MembersVH) viewHolder;

                membersVH.name.setText(member.getFullName());
                membersVH.dateJoined.setText(member.getDateJoined());

                boolean admin = member.getAdmin();
                Log.i("Member", String.valueOf(admin));
                if (!admin) {
                    Log.i("admin", String.valueOf(admin));
                    membersVH.role.setVisibility(View.GONE);
                }
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) viewHolder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;

        }
    }

    @Override
    public int getItemCount() {

        return membersArrayList == null ? 0 : membersArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == membersArrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(membersArrayList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    public void add(BranchMemberDao r) {
        membersArrayList.add(r);
        notifyItemInserted(membersArrayList.size() - 1);
    }

    public void addAll(List<BranchMemberDao> membersArrayList) {
        for (BranchMemberDao memberDao : membersArrayList) {
            add(memberDao);
        }
    }

    public void remove(BranchMemberDao r) {
        int position = membersArrayList.indexOf(r);
        if (position > -1) {
            membersArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new BranchMemberDao());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = membersArrayList.size() - 1;
        BranchMemberDao result = getItem(position);

        if (result != null) {
            membersArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public BranchMemberDao getItem(int position) {
        return membersArrayList.get(position);
    }

    public class MembersVH extends RecyclerView.ViewHolder {
        TextView name;
        TextView dateJoined;
        TextView role;

        public MembersVH(View view) {
            super(view);

            name = view.findViewById(R.id.title);
            dateJoined = view.findViewById(R.id.date);
            role = view.findViewById(R.id.role);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }
}
