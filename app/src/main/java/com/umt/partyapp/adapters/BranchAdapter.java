package com.umt.partyapp.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.BranchInformationActivity;
import com.umt.partyapp.activity.ListBranchActivity;
import com.umt.partyapp.apiresponse.BranchDataDto;

import com.umt.partyapp.util.PaginationAdapterCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BranchAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BranchDataDto> branchList;
    private List<BranchDataDto> branchListFiltered;
    private Context context;
    public static Long branchId = null;
    JSONObject jsonObject;
    SweetAlertDialog pDialog;
    ListBranchActivity listBranchActivity;

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    public  BranchAdapter(Context context){
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        branchList = new ArrayList<>();


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_member_list, viewGroup, false);
        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.adapter_list_branch, viewGroup, false);
                viewHolder = new BranchAdapter.MyViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new BranchAdapter.LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {

        BranchDataDto branch = branchList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final BranchAdapter.MyViewHolder myViewHolder = (BranchAdapter.MyViewHolder) viewHolder;
                Log.d("TAG", branch.getName().toString());
                myViewHolder.name.setText(branch.getName());
                boolean isExpanded = branch.isExpanded();
                myViewHolder.linearLayoutexpandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                myViewHolder.image1.setVisibility(isExpanded ? View.GONE: View.VISIBLE );
                myViewHolder.image2.setVisibility(isExpanded ? View.VISIBLE: View.GONE );
                myViewHolder.description.setText(branch.getDescription());
                myViewHolder.buttonJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listBranchActivity.customeDailog("","Are you sure You want to join peace branch",2);
                 listBranchActivity.BranchId =  branchListFiltered.get(position).getId();
                            }
                        });

                break;
            case LOADING:
                BranchAdapter.LoadingVH loadingVH = (BranchAdapter.LoadingVH) viewHolder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;

        }
    }

//    @Override
//    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView")  int position) {
//        holder.name.setText(branchListFiltered.get(position).getName());
//        boolean isExpanded = branchList.get(position).isExpanded();
//        holder.linearLayoutexpandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
//        holder.image1.setVisibility(isExpanded ? View.GONE: View.VISIBLE );
//        holder.image2.setVisibility(isExpanded ? View.VISIBLE: View.GONE );
//        holder.description.setText(branchListFiltered.get(position).getDescription());
//        //join group
//        holder.buttonJoin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listBranchActivity.customeDailog("","Are you sure You want to join peace branch",2);
//                listBranchActivity.BranchId =  branchListFiltered.get(position).getId();
//
//                            }
//                        });
//    }

//    @Override
//    public int getItemCount() {
//
//        if(branchList != null){
//            return branchListFiltered.size();
//        } else {
//            return 0;
//        }
//    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    branchListFiltered = branchList;
//                } else {
//                    List<BranchDataDto> filteredList = new ArrayList<>();
//                    for (BranchDataDto branch : branchList) {
//                        if (branch.getName().toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(branch);
//                        }
//                    }
//                    branchListFiltered = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = branchListFiltered;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                branchListFiltered = (ArrayList<BranchDataDto>) filterResults.values;
//
//                notifyDataSetChanged();
//            }
//        };
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView description;
        LinearLayout linearLayout,linearLayoutexpandableLayout;
        TableLayout tableLayout;
        ImageView image1,image2;
        Button buttonJoin,buttonGroupDetails;

        public MyViewHolder(View view) {
            super(view);
            name =  view.findViewById(R.id.list_branch_name);
            description =  view.findViewById(R.id.list_branch_description);
            linearLayout = (LinearLayout) view.findViewById(R.id.branch_layout);
            image1 = view.findViewById(R.id.toogle_down_image);
            image2 = view.findViewById(R.id.toogle_up_image);
            buttonGroupDetails = view.findViewById(R.id.branch_details);
            buttonJoin = view.findViewById(R.id.list_join_branch);
            //view branch details
            buttonGroupDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BranchInformationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
            linearLayoutexpandableLayout = view.findViewById(R.id.expandableLayout);
            //expand layout
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BranchDataDto branch = branchList.get(getAdapterPosition());
                    branch.setExpanded(!branch.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
            //expand by image
            image1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BranchDataDto branch = branchList.get(getAdapterPosition());
                    branch.setExpanded(!branch.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
            image2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BranchDataDto branch = branchList.get(getAdapterPosition());
                    branch.setExpanded(!branch.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
            //image = (ImageView)view.findViewById(R.id.image);
        }
    }

    public void customeDailog(String header, String message, int option) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = listBranchActivity.findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        View image = dialogView.findViewById(R.id.dialog_image);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        Button accept = dialogView.findViewById(R.id.btn_accept);
        accept.setText("Join");
        //display one button
        if(option==1)
        {
            okBtn.setVisibility(View.VISIBLE);
        }
        else
        {
            accept.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }
        //TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);
        //alertHeaderText.setText(header);
        alertMessage.setText(message);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });




    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

    @Override
    public int getItemCount() {

        return branchList == null ? 0 : branchList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == branchList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(branchList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    public void add(BranchDataDto r) {
        branchList.add(r);
        notifyItemInserted(branchList.size() - 1);
    }

    public void addAll(List<BranchDataDto> branchList) {
        for (BranchDataDto branchDataDto : branchList) {
            add(branchDataDto);
        }
    }

    public void remove(BranchDataDto r) {
        int position = branchList.indexOf(r);
        if (position > -1) {
            branchList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new BranchDataDto());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = branchList.size() - 1;
        BranchDataDto result = getItem(position);

        if (result != null) {
            branchList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public BranchDataDto getItem(int position) {
        return branchList.get(position);
    }



    private void parseResponse(String response,View v) throws JSONException {

        jsonObject = new JSONObject(response);
        Boolean status = Boolean.valueOf(jsonObject.getString("status"));

        if (!status) {
            new SweetAlertDialog(v.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Alert!")
                    .setContentText(jsonObject.getString("message"))
                    .show();

        } else if (status) {
            new SweetAlertDialog(v.getContext(), SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Success!")
                    .setContentText(jsonObject.getString("message"))
                    .show();


        }
    }




    }

