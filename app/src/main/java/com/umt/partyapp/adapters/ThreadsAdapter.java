package com.umt.partyapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.ChatActivity;
import com.umt.partyapp.activity.MessageThreadActivity;
import com.umt.partyapp.model.MessageDto;

import java.util.ArrayList;
import java.util.List;

public class ThreadsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private final int CHAT_ME = 100;
    private final int CHAT_YOU = 200;
    ChatActivity chatActivity;
    MessageThreadActivity messageThreadActivity;

    private List<MessageDto> items = new ArrayList<>();

    private Context ctx;
    private ThreadsAdapter.OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, MessageDto obj, int position);
    }

    public void setOnItemClickListener(final ThreadsAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ThreadsAdapter(Context context) {
        ctx = context;

    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView text_content;
        public TextView text_time;
        public TextView user_name;
        public View lyt_parent;
        public ImageView profile_image;
        public CardView cardView;



        public ItemViewHolder(View v) {
            super(v);
            text_content = v.findViewById(R.id.text_content);
            text_time = v.findViewById(R.id.text_time);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            user_name = v.findViewById(R.id.text_user_name);
            profile_image = v.findViewById(R.id.sender_profile_image);
            cardView = v.findViewById(R.id.card_chat_me);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == CHAT_ME) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_thread_me, parent, false);
            vh = new ThreadsAdapter.ItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_thread_you, parent, false);
            vh = new ThreadsAdapter.ItemViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof ThreadsAdapter.ItemViewHolder) {
            final MessageDto m = items.get(position);
            ThreadsAdapter.ItemViewHolder vItem = (ThreadsAdapter.ItemViewHolder) holder;
            vItem.text_content.setText(m.getContent());
            Log.d("username",m.getUsername());
            if(!m.getUsername().equals("umt-admin"))
            {
//                vItem.user_name.setText(m.getUsername());
            }

            vItem.text_time.setText(m.getDate());
            vItem.cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(ctx, m.getContent(), Toast.LENGTH_SHORT).show();
                    chatActivity. replyMessage = m.getContent();
                    chatActivity.replyUsername =m.getUsername();
                    chatActivity.bottomSheet();
                    return true;
                }
            });


            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, m, position);




                    }
                }
            });
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).isFromMe() ? CHAT_ME : CHAT_YOU;
    }

    public void insertItem(MessageDto item) {
        this.items.add(item);
        notifyItemInserted(getItemCount());
    }

    public void setItems(List<MessageDto> items) {
        this.items = items;
    }
}