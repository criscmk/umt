package com.umt.partyapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.ChatActivity;
import com.umt.partyapp.activity.ChatForumsActivity;
import com.umt.partyapp.activity.HomeActivity;
import com.umt.partyapp.activity.ListBranchActivity;
import com.umt.partyapp.apiresponse.BranchDataDto;
import com.umt.partyapp.model.ForumsDto;

import java.util.List;

public class ForumsAdapter extends RecyclerView.Adapter<ForumsAdapter.MyViewHolder> {
    Context context;
    List<ForumsDto> forumsList;
    ChatForumsActivity chatForumsActivity;


    public ForumsAdapter(Context context, List<ForumsDto> forumsList) {
        this.context = context;
        this.forumsList = forumsList;
    }


    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.forums_layout,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name.setText(forumsList.get(position).getName());
        holder.description.setText(forumsList.get(position).getDescription());
        holder.time.setText(forumsList.get(position).getTime());
        holder.counter.setText(forumsList.get(position).getCounter());
        boolean isExpanded = forumsList.get(position).isExpanded();
        holder.hiddenLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.imageView1.setVisibility(isExpanded ? View.GONE: View.VISIBLE);
        holder.imageView2.setVisibility(isExpanded ? View.VISIBLE: View.GONE);
        holder.view_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ChatActivity.class));
            }
        });


    }

    @Override
    public int getItemCount() {
        return forumsList.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView name;
        private TextView description;
        private TextView time;
        private TextView counter;
        private Button  view_chat;
        private ImageView imageView1,imageView2;
        private LinearLayout linearLayout,hiddenLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name =(TextView)  itemView.findViewById(R.id.forum_name);
            description =(TextView)  itemView.findViewById(R.id.forum_description);
            time =(TextView)  itemView.findViewById(R.id.forum_time);
            counter =(TextView)  itemView.findViewById(R.id.forum_counter);
            imageView1 =(ImageView)  itemView.findViewById(R.id.forum_view);
            imageView2 =(ImageView)  itemView.findViewById(R.id.forum_checked);
            linearLayout =(LinearLayout) itemView.findViewById(R.id.forum_visible_layout);
            hiddenLayout =(LinearLayout) itemView.findViewById(R.id.forum_join_exit);
            view_chat = (Button) itemView.findViewById(R.id.view_chat);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ForumsDto  forum = forumsList.get(getAdapterPosition());
                    forum.setExpanded(!forum.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }

}
