package com.umt.partyapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.support.v7.widget.RecyclerView;
import android.widget.Toast;


import com.umt.partyapp.R;
import com.umt.partyapp.activity.ChatActivity;
import com.umt.partyapp.activity.MessageThreadActivity;
import com.umt.partyapp.model.MessageDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdapterChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String MESSAGE = "com.umt.partyapp.adapters.MESSAGE";
    public static final String USERNAME = "com.umt.partyapp.adapters.USERNAME";

    private final int CHAT_ME = 100;
    private final int CHAT_YOU = 200;
    ChatActivity chatActivity;
    MessageThreadActivity messageThreadActivity;

    private List<MessageDto> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, MessageDto obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterChat(Context context) {
        ctx = context;
        chatActivity = (ChatActivity) ctx;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == CHAT_ME) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_me, parent, false);
            vh = new ItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_you, parent, false);
            vh = new ItemViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof ItemViewHolder) {
            final MessageDto m = items.get(position);
            ItemViewHolder vItem = (ItemViewHolder) holder;
            vItem.text_content.setText(m.getContent());
            Log.d("username", m.getUsername());

            int color;
            switch (m.getUsername()) {
                case "umt-admin":
                    color = chatActivity.getResources().getColor(R.color.red_600);
                    break;
                case "denis.bii":
                    color = chatActivity.getResources().getColor(R.color.colorAccent);
                    break;
                case "pmacharia":
                    color = chatActivity.getResources().getColor(R.color.light_blue_500);
                    break;
                default:
                    color = chatActivity.getResources().getColor(R.color.amber_500);
                    break;
            }
            if (!m.getUsername().equals("umt-admin")) {
                vItem.user_name.setText(m.getUsername());
                vItem.user_name.setTextColor(color);
            }


            vItem.text_time.setText(m.getDate());

            Random random = new Random();

            vItem.message_likes.setText(String.valueOf(random.nextInt(10) + 1));
            vItem.message_reactions.setText(String.valueOf(random.nextInt(10) + 1));

            vItem.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    chatActivity.replyMessage = m.getContent();
                    chatActivity.replyUsername = m.getUsername();
                    chatActivity.mainMessageId = m.getMessageId();
                    chatActivity.bottomSheet();
                    Toast.makeText(chatActivity, "Message Id" + m.getMessageId(), Toast.LENGTH_SHORT).show();

                    return true;
                }
            });


            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, m, position);


                    }
                }
            });

            vItem.message_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, MessageThreadActivity.class);
                    intent.putExtra(MESSAGE, m.getContent());
                    intent.putExtra(USERNAME, m.getUsername());
                    ctx.startActivity(intent);
                }
            });

        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).isFromMe() ? CHAT_ME : CHAT_YOU;
    }

    public void insertItem(MessageDto item) {
        this.items.add(item);
        notifyItemInserted(getItemCount());
    }

    public void setItems(List<MessageDto> items) {
        this.items = items;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView text_content, message_likes, message_reactions;
        public TextView text_time;
        public TextView user_name;
        public View lyt_parent;
        public ImageView profile_image;
        public CardView cardView;
        public LinearLayout message_layout;


        public ItemViewHolder(View v) {
            super(v);
            text_content = v.findViewById(R.id.text_content);
            text_time = v.findViewById(R.id.text_time);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            user_name = v.findViewById(R.id.text_user_name);
            profile_image = v.findViewById(R.id.sender_profile_image);
            cardView = v.findViewById(R.id.card_chat_me);
            message_layout = v.findViewById(R.id.messageThread);
            message_likes = v.findViewById(R.id.message_likes);
            message_reactions = v.findViewById(R.id.message_reactions);


//            v.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    chatActivity.replyMessage = m.getContent();
//                    chatActivity.replyUsername = m.getUsername();
//                    chatActivity.mainMessageId = m.getMessageId();
//                    chatActivity.bottomSheet();
//                    Toast.makeText(chatActivity, "Message Id" + m.getMessageId(), Toast.LENGTH_SHORT).show();
//
//                    return true;
//                }
//            });

        }


    }
}