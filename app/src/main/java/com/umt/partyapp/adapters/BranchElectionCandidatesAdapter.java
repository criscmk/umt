package com.umt.partyapp.adapters;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.BranchElectionCandidateDto;
import com.umt.partyapp.model.VoteDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class BranchElectionCandidatesAdapter extends RecyclerView.Adapter<BranchElectionCandidatesAdapter.MyViewHolder> {
    List<BranchElectionCandidateDto> candidates;
    Context context;
    Boolean votingOpen;
    Long electionId;
    Boolean hasVoted = false;

    Fragment fragment;
    MyViewHolder myViewHolder;

    public BranchElectionCandidatesAdapter(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
        candidates = new ArrayList<>();
        votingOpen = false;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_election_candidate, viewGroup, false);
        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        viewHolder.candidateName.setText(candidates.get(i).getCandidateName());
        Long candidateId = candidates.get(i).getId();
        String candidateName = candidates.get(i).getCandidateName();


        viewHolder.btnVote.setOnClickListener(v -> {
            if (!hasVoted) {
                viewHolder.progressBar.setVisibility(View.VISIBLE);
                viewHolder.btnVote.setVisibility(View.GONE);
                new Handler().postDelayed(() -> {
                    viewHolder.progressBar.setVisibility(View.GONE);
                    confirmDialog("CONFIRM CANDIDATE", candidateId, candidateName);
                }, 4000);
            } else {
                errorAlert("ATTENTION VOTER", "Dear voter, you can only vote once");
            }
        });



    }

    public void add(BranchElectionCandidateDto r) {
        candidates.add(r);
        notifyItemInserted(candidates.size() - 1);
    }

    public void addAll(List<BranchElectionCandidateDto> candidatesList, Boolean votingStatus, Long election) {
        for (BranchElectionCandidateDto candidateDto : candidatesList) {
            add(candidateDto);
        }
        votingOpen = votingStatus;
        electionId = election;

        Log.d("TAG", "addAll: " + votingOpen + electionId);
    }


    @Override
    public int getItemCount() {
        return candidates.size();
    }


    public void vote(Long candidateId, String candidateName) {

        VoteDto vote = new VoteDto(electionId, candidateId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(context).getApi().vote(vote);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONObject responseData = jsonObject.getJSONObject("data");

                            successAlert("SUCCESS!!!", candidateName);

                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void successAlert(String title, String candidate) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(Html.fromHtml(context.getString(R.string.vote_success_message, candidate)));
        builder.setPositiveButton("Ok", (dialogInterface, i) -> {
//            ((CoordinatorElectionCandidatesFragment)fragment).nextFragment();
            hasVoted = true;

        });

        builder.show();
    }

    public void errorAlert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> {

        });

        builder.show();
    }

    public void confirmDialog(String title, Long candidateId, String candidateName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(Html.fromHtml(context.getString(R.string.vote_confirmation_message, candidateName)));
        builder.setPositiveButton("Vote", (dialogInterface, i) -> {
            successAlert("SUCCESS!!!", candidateName);
        });

        builder.setNegativeButton("Cancel", (dialogInterface, i) -> {

        });

        builder.show();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView candidateName;
        private Button btnVote;
        private ProgressBar progressBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            candidateName = itemView.findViewById(R.id.candidate_name);
            btnVote = itemView.findViewById(R.id.btnVote1);
            progressBar = itemView.findViewById(R.id.progress_bar_vote);

            if (votingOpen && !hasVoted) {
                btnVote.setVisibility(View.VISIBLE);
            }
        }

    }
    public void vote(Long candidateId, String candidateName) {
        myViewHolder.progressBar.setVisibility(View.VISIBLE);
        myViewHolder.btnVote.setVisibility(View.GONE);

        Call<ResponseBody> call = getCallEndpoint(candidateId);

        Log.d("TAG", "vote: " + call.request().toString());
//        VoteDto vote = new VoteDto(electionId, candidateId);
//
//        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(context).getApi().gubernatorialVote(vote);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                myViewHolder.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONObject responseData = jsonObject.getJSONObject("data");

                            successAlert("SUCCESS!!!", candidateName);

                        } else {
                            errorAlert("ATTENTION VOTER", "Voting operation failed. Try again");
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                myViewHolder.progressBar.setVisibility(View.GONE);
            }
        });
    }
}
