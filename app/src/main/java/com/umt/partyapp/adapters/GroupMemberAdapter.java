package com.umt.partyapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.model.DataModel;
import com.umt.partyapp.model.GroupMemberDto;

import java.util.ArrayList;

public class GroupMemberAdapter extends ArrayAdapter<GroupMemberDto> implements View.OnClickListener{

    private ArrayList<GroupMemberDto> dataSet;
    Context mContext;

    public GroupMemberAdapter(@NonNull Context context,ArrayList<GroupMemberDto> data) {
        super(context, R.layout.group_member_list, data);
        this.dataSet = data;
        this.mContext=context;

    }

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView description;
        TextView role;

    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        GroupMemberDto dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.group_member_list, parent, false);

            viewHolder.name = (TextView) convertView.findViewById(R.id.title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.date);
            viewHolder.role = (TextView) convertView.findViewById(R.id.role);

            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.name.setText(dataModel.getName());
        viewHolder.description.setText(dataModel.getDescription());
        if(dataModel.getCordinator()==true)
        {
            viewHolder.role.setVisibility(View.VISIBLE);
        }



        // Return the completed view to render on screen
        return convertView;
    }


}
