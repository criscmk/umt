package com.umt.partyapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.umt.partyapp.R;


import com.umt.partyapp.model.VerifyCodeDto;
import com.umt.partyapp.model.ResendCodeDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.sms.SMSReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationActivity extends AppCompatActivity implements
        SMSReceiver.OTPReceiveListener {
    private EditText editTextVerificationCode;
    private TextInputLayout inputLayoutCode;
    private Button buttonVerify,buttonResend;
    private ProgressBar progressBar;
    JSONObject jsonObject;
    String userId,phoneVerificationCode,memberNumber;
    private SMSReceiver smsReceiver;
    int btnState = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        editTextVerificationCode= (EditText) findViewById(R.id.member_verification_code);

        progressBar = (ProgressBar) findViewById(R.id.verification_progress);
        buttonVerify = (Button) findViewById(R.id.verificationLogIn);
        buttonResend = (Button) findViewById(R.id.member_resend_code_btn);
        Intent intent = getIntent();
//        memberNumber = intent.getStringExtra(MemberRegistrationActivity.MemberNumber);
//        userId= intent.getStringExtra(MemberRegistrationActivity.USER_ID);
        String Userdetails = intent.getStringExtra(MemberRegistrationActivity.UserDetails);
        try {
            jsonObject = new JSONObject(Userdetails);
             userId = jsonObject.getString("userId");
             memberNumber = jsonObject.getString("memberNumber");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String msg  = "Success. User registered. Your Member Number " +" " + memberNumber;

        customeDailog("Success",msg);
        startSMSListener();
        Toast.makeText(this, "userid"+userId, Toast.LENGTH_SHORT).show();
        Log.d("useruid",userId);

        buttonVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validatefields();
                    btnState = 1;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        buttonResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                resendCode();
            }
        });
    }

    private void validatefields() throws JSONException {
        phoneVerificationCode = editTextVerificationCode.getText().toString().trim();
        if (phoneVerificationCode.isEmpty()) {
            editTextVerificationCode.setError("Verification code is required");
            editTextVerificationCode.requestFocus();
            return;

        }
            verifyCode();
    }



    private void verifyCode() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);
        VerifyCodeDto userCode = new VerifyCodeDto(userId,phoneVerificationCode);
        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getApplicationContext()).getApi().VerifyCode(userCode);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                //Toast.makeText()
                String body = null;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        if (response.body() != null) {

                            Log.i("responce",body);


                            try {
                                parseResponse(body);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.i("error",t.toString());
            }
        });


    }

    //resend code
    private void resendCode()
    {

        progressBar.setVisibility(View.VISIBLE);
        ResendCodeDto resendCodeDto = new ResendCodeDto(userId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getApplicationContext()).getApi().ResendCode(resendCodeDto);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                //Toast.makeText()
                String body = null;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.i("responce",body);
                    //Toast.makeText(MemberRegistrationActivity.this, "responce"+body, Toast.LENGTH_LONG).show();
                    //Toast.makeText()

                    if (response.body() != null) {

                        Log.i("onSuccess", response.body().toString());
                        try {
                            parseResponse(body);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.i("error",t.toString());
            }
        });


    }
    private void parseResponse(String response) throws JSONException {

        jsonObject = new JSONObject(response);
        Boolean status = Boolean.valueOf(jsonObject.getString("status"));

        if(!status)
        {
            Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            customeDailog("Alert",jsonObject.getString("message"));
        }
        else if (status)
        {
            customeDailog("Success",jsonObject.getString("message"));
            Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            if(btnState == 1)
            {
                Intent intent = new Intent(VerificationActivity.this, LogInActivity.class);

                startActivity(intent);
            }



        }

    }










//sms reciever
private void startSMSListener() {
    try {
        smsReceiver = new SMSReceiver();
        smsReceiver.setOTPListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        this.registerReceiver(smsReceiver, intentFilter);

        SmsRetrieverClient client = SmsRetriever.getClient(this);

        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(VerificationActivity.this, "Okaayyyyyyyyy", Toast.LENGTH_SHORT).show();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Fail to start API
                Toast.makeText(VerificationActivity.this, "failed", Toast.LENGTH_SHORT).show();

            }
        });
    } catch (Exception e) {
        e.printStackTrace();
        Toast.makeText(VerificationActivity.this, "error"+e.toString(), Toast.LENGTH_SHORT).show();
    }
}



    @Override
    public void onOTPReceived(String otp) {

        Toast.makeText(this, "OTP"+otp, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {
        Toast.makeText(this, "OTP"+error, Toast.LENGTH_SHORT).show();
    }



    public void customeDailog(String header, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        alertHeaderText.setText(header);
        alertMessage.setText(message);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        okBtn.setVisibility(View.VISIBLE);
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        };
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 3000);

    }
}
