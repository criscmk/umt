package com.umt.partyapp.activity

import android.content.Context
import android.content.Intent
import android.os.*
import android.support.annotation.Nullable
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import chat.rocket.common.model.ServerInfo
import chat.rocket.common.model.Token
import chat.rocket.core.RocketChatClient
import chat.rocket.core.TokenRepository
import chat.rocket.core.internal.realtime.socket.connect
import chat.rocket.core.internal.rest.loginWithEmail
import chat.rocket.core.internal.rest.postMessage
import com.umt.partyapp.R
import com.umt.partyapp.adapters.AdapterChat.MESSAGE
import com.umt.partyapp.adapters.AdapterChat.USERNAME
import com.umt.partyapp.adapters.ThreadsAdapter
import com.umt.partyapp.chat.Callback
import com.umt.partyapp.handlers.ChatHandler
import com.umt.partyapp.model.MessageDto
import com.umt.partyapp.retrofitinterface.RocketChatClientService
import com.umt.partyapp.service.ChatListenerService
import com.umt.partyapp.util.Tools
import kotlinx.coroutines.runBlocking
import java.util.*

class MessageThreadActivity : AppCompatActivity(), ChatHandler.AppReceiver {
    @kotlin.jvm.JvmField
    var replyMessage: Any = "cmk"

    @kotlin.jvm.JvmField
    var replyUsername: Any = "cmk"
    var btn_send: FloatingActionButton? = null
    private var et_content: EditText? = null
    private var reply_content: EditText? = null
    var adapter: ThreadsAdapter? = null
    private var recycler_view: RecyclerView? = null
    var btn_cancel: ImageView? = null
    var replytext: TextView? = null;
    var replyUsernametxt: TextView? = null
    var thread: Boolean = false
    private var actionBar: ActionBar? = null
    private var backBtn: LinearLayout? = null
    var handler: Handler? = null
    var messageData: Message? = null
    var fromMe: Boolean? = true
    var reply_thread_layout: RelativeLayout? = null
    var reply_chat_layout: RelativeLayout? = null
    var title: TextView? = null
    var message: String? = null
    var username: String? = null
    var thread_header_message: TextView? = null
    var thread_user_name: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_thread)

        message = intent.getStringExtra(MESSAGE)
        username = intent.getStringExtra(USERNAME)

        iniComponent()
        initToolbar()
        initiateChat()

        randomReplies()

        thread_header_message?.setText(message)
        thread_user_name?.setText(username)

    }

    private fun randomReplies() {
        val messages = arrayOf(
                "Was I always going to be here? No I was not. I was going to be homeless at one time, a taxi driver, truck driver, or any kind of job that would get me a crust of bread. You never know what’s going to happen.",
                "The best way to guarantee a loss is to quit.",
                "When you have made your choice, it is providence that is your guide. Good, bad, or indifferent. Your fate lies in that.",
                "I can say that life is good to me. Has been and is good. So I think my task is to be good to it. How do you be good to life? You live it.",
                "One of the things you can always depend on – this is one of the truths of the universe, and you heard it first from here – whatever we decide we want to do is what we do.",
                "Forgiveness liberates the soul, it removes fear.",
                "Don’t be different just for different’s sake. If you see it differently, function that way. Follow your own muse, always.",
                "If you live a life of make-believe, your life isn’t worth anything until you do something that does challenge your reality.")

        val random = Random()
        var message = ""
        for (i in 0..4) {
            message = messages[random.nextInt(messages.size)]
            adapter!!.insertItem(MessageDto(adapter!!.itemCount.toLong(), message, false, adapter!!.itemCount % 5 === 0, "Umt-Admin", "cmk", "agggsggsgsg", Tools.getFormattedTimeEvent(System.currentTimeMillis())))


        }

    }

    fun initiateChat() {
        val intent = Intent(applicationContext, ChatListenerService::class.java)
        handler = ChatHandler(this)
        intent.putExtra("handler", Messenger(handler))
        startService(intent)

    }

    private fun RocketChatClient.serverInfo(callback: Callback<ServerInfo>) {

    }

    fun initToolbar() {
        val toolbar: Toolbar = findViewById(R.id.chat_toolbar)
        setSupportActionBar(toolbar)
        actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(false)
        actionBar?.setHomeButtonEnabled(false)
        actionBar?.setTitle("Politics and Human Rights")
        //Tools.setSystemBarColorInt(this, Color.parseColor("#054D44"))
    }

    fun iniComponent() {
        recycler_view = findViewById(R.id.recyclerView)
        val layoutManager = LinearLayoutManager(this)
        backBtn = findViewById(R.id.lyt_back)
        recycler_view?.setLayoutManager(layoutManager)
        recycler_view?.setHasFixedSize(true)
        adapter = ThreadsAdapter(this)
        recycler_view?.setAdapter(adapter)
        replytext = findViewById(R.id.reply_message)
        replyUsernametxt = findViewById(R.id.reply_username)
        btn_send = findViewById(R.id.btn_send)
        et_content = findViewById(R.id.text_content)
        reply_content = findViewById(R.id.text_reply)
        reply_chat_layout = findViewById(R.id.chat_reply)
        title = findViewById(R.id.toolbar_title)
        thread_header_message = findViewById(R.id.thread_header_message)
        thread_user_name = findViewById(R.id.thread_user_name)
        btn_send?.setOnClickListener(View.OnClickListener { sendChat() })
        et_content?.addTextChangedListener(contentWatcher)
        reply_content?.addTextChangedListener(contentWatcher)
        backBtn?.setOnClickListener(View.OnClickListener { onBackPressed() })
        val toolbarTitle = intent.getStringExtra("title")
        val message = intent.getStringExtra("lastMessage")
        title?.setText(toolbarTitle)


        //adapter!!.insertItem(MessageDto(adapter!!.itemCount.toLong(), message, true, adapter!!.itemCount % 5 === 0, "Umt-Admin", "cmk", "agggsggsgsg", Tools.getFormattedTimeEvent(System.currentTimeMillis())))

    }

    private fun sendChat() {
        val msg: String
        msg = et_content?.getText().toString()
        message
        Log.i("Thread Reply", msg)
        adapter!!.insertItem(MessageDto(adapter!!.itemCount.toLong(), msg, true, adapter!!.itemCount % 5 === 0, "Umt-Admin", "cmk", "agggsggsgsg", Tools.getFormattedTimeEvent(System.currentTimeMillis())))
        et_content?.setText("")
//        val task = SendMessageTask()
//        task.execute(msg, "nqxp68MLZytJw3sux")

    }

    override fun onPostCreate(@Nullable savedInstanceState: Bundle?, @Nullable persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private val contentWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(etd: Editable) {
            if (etd.toString().trim { it <= ' ' }.length == 0) {
                btn_send?.setImageResource(R.drawable.ic_mic)
            } else {
                btn_send?.setImageResource(R.drawable.ic_send)
            }
        }

        override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
        override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_chat_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else {
            Toast.makeText(applicationContext, item.title.toString() + " clicked", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }


    class SimpleTokenRepository : TokenRepository {
        private var savedToken: Token? = null
        override fun save(url: String, token: Token) {
            savedToken = token
        }

        override fun get(url: String): Token? {
            return savedToken
        }
    }

    fun connectToChat() {


    }

    inner class SendMessageTask : AsyncTask<String, Int, Int>() {

        //Override the doInBackground method
        override fun doInBackground(vararg params: String?): Int {

            val count: Int = params.size

            var client = RocketChatClientService.clientInstance;

            var message = params[0];
            var roomId = params[1]


            client.connect()

            runBlocking {
                val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")
                client.postMessage(
                        roomId = "nqxp68MLZytJw3sux",
                        text = message,
                        alias = "umt-admin",
                        emoji = ":smirk:",
                        avatar = "https://avatars2.githubusercontent.com/u/224255?s=88&v=4")
            }
            return count;

        }

        // Override the onProgressUpdate method to post the update on main thread
        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            if (values[0] != null) {


            }

        }

        override fun onPreExecute() {

            super.onPreExecute()

        }

        // Update the final status by overriding the OnPostExecute method.
        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)

        }

    }


    override fun onReceiveResult(message: Message?) {
//        var room: Room = message?.obj as Room;
//
//        Log.d("MSRECEIVER", room.toString())
//        et_content?.setText("")

//        if (room.id == "nqxp68MLZytJw3sux") {
//
//            if (room.lastMessage?.sender?.username == "umt-admin") {
//                adapter?.insertItem(MessageDto(adapter!!.getItemCount().toLong(), room.lastMessage?.message, true, adapter!!.getItemCount() % 5 === 0, room.lastMessage?.sender?.username, "cmk", room.lastMessage?.id, Tools.getFormattedTimeEvent(room?.lastMessage?.timestamp)))
//            } else {
//                adapter?.insertItem(MessageDto(adapter!!.getItemCount().toLong(), room.lastMessage?.message, false, adapter!!.getItemCount() % 5 === 0, room.lastMessage?.sender?.username, "cmk", room.lastMessage?.id, Tools.getFormattedTimeEvent(room?.lastMessage?.timestamp)))
//            }
//        }


    }


}