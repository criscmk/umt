package com.umt.partyapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.fragment.home.HomeFragment;
import com.umt.partyapp.fragment.home.NotificationFragment;
import com.umt.partyapp.sessionmanager.Sessionmangementsharedpref;
import com.umt.partyapp.util.Tools;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ViewPager view_pager;
    private TabLayout tab_layout;
    private SectionsPagerAdapter adapter;

    Boolean data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //Check if user logged in -- Refresh token exists and is valid - Login

        Sessionmangementsharedpref sessionMangementSharedPref = new Sessionmangementsharedpref(this);
        if (!sessionMangementSharedPref.isLoggedIn()) {
            //sessionMangementSharedPref.logout();
        } else {
//            Intent loginIntent = new Intent(HomeActivity.this, ListBranchActivity.class);
//            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(loginIntent);
//            finish();
        }

        //If user membership valid - List Branch


        initToolbar();
        initComponent();

        Intent intent = getIntent();
        data = intent.getBooleanExtra("votedPetition", false);
    }

    @SuppressLint("ResourceAsColor")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Love Branch");
        toolbar.setTitleTextColor(getResources().getColor(R.color.umt_white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarColor(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        initNavigationView();
    }

    @SuppressLint("ResourceAsColor")
    private void initNavigationView() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        //View headerView= navigationView.getHeaderView(1);
//        navigationView.getBackground().setAlpha(212);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

    }

    private void initComponent() {
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(view_pager);
        tab_layout = (TabLayout) findViewById(R.id.tab_layout);
        tab_layout.setupWithViewPager(view_pager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Trending");
        adapter.addFragment(new NotificationFragment(), "Forums");
        adapter.addFragment(HomeActivity.PlaceholderFragment.newInstance(3), "Chats");
        adapter.addFragment(new NotificationFragment(), "Notifications");


        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.branch_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.nav_chat) {
            startActivity(new Intent(this, ChatForumsActivity.class));
            return true;

        } else if (id == R.id.vote) {

           Intent intent = new Intent(this, VoteActivity.class);
           intent.putExtra("votedPetition", data);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_branch) {
            startActivity(new Intent(this, BranchInformationActivity.class));
            return true;

        } else if (id == R.id.nav_contribute) {
            return true;

        } else if (id == R.id.nav_invite) {
            startActivity(new Intent(this, InviteMembers.class));
            return true;
        }

        return super.onOptionsItemSelected(menuItem);

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static VoteActivity.PlaceholderFragment newInstance(int sectionNumber) {
            VoteActivity.PlaceholderFragment fragment = new VoteActivity.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabs_basic, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public boolean isLogged() {

        return Sessionmangementsharedpref.getInstance(this).isLoggedIn();

    }
}