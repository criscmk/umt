package com.umt.partyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.umt.partyapp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import com.umt.partyapp.model.UserLoginResponceDto;

import com.umt.partyapp.retrofitinterface.RetrofitClient;
import com.umt.partyapp.sessionmanager.Sessionmangementsharedpref;
import com.umt.partyapp.util.Constants;
import com.umt.partyapp.util.CustomAlerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {

    private ProgressBar progress_bar;
    private FloatingActionButton fab;
    private View parent_view;
    String sessionType = "";


    EditText editTextUsername,editTextPassword;
    String username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        parent_view = findViewById(android.R.id.content);
        progress_bar = findViewById(R.id.progress_bar);
        fab = findViewById(R.id.fab);

        editTextUsername = findViewById(R.id.username);
        editTextPassword = findViewById(R.id.password);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            sessionType = extras.getString("SIGN_IN_TYPE");
        }

        ((View) findViewById(R.id.sign_up_for_account)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        ((View) findViewById(R.id.forgot_password_layout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(intent);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                validate();

            }
        });
    }

    public void demoLogin() {
        progress_bar.setVisibility(View.VISIBLE);
        fab.setAlpha(0f);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progress_bar.setVisibility(View.GONE);
                fab.setAlpha(1f);

                Intent p = new Intent(LogInActivity.this, WalkThroughActivity.class);
                startActivity(p);
            }
        }, 1000);
    }


    public void   validate() {
        username = editTextUsername.getText().toString().trim();
        password = editTextPassword.getText().toString().trim();

        if(username.isEmpty()){
            editTextUsername.setError("Id Number/ Email is required");
            editTextUsername.requestFocus();
            return;
        }
        if(password.isEmpty())
        {
            editTextPassword.setError("Password is required");
            editTextPassword.requestFocus();
            return;
        }
        userLogin();
    }

    public void userLogin() {

        Call<ResponseBody> call = RetrofitClient
                .getmInstance()
                .getApi()
                .UserLogin(username,password, Constants.CLIENT_ID,Constants.CLIENT_SECRET,Constants.REFRESH_TOKEN_GRANT_TYPE2);

        progress_bar.setVisibility(View.VISIBLE);
        fab.setAlpha(0f);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progress_bar.setVisibility(View.GONE);
                fab.setAlpha(1f);
                Log.d("response main",response.toString());
                try {

                    if (response.isSuccessful()) {
                        String s = response.body().string();

                        JSONObject jsonObject = new JSONObject(s);

                        UserLoginResponceDto userDto = new UserLoginResponceDto(
                                jsonObject.getString("access_token"),
                                jsonObject.getString("refresh_token"),
                                jsonObject.getInt("expires_in")

                        );

                        Sessionmangementsharedpref.getInstance(getApplicationContext()).userLogin(userDto);
                        Log.d("response 2", s);
                        Intent p = new Intent(LogInActivity.this, WalkThroughActivity.class);
                        startActivity(p);

                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Log.d("error",e.toString());
                    progress_bar.setVisibility(View.GONE);
                }



            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progress_bar.setVisibility(View.GONE);
                fab.setAlpha(1f);
                Log.d("error",t.toString());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
    public void isLogged()
    {
        if (!Sessionmangementsharedpref.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LogInActivity.class));
        }
        else {
            startActivity(new Intent(this, ListBranchActivity.class));
        }

    }
}
