package com.umt.partyapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.model.ForgortPasswordDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.hbb20.CountryCodePicker;

public class ForgotPassword extends AppCompatActivity {

    private EditText phoneNumber;
    private Button submitBtn;
    private ProgressBar mProgressBar;
    public CountryCodePicker ccp;

    public final static String USER_ID = "com.umt.partyapp.activity.forgotpasswordactivity.user_id";
    public final static String USER_PHONE = "com.umt.partyapp.activity.forgotpasswordactivity.user_phone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        ccp = (CountryCodePicker) findViewById(R.id.ccp_fp);
        phoneNumber = findViewById(R.id.user_phonenumber);
         ccp.registerCarrierNumberEditText(phoneNumber);
        ccp.setNumberAutoFormattingEnabled(true);


        submitBtn = findViewById(R.id.submit_phone);

        mProgressBar = findViewById(R.id.forgot_password_progress);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPhoneNumber();
            }
        });




    }

    public void submitPhoneNumber() {

        String telNumber = ccp.getFullNumberWithPlus();
        Log.i("number", telNumber);

        if (!ccp.isValidFullNumber()) {
            phoneNumber.setError("Invalid for number");
            phoneNumber.requestFocus();
            return;
        }

//        if (telNumber.length() < 10){
//            phoneNumber.setError("Invalid phone nunber");
//            phoneNumber.requestFocus();
//            return;
//        }

        showProgressBar();

//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("phoneNumber", telNumber);


        ForgortPasswordDto forgortPasswordDto = new ForgortPasswordDto(telNumber);

            Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getApplicationContext()).getApi().RequestPasswordReset(forgortPasswordDto);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    hideProgressBar();
                    try {
                        if (response.isSuccessful()) {
                            String s = response.body().string();

                            Log.i("res", s);

                            JSONObject res = new JSONObject(s);

                            Boolean status = res.getBoolean("status");
                            String message = res.getString("message");

                            if (status){
                                JSONObject data = res.getJSONObject("data");

                                String id = data.getString("id");
                                String phone = data.getString("phoneNumber");
                                Intent newIntent = new Intent(getApplicationContext(), ResetPassword.class);
                                newIntent.putExtra(USER_ID, id);
                                newIntent.putExtra(USER_PHONE, phone);
                                startActivity(newIntent);

                            }
                            else {
                                customeDailog("ERROR!!!", message);
                            }
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    public void customeDailog(String header, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        alertHeaderText.setText(header);
        alertMessage.setText(message);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // Hide after some seconds
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        };

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 3000);

    }

    public void onClick(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPassword.this, LogInActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}