package com.umt.partyapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.umt.partyapp.R
import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.os.PersistableBundle
import android.support.annotation.Nullable
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.ActionBar

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import chat.rocket.common.RocketChatException
import chat.rocket.common.model.ServerInfo
import chat.rocket.common.model.Token
import chat.rocket.common.util.PlatformLogger
import chat.rocket.core.RocketChatClient
import chat.rocket.core.TokenRepository
import chat.rocket.core.compat.serverInfo
import chat.rocket.core.createRocketChatClient
import chat.rocket.core.internal.realtime.*
import chat.rocket.core.internal.realtime.socket.connect
import chat.rocket.core.internal.realtime.socket.model.State
import chat.rocket.core.internal.rest.chatRooms
import chat.rocket.core.internal.rest.loginWithEmail
import chat.rocket.core.internal.rest.permissions
import chat.rocket.core.internal.rest.serverInfo
import chat.rocket.core.model.Message
import chat.rocket.core.model.history
import chat.rocket.core.model.messages
import com.umt.partyapp.adapters.AdapterChat
import com.umt.partyapp.chat.Callback
import com.umt.partyapp.model.MessageDto
import com.umt.partyapp.util.Tools
import kotlinx.android.synthetic.main.activity_test.*
import kotlinx.android.synthetic.main.sheet_list.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class TestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        val bottomSheetDialog = BottomSheetDialog (this)
        val view =layoutInflater.inflate(R.layout.sheet_list,null)
        bottomSheetDialog.setContentView(view)
        btn_dialogue.setOnClickListener{
            bottomSheetDialog.show()
        }
        view.reply.setOnClickListener{
            Toast.makeText(this,"heloooooo", LENGTH_LONG).show()
        }


    }
   }