package com.umt.partyapp.activity;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.model.CreateBranchDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.CustomAlerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateBranchActivity extends AppCompatActivity {
    private EditText editTextBranchName,editTextBranchDescription;
    private TextView textViewWordCount;
    int counter;
    ProgressBar progressBar;
    Button createBranchBtn;
    String name,description;
    ListBranchActivity listBranchActivity;
    public String Token = "bearer 4338053f-7234-4ee9-81ac-67079ab77341";
    CustomAlerts customAlerts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_branch);
        initToolbar();
        editTextBranchName = findViewById(R.id.branch_name);
        editTextBranchDescription = findViewById(R.id.branch_description);
        textViewWordCount = findViewById(R.id.word_count);
        progressBar = findViewById(R.id.progress_bar_create_branch);
        createBranchBtn = findViewById(R.id.create_branch_btn);

        editTextBranchDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int length = editTextBranchDescription.length();
                String convert = String.valueOf(length);
                counter = 50;
                if (count == 0 && length >= counter) {
                  setCharLimit(editTextBranchDescription, editTextBranchDescription.length());
                } else {
                  removeFilter(editTextBranchDescription);
                }
                int remainingWords = counter -length;

                if(remainingWords<0)
                {
                    remainingWords = 0;
                }

                textViewWordCount.setText(String.valueOf(remainingWords)+ " characters remaining");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        createBranchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();

            }
        });
    }
    public void validate()
    {
       name = editTextBranchName.getText().toString();
       description = editTextBranchDescription.getText().toString();
        if(name.isEmpty()){
            editTextBranchName.setError("Name is required");
            return;
        }
        if(description.isEmpty())
        {
            editTextBranchDescription.setError("Description is required");
            return;
        }
        CreateBranch();
    }
    public void CreateBranch()
    {
        progressBar.setVisibility(View.VISIBLE);
        createBranchBtn.setAlpha(0f);

        CreateBranchDto createBranchDto = new CreateBranchDto(name,description);
        Call<ResponseBody> call = RetrofitAuthenticatedClient
                .getmInstance(getApplicationContext())
                .getApi()
                .CreateBranch(createBranchDto);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                createBranchBtn.setAlpha(1f);
                Log.d("response main",response.toString());
                try {

                    if (response.isSuccessful()) {
                        progressBar.setVisibility(View.GONE);
                        createBranchBtn.setAlpha(1f);
                        String s = response.body().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Boolean status = Boolean.valueOf(jsonObject.getString("status"));
                        if(status)
                        {
                            customAlerts.SweetAlert(CreateBranchActivity.this,"Success",jsonObject.getString("message"), SweetAlertDialog.SUCCESS_TYPE);
                        }
                        else
                        {
                           customAlerts.SweetAlert(CreateBranchActivity.this,"Alert",jsonObject.getString("message"), SweetAlertDialog.WARNING_TYPE);
                        }
//
//                        Intent p = new Intent(ListBranchActivity.this, HomeActivity.class);
//                        startActivity(p);
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Log.d("error",e.toString());
                    progressBar.setVisibility(View.GONE);
                    createBranchBtn.setAlpha(1f);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                createBranchBtn.setAlpha(1f);
                Log.d("error",t.toString());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private int countWords(String s) {
        String trim = s.trim();
        if (trim.isEmpty())
            return 0;
        return trim.split("\\s+").length; // separate string around spaces
    }

    private InputFilter filter;

    private void setCharLimit(EditText et, int max) {
        filter = new InputFilter.LengthFilter(max);
        et.setFilters(new InputFilter[] { filter });
    }

    private void removeFilter(EditText et) {
        if (filter != null) {
            et.setFilters(new InputFilter[0]);
            filter = null;
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.branch_toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Branch");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sub_menu, menu);

           return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        if(id == R.id.action_info)
        {
            customeDailog("Info","Branch name must consist of a maximum of 3 words and must not include the word branch",1);

            return true;
        }
        if(id == R.id.action_profile)
        {
            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void customeDailog(String header, String message, int option) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        View image = dialogView.findViewById(R.id.dialog_image);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        Button accept = dialogView.findViewById(R.id.btn_accept);
        accept.setText("Join");
        //display one button
        if(option==1)
        {
            okBtn.setVisibility(View.VISIBLE);
        }
        else
        {
            accept.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }


        //TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        //alertHeaderText.setText(header);
        alertMessage.setText(message);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

    }

    public void SweetAlert(String title,String message,int icon)
    {
        SweetAlertDialog sweetAlertDialog= new SweetAlertDialog(CreateBranchActivity.this, icon);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismiss();
                }
            }
        };
        sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 5000);
    }

}