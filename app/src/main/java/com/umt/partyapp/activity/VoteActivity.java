package com.umt.partyapp.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ToxicBakery.viewpager.transforms.FlipHorizontalTransformer;
import com.umt.partyapp.R;
import com.umt.partyapp.fragment.election.coordinator.CoordinatorElectionRootFragment;
import com.umt.partyapp.fragment.election.governor.GovernorElectionRootFragment;
import com.umt.partyapp.fragment.election.mca.McaElectionRootFragment;
import com.umt.partyapp.fragment.election.mp.MpRootFragment;
import com.umt.partyapp.fragment.election.president.PresidentialElectionRootFragment;
import com.umt.partyapp.fragment.election.senator.SenatorElectionRootFragment;
import com.umt.partyapp.fragment.election.womanrep.WomanRepRootFragment;
import com.umt.partyapp.util.Tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class VoteActivity extends AppCompatActivity {
    private static final String TAG = "voting";

    private static final int NUM_PAGES = 1;

    private ViewPager view_pager;
    private TabLayout tab_layout;
    private SectionsPagerAdapter adapter;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);

        initToolbar();

//        getBranchElectionInfo();
        initComponent();

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Election Room");
        toolbar.setTitleTextColor(getResources().getColor(R.color.umt_white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarColor(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initComponent() {

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(view_pager);

        tab_layout = (TabLayout) findViewById(R.id.tab_layout);
        tab_layout.setupWithViewPager(view_pager);
    }


    public void refreshActivity() {
        finish();
        startActivity(getIntent());
    }

    public long getDateTimeDiff(String dateFrom, String dateTo) {
        long diff = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");

        if (dateFrom == null) {
            dateFrom = formatter.format(new Date());
        } else {
            dateFrom = getDate(dateFrom);
        }

        dateTo = getDate(dateTo);

        Log.d(TAG, "date from " + dateFrom);
        Log.d(TAG, "date to " + dateTo);
        Date oldDate, newDate;

        try {
            oldDate = formatter.parse(dateFrom);
            newDate = formatter.parse(dateTo);
           long oldLong = oldDate.getTime();
           long newLong = newDate.getTime();
            diff = newLong - oldLong;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diff;
    }

    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupViewPager(ViewPager viewPager) {

        adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new CoordinatorElectionRootFragment(), "Coordinator");
        adapter.addFragment(new McaElectionRootFragment(), "MCA");
        adapter.addFragment(new MpRootFragment(), "MP");
        adapter.addFragment(new WomanRepRootFragment(), "Women Rep");
        adapter.addFragment(new SenatorElectionRootFragment(), "Senator");
        adapter.addFragment(new GovernorElectionRootFragment(), "Governor");
        adapter.addFragment(new PresidentialElectionRootFragment(), "Party Leader");

        adapter.addFragment(PlaceholderFragment.newInstance(8), "Ward Committee");

        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(true, new FlipHorizontalTransformer());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_vote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabs_basic, container, false);

            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            Button btnNext = rootView.findViewById(R.id.next_fragment);

            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

            btnNext.setOnClickListener(v -> {

            });
            return rootView;
        }
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private Fragment mFragmentAtPos0;
        private final FragmentManager mFragmentManager;

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
            mFragmentManager = manager;
        }


        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public Fragment getItem(int position) {
            Fragment f = mFragmentList.get(position);

//            if (position == 0)
//                return new BlankFragment();
//            else if (position == 1)
//                return new BlankFragment();
//            else
            return f;


//           f.setArguments();
//            Log.d(TAG, "fragment " + f.getArguments().toString() + " position " + position);


        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
