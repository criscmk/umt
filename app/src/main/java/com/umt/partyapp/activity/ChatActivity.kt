package com.umt.partyapp.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.support.annotation.Nullable
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.FileProvider
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import butterknife.ButterKnife
import butterknife.OnClick
import chat.rocket.common.model.RoomType
import chat.rocket.common.model.ServerInfo
import chat.rocket.common.model.Token
import chat.rocket.core.RocketChatClient
import chat.rocket.core.TokenRepository
import chat.rocket.core.internal.realtime.socket.connect
import chat.rocket.core.internal.rest.*
import chat.rocket.core.model.PagedResult
import chat.rocket.core.model.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.umt.partyapp.BuildConfig
import com.umt.partyapp.R
import com.umt.partyapp.adapters.AdapterChat
import com.umt.partyapp.chat.Callback
import com.umt.partyapp.handlers.ChatHandler
import com.umt.partyapp.model.MessageDto
import com.umt.partyapp.retrofitinterface.RocketChatClientService
import com.umt.partyapp.service.ChatListenerService
import com.umt.partyapp.util.FileCompressor
import com.umt.partyapp.util.Tools
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.sheet_list.view.*
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ChatActivity : AppCompatActivity(), ChatHandler.AppReceiver {


    @kotlin.jvm.JvmField
    var replyMessage: Any = "cmk"

    @kotlin.jvm.JvmField
    var replyUsername: Any = "cmk"

    @kotlin.jvm.JvmField
    var mainMessageId: Any = "cmk"
    var btn_send: FloatingActionButton? = null
    private var et_content: EditText? = null
    private var reply_content: EditText? = null
    var adapter: AdapterChat? = null
    private var recycler_view: RecyclerView? = null
    var btn_cancel: ImageView? = null
    var btn_photo_image: ImageView? = null
    var replytext: TextView? = null;
    var replyUsernametxt: TextView? = null
    var thread: Boolean = false
    private var actionBar: ActionBar? = null
    private var backBtn: LinearLayout? = null
    var handler: Handler? = null
    var messageData: Message? = null
    var fromMe: Boolean? = true
    var reply_thread_layout: RelativeLayout? = null
    var reply_chat_layout: RelativeLayout? = null
    var title: TextView? = null
    var roomId: String? = null
    var delete: Boolean = false


    //File Upload
    val REQUEST_TAKE_PHOTO: Int = 1
    val REQUEST_GALLERY_PHOTO = 2
    var mPhotoFile: File? = null
    var mCompressor: FileCompressor? = null

   /* @BindView(R.id.image_photo_camera)
    var imagePhotoCamera: ImageView? = null*/


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        roomId = "nqxp68MLZytJw3sux"

        ButterKnife.bind(this);
        iniComponent()
        initToolbar()
        initiateChat()
        fetchMessages()

    }


    fun initiateChat() {
        val intent = Intent(applicationContext, ChatListenerService::class.java)
        handler = ChatHandler(this)
        intent.putExtra("handler", Messenger(handler))
        startService(intent)

    }

    private fun RocketChatClient.serverInfo(callback: Callback<ServerInfo>) {

    }

    fun initToolbar() {
        val toolbar: Toolbar = findViewById(R.id.chat_toolbar)
        setSupportActionBar(toolbar)
        actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(false)
        actionBar?.setHomeButtonEnabled(false)
        actionBar?.setTitle(null)
        //Tools.setSystemBarColorInt(this, Color.parseColor("#054D44"))
    }

    fun iniComponent() {
        recycler_view = findViewById(R.id.recyclerView)
        val layoutManager = LinearLayoutManager(this)
        backBtn = findViewById(R.id.lyt_back)
        btn_photo_image = findViewById(R.id.image_photo_camera)
        recycler_view?.setLayoutManager(layoutManager)
        recycler_view?.setHasFixedSize(true)
        adapter = AdapterChat(this)
        recycler_view?.setAdapter(adapter)
        replytext = findViewById(R.id.reply_message)
        replyUsernametxt = findViewById(R.id.reply_username)
        btn_send = findViewById(R.id.btn_send)
        et_content = findViewById(R.id.text_content)
        reply_content = findViewById(R.id.text_reply)
        btn_cancel = findViewById(R.id.cancel_reply)
        reply_thread_layout = findViewById(R.id.reply_in_thread)
        reply_chat_layout = findViewById(R.id.chat_reply)
        title = findViewById(R.id.toolbar_title)
        btn_send?.setOnClickListener(View.OnClickListener { sendChat() })
        et_content?.addTextChangedListener(contentWatcher)
        reply_content?.addTextChangedListener(contentWatcher)
        backBtn?.setOnClickListener(View.OnClickListener { onBackPressed() })
        reply_chat_layout?.visibility = View.VISIBLE
        btn_cancel?.setOnClickListener(View.OnClickListener {
            reply_thread_layout?.visibility = View.INVISIBLE;
            reply_chat_layout?.visibility = View.VISIBLE;
            thread = false
        })

        btn_photo_image?.setOnClickListener(View.OnClickListener {
            Toast.makeText(applicationContext,  " clicked", Toast.LENGTH_SHORT).show()

            selectImage()
        })


    }


    fun bottomSheet() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.sheet_list, null)

        bottomSheetDialog.setContentView(view)

        bottomSheetDialog.show()
        view.reply.setOnClickListener {
            bottomSheetDialog.hide()
            thread = true
            this.reply_thread_layout?.visibility = View.VISIBLE
            this.chat_reply?.visibility = View.INVISIBLE
            replytext?.setText(replyMessage.toString())

            if (replyUsername == "umt-admin") {
                replyUsernametxt?.setText("You")
            } else {
                replyUsernametxt?.setText(replyUsername.toString())
            }


        }
        view.like_message.setOnClickListener {
            bottomSheetDialog.hide()
//            val like = likeMessageTask()
//            like.execute()

        }
        view.delete.setOnClickListener {
            bottomSheetDialog.hide()
            delete = true
            val delete = deleteMessageTask()
            delete.execute()
        }

    }

    fun fetchMessages() {
        val fetchMessagesTask = GetMessages();
        fetchMessagesTask.execute("nqxp68MLZytJw3sux")
    }

    private fun sendChat() {
        val msg: String

        if (thread) {
            msg = reply_content?.getText().toString()
            title?.setText(replyMessage.toString())
            this.thread = true
            this.reply_chat_layout?.visibility = View.VISIBLE
            this.reply_thread_layout?.visibility = View.INVISIBLE
            this.reply_content?.setText("")
//            val intent = Intent(this, MessageThreadActivity::class.java).apply {
//                putExtra("title", replyMessage.toString())
//                putExtra("lastMessage", msg)
//
//            }
//            startActivity(intent)

        } else {
            msg = et_content?.getText().toString()

        }

        val task = SendMessageTask()
        task.execute(msg, "nqxp68MLZytJw3sux")
//        val intent = Intent(this, MessageThreadActivity::class.java).apply {
//            putExtra(EXTRA_MESSAGE, msg)
        // }
    }

    override fun onPostCreate(@Nullable savedInstanceState: Bundle?, @Nullable persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private val contentWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(etd: Editable) {
            if (etd.toString().trim { it <= ' ' }.length == 0) {
                btn_send?.setImageResource(R.drawable.ic_mic)
            } else {
                btn_send?.setImageResource(R.drawable.ic_send)
            }
        }

        override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
        override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_chat_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else {
            Toast.makeText(applicationContext, item.title.toString() + " clicked", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

/*
suspend fun showFavoriteMessage(client: RocketChatClient) {
        val result = client.getFavoriteMessages("GENERAL", RoomType.Channel(), 0)
        println("favoriteMessages: $result")
}

suspend fun showFileList(client: RocketChatClient) {
        val result = client.getFiles("GENERAL", RoomType.Channel(), 0)
        println("Attachment from the File List: $result")
}
*/

    class SimpleTokenRepository : TokenRepository {
        private var savedToken: Token? = null
        override fun save(url: String, token: Token) {
            savedToken = token
        }

        override fun get(url: String): Token? {
            return savedToken
        }
    }

    fun connectToChat() {


    }

    inner class GetMessages : AsyncTask<String, Int, PagedResult<List<chat.rocket.core.model.Message>>>() {

        override fun doInBackground(vararg params: String?): PagedResult<List<chat.rocket.core.model.Message>> {
            val count: Int = params.size

            var client = RocketChatClientService.clientInstance;

            /*var roomType = params[0];
            var roomId = params[1]*/


            client.connect()
            val msgList: List<chat.rocket.core.model.Message> = emptyList()
            var pagedResult = PagedResult<List<chat.rocket.core.model.Message>>(msgList, 0, 0)
            runBlocking {
                val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")
                pagedResult = client.messages(
                        roomId = "nqxp68MLZytJw3sux",
                        count = 500,
                        offset = 0,
                        roomType = RoomType.PrivateGroup())

                Log.d("results", pagedResult.total.toString())
            }

            return pagedResult;
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)

            if (values[0] != null) {


            }
        }

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun onPostExecute(result: PagedResult<List<chat.rocket.core.model.Message>>?) {
            super.onPostExecute(result)

            if (result != null) {
                val messages: List<chat.rocket.core.model.Message> = result.result
                for (message in messages) {
                    Log.d("messages", message.toString());
                    if (message?.sender?.username == "umt-admin") {
                        adapter?.insertItem(MessageDto(adapter!!.itemCount.toLong(), message.message, true, adapter!!.itemCount % 5 === 0, message?.sender?.username, 1.toString(), message?.sender?.id, Tools.getFormattedTimeEvent(message.timestamp)))
                    } else {
                        adapter?.insertItem(MessageDto(adapter!!.itemCount.toLong(), message.message, false, adapter!!.itemCount % 5 === 0, message?.sender?.username, 1.toString(), message?.sender?.id, Tools.getFormattedTimeEvent(message?.timestamp)))
                    }
                }
            }
        }

    }

    inner class SendMessageTask : AsyncTask<String, Int, Int>() {

        //Override the doInBackground method
        override fun doInBackground(vararg params: String?): Int {

            val count: Int = params.size

            var client = RocketChatClientService.clientInstance;

            var message = params[0];
            var roomId = params[1]


            client.connect()

            runBlocking {
                val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")

                if (thread) {
                    client.sendMessage(
                            roomId = roomId.toString(),
                            messageId = mainMessageId.toString(),
                            message = message)


                } else {
                    client.postMessage(
                            roomId = "nqxp68MLZytJw3sux",
                            text = message,
                            alias = "umt-admin",
                            emoji = ":smirk:",
                            avatar = "https://avatars2.githubusercontent.com/u/224255?s=88&v=4")
                }
            }





            return count;

        }

        // Override the onProgressUpdate method to post the update on main thread
        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            if (values[0] != null) {


            }

        }

        override fun onPreExecute() {

            super.onPreExecute()

        }

        // Update the final status by overriding the OnPostExecute method.
        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)

        }


    }

    // Create inner class by extending the AsyncTask
    inner class SendText : AsyncTask<String, Int, Int>() {

        //Override the doInBackground method
        override fun doInBackground(vararg params: String?): Int {

            val count: Int = params.size
            var client = RocketChatClientService.clientInstance;


            client.connect()
            runBlocking {
                val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")

                client.starMessage(
                        messageId = mainMessageId.toString()
                )

            }



            return count;

        }

        // Override the onProgressUpdate method to post the update on main thread
        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            if (values[0] != null) {


            }

        }

        override fun onPreExecute() {

            super.onPreExecute()

        }

        // Update the final status by overriding the OnPostExecute method.
        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)

        }

    }

    inner class deleteMessageTask : AsyncTask<String, Int, Int>() {

        //Override the doInBackground method
        override fun doInBackground(vararg params: String?): Int {
            val count: Int = params.size
            var client = RocketChatClientService.clientInstance;

            client.connect()
            runBlocking {
                val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")

                client.deleteMessage(
                        roomId = roomId.toString(),
                        msgId = mainMessageId.toString()
                )
            }


            return count;

        }

        // Override the onProgressUpdate method to post the update on main thread
        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            if (values[0] != null) {


            }

        }

        override fun onPreExecute() {

            super.onPreExecute()

        }

        // Update the final status by overriding the OnPostExecute method.
        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)

        }

    }

    override fun onReceiveResult(message: Message?) {
        var room: Room = message?.obj as Room;

        Log.d("MSRECEIVER", room.toString())
        et_content?.setText("")

        if (room.id == roomId) {

            this.thread = false
            if (room.lastMessage?.id != mainMessageId || !delete) {
                if (room.lastMessage?.sender?.username == "umt-admin") {
                    adapter?.insertItem(MessageDto(adapter!!.getItemCount().toLong(), room.lastMessage?.message, true, adapter!!.getItemCount() % 5 === 0, room.lastMessage?.sender?.username, "cmk", room.lastMessage?.id, Tools.getFormattedTimeEvent(room?.lastMessage?.timestamp)))
                } else {
                    adapter?.insertItem(MessageDto(adapter!!.getItemCount().toLong(), room.lastMessage?.message, false, adapter!!.getItemCount() % 5 === 0, room.lastMessage?.sender?.username, "cmk", room.lastMessage?.id, Tools.getFormattedTimeEvent(room?.lastMessage?.timestamp)))
                }

            }


        }


    }

    /**
     * Alert dialog for capture or select from galley
     */
    private fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library",
                "Cancel")
        val builder: AlertDialog.Builder = AlertDialog.Builder(this@ChatActivity)
        builder.setItems(items) { dialog, item ->
            if (items[item] == "Take Photo") {
                requestStoragePermission(true)
            } else if (items[item] == "Choose from Library") {
                requestStoragePermission(false)
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    /**
     * Capture image from camera
     */
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID.toString() + ".provider",
                        photoFile)
                mPhotoFile = photoFile
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }


    /**
     * Select image fro gallery
     */
    private fun dispatchGalleryIntent() {
        val pickPhoto = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO)
    }


    internal fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    mPhotoFile = mCompressor?.compressToFile(mPhotoFile)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                btn_photo_image?.let { Glide.with(this@ChatActivity).load(mPhotoFile).apply(RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.ic_photo_camera)).into(it) }
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                val selectedImage: Uri? = data.data
                try {
                    mPhotoFile = mCompressor?.compressToFile(File(getRealPathFromUri(selectedImage)))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                btn_photo_image?.let { Glide.with(this@ChatActivity).load(mPhotoFile).apply(RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.ic_photo_camera)).into(it) }
            }
        }
    }


    /**
     * Requesting multiple permissions (storage and camera) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private fun requestStoragePermission(isCamera: Boolean) {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent()
                            } else {
                                dispatchGalleryIntent()
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest?>?, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { error -> Toast.makeText(applicationContext, "Error occurred! ", Toast.LENGTH_SHORT).show() }
                .onSameThread()
                .check()
    }


    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog: DialogInterface, which: Int ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog: DialogInterface, which: Int -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    /**
     * Create file with current timestamp name
     *
     * @return
     * @throws IOException
     */
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val mFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(mFileName, ".jpg", storageDir)
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    fun getRealPathFromUri(contentUri: Uri?): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = contentResolver.query(contentUri!!, proj, null, null, null)
            assert(cursor != null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }

    @OnClick(R.id.image_photo_camera)
    fun onViewClicked() {
        selectImage()
    }

}