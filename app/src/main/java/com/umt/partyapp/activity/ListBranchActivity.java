package com.umt.partyapp.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.adapters.BranchAdapter;
import com.umt.partyapp.apiresponse.BranchDataDto;
import com.umt.partyapp.apiresponse.BranchListResponseDto;
import com.umt.partyapp.apiresponse.BranchMemberDao;
import com.umt.partyapp.apiresponse.BranchMembersResponseDao;
import com.umt.partyapp.model.BranchDto;
import com.umt.partyapp.model.JoinBranchDto;
import com.umt.partyapp.model.UserLoginResponceDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;

import com.umt.partyapp.sessionmanager.Sessionmangementsharedpref;
import com.umt.partyapp.util.DividerItemDecoration;
import com.umt.partyapp.util.PaginationAdapterCallback;
import com.umt.partyapp.util.PaginationScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListBranchActivity extends AppCompatActivity implements PaginationAdapterCallback {
    private static final String TAG = "BranchList";
    private SearchView searchView;
    private RecyclerView recyclerView;
    private BranchAdapter branchAdapter;
    private List<BranchDto> branchList;
    private FloatingActionButton floatingActionButton;
    private ProgressBar progressBar;
    JSONObject jsonObject;
    private Toolbar mTopToolbar;
    private Activity parentActivity;
    private TextView txtError;
    private Button btnRetry;
    public Long BranchId;
    LinearLayoutManager linearLayoutManager;
    LinearLayout errorLayout;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private static final int PAGE_START = 1;
    private int branchId = 1, pageSize = 5, currentPage = PAGE_START;
    private static int TOTAL_PAGES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_branch);
        recyclerView =  findViewById(R.id.branch_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = findViewById(R.id.progress_bar_list_branch);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);

        //add divider
        recyclerView.addItemDecoration(new DividerItemDecoration(LinearLayoutManager.VERTICAL, this, 16, 46));
        branchAdapter = new BranchAdapter(this);
        recyclerView.setAdapter(branchAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //progressBar = (ProgressBar) findViewById(R.id.branches_progress);
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        loadFirstPage();

        btnRetry.setOnClickListener(view -> loadFirstPage());

        floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regIntent = new Intent(ListBranchActivity.this, CreateBranchActivity.class);
                startActivity(regIntent);
            }
        });
        // progressBar.setVisibility(View.VISIBLE);
        branchList = new ArrayList<>();
        //fetchBranches();
        initToolbar();
        //pendingRequest();

        progressBar.setVisibility(View.VISIBLE);
        floatingActionButton.setAlpha(0f);

    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.branch_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Branches in your ward");
    }

    private void loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ");

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
        currentPage = PAGE_START;

//        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().fetchBranchMembers(token, branchId, pageSize,currentPage);

        getBranches().enqueue(new Callback<BranchListResponseDto>() {
            @Override
            public void onResponse(Call<BranchListResponseDto> call, Response<BranchListResponseDto> response) {
                hideErrorView();

                if (response.isSuccessful()) {
//                    Log.i(TAG, "onResponse: " + response.body().toString());
                    Log.i(TAG,response.body().getStatus().toString());
                    if(response.body().getStatus())
                    {
                        Integer total = response.body().getTotal();
                        TOTAL_PAGES = (int) Math.ceil(total / pageSize);

                        Log.i(TAG, String.valueOf(TOTAL_PAGES));

//                     Got data. Send it to adapter
                        List<BranchDataDto> results = fetchResults(response);
                        Log.i(TAG, "onResponse: " + results.toString());
                        progressBar.setVisibility(View.GONE);

                        branchAdapter.addAll(results);

                        if (currentPage <= TOTAL_PAGES) branchAdapter.addLoadingFooter();
                        else isLastPage = true;
                    }else
                    {

                    }


                }
            }

            @Override
            public void onFailure(Call<BranchListResponseDto> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }


    private Call<BranchListResponseDto> getBranches() {
        return RetrofitAuthenticatedClient.getmInstance(getApplicationContext()).getApi().getWardBranches(pageSize, currentPage);

    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);

        getBranches().enqueue(new Callback<BranchListResponseDto>() {
            @Override
            public void onResponse(Call<BranchListResponseDto> call, Response<BranchListResponseDto> response) {
//                Log.i(TAG, "onResponse: " + currentPage
//                        + (response.raw().cacheResponse() != null ? "Cache" : "Network"));

                branchAdapter.removeLoadingFooter();
                isLoading = false;

                List<BranchDataDto> results = fetchResults(response);
                branchAdapter.addAll(results);

                if (currentPage != TOTAL_PAGES) branchAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<BranchListResponseDto> call, Throwable t) {
                t.printStackTrace();
                branchAdapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }


    private List<BranchDataDto> fetchResults(Response<BranchListResponseDto> response) {
        BranchListResponseDto branchListResponseDto = response.body();
        return branchListResponseDto.getBranchdata();
    }


    @Override
    public void retryPageLoad() {
        loadNextPage();

    }


    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    //pending request alert
    public void pendingRequest() {
        customeDailog("", "You Have Been Invited To Join Love Branch by Calton Wanga", 2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //branchAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //branchAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        if (id == R.id.action_info) {
            customeDailog("Information", "If You choose to create a group you need 10 members to join the group for the group to be active", 1);

        }
        if (id == R.id.action_profile) {
            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    public void customeDailog(String header, String message, int option) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup

        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        View image = dialogView.findViewById(R.id.dialog_image);
        Button cancel = dialogView.findViewById(R.id.btn_cancel);
        Button accept = dialogView.findViewById(R.id.btn_accept);
        accept.setText("Join");
        //display one button
        if (option == 1) {
            okBtn.setVisibility(View.VISIBLE);
        } else {
            accept.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }


        //TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        //alertHeaderText.setText(header);
        alertMessage.setText(message);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Toast.makeText(ListBranchActivity.this, "Dismiss", Toast.LENGTH_SHORT).show();

            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                joinBranch();

            }
        });

    }

    //join Branch
    public void joinBranch() {

        Toast.makeText(ListBranchActivity.this, "Branch Id" + BranchId, Toast.LENGTH_SHORT).show();
        SweetAlertDialog pDialog = new SweetAlertDialog(ListBranchActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Joining Branch");
        pDialog.setCancelable(true);
        pDialog.show();
        JoinBranchDto joinBranchDto = new JoinBranchDto(BranchId);
        Call<ResponseBody> call = RetrofitAuthenticatedClient
                .getmInstance(getApplicationContext())
                .getApi()
                .JoinBranch(joinBranchDto);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.hide();

                Log.d("response main", response.toString());
                try {

                    if (response.isSuccessful()) {
                        String s = response.body().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Boolean status = Boolean.valueOf(jsonObject.getString("status"));
                        if (status) {
                            SweetAlert("Success", jsonObject.getString("message"), SweetAlertDialog.SUCCESS_TYPE);
                        } else {
                            SweetAlert("Alert", jsonObject.getString("message"), SweetAlertDialog.WARNING_TYPE);
                        }

//
//                        Intent p = new Intent(ListBranchActivity.this, HomeActivity.class);
//                        startActivity(p);

                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                    pDialog.hide();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.hide();
                Log.d("error", t.toString());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void SweetAlert(String title, String message, int icon) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ListBranchActivity.this, icon);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();

        // Hide after some seconds
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (sweetAlertDialog.isShowing()) {
                    sweetAlertDialog.dismiss();
                }
            }
        };
        sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 3000);
    }


}