package com.umt.partyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.fragment.invitemembers.InviteViaEmail;
import com.umt.partyapp.fragment.invitemembers.InviteViaOther;
import com.umt.partyapp.fragment.invitemembers.InviteViaSms;
import com.umt.partyapp.util.Tools;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.MultiContactPicker;

import java.util.ArrayList;
import java.util.List;

public class InviteMembers extends AppCompatActivity {

    private ViewPager view_pager;
    private TabLayout tab_layout;
    private SectionsPagerAdapter adapter;

    private ArrayList<ContactResult> resultsSms = new ArrayList<>();
    private ArrayList<ContactResult> resultsEmails = new ArrayList<>();
    private ArrayList<String> selectedContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_members);

        initToolbar();
        initComponent();

    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_invite_members);
//        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Invite Members");
        toolbar.setTitleTextColor(getResources().getColor(R.color.umt_white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarColor(this);
    }

    private void initComponent() {
        view_pager = findViewById(R.id.view_pager_invite_memebers);
        setupViewPager(view_pager);

        tab_layout = findViewById(R.id.tab_layout_invite_members);
        tab_layout.setupWithViewPager(view_pager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new InviteViaSms(), "Sms Invitation");
        adapter.addFragment(new InviteViaEmail(), "Email Invitation");
        adapter.addFragment(new InviteViaOther(), "Social Invites");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.d("i", String.valueOf(i));
                initiateOtherShareIntent(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_vote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void initiateOtherShareIntent(int position) {
        if (position == 2) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Carlton Wanga is iniviting you to join UMT Love Branch: https://play.google.com/store/apps/details?id=com.umt.app");
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, "Invite via");
            startActivity(shareIntent);
        }
    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == InviteViaSms.CONTACT_PICKER_REQUEST_SMS) {
            if (resultCode == RESULT_OK) {
                resultsSms.addAll(MultiContactPicker.obtainResult(data));
                Log.i("request code", String.valueOf(requestCode));

                InviteViaSms inviteViaSmsFragment = (InviteViaSms) getSupportFragmentManager().getFragments().get(0);
                inviteViaSmsFragment.onReceiveContactsResults(resultsSms);

            } else if (resultCode == RESULT_CANCELED) {
                System.out.println("User closed the picker without selecting items.");
            }
        } else if (requestCode == InviteViaEmail.CONTACT_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                resultsEmails.addAll(MultiContactPicker.obtainResult(data));
                InviteViaEmail inviteViaEmailFragment = (InviteViaEmail) getSupportFragmentManager().getFragments().get(1);
                inviteViaEmailFragment.onReceiveContactsResults(resultsEmails);
            } else if (resultCode == RESULT_CANCELED) {
                System.out.println("User closed the picker without selecting items.");
            }
        }
    }
}