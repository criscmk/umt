package com.umt.partyapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.retrofitinterface.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    public EditText mEditext, mWardEditext, mCounstituencyEditext, mCountyEditext, mFullNameEditext;
    public RadioGroup mGender;
    public ProgressBar mProgressBar;
    public LinearLayout mUserIebcDetails;
    public Button regBtn;
    public Button idValidateBtn;

    String idNumber = null;

    public final static String USER_ID = "com.umt.partyapp.activity.user_id";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mEditext = findViewById(R.id.member_reg_id);
        mProgressBar = findViewById(R.id.login_progress);

        mUserIebcDetails = findViewById(R.id.user_details_wrapper);
        mUserIebcDetails.setVisibility(View.GONE);

        mFullNameEditext = findViewById(R.id.member_reg_name);
        mCountyEditext = findViewById(R.id.member_reg_county);
        mCounstituencyEditext = findViewById(R.id.member_reg_constituency);
        mWardEditext = findViewById(R.id.member_reg_ward);

        mGender = findViewById(R.id.gender);
        regBtn = findViewById(R.id.confirm_button);
        idValidateBtn = findViewById(R.id.id_validation_button);

    }

    private void getUserIebcData() {

        idNumber = mEditext.getText().toString().trim();
        Log.i("Id number", idNumber);

        if (idNumber.isEmpty()) {
            mEditext.setError("ID/passport number is required");
            mEditext.requestFocus();
            return;
        }

        if (idNumber.length() < 7) {
            mEditext.setError("Enter a valid ID/passport number");
            mEditext.requestFocus();
            return;
        }

        Call<ResponseBody> call = RetrofitClient
                .getmInstance()
                .getApi()
                .getUserIebcData(idNumber);

        showProgressBar();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressBar();
                try {
                    if (response.isSuccessful()) {
                        String s = response.body().string();
                        Log.i("response", s);

                        JSONObject jsonObject = new JSONObject(s);

                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        Log.i("status", String.valueOf(status));

                        if (status) {
                            customeDailog("SUCCESS!!!", message);
                            regBtn.setVisibility(View.VISIBLE);
                            idValidateBtn.setVisibility(View.GONE);
//                            mEditext.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            JSONObject userIebcData = jsonObject.getJSONObject("data");
                            Log.i("data", userIebcData.toString());

                            String firstname = userIebcData.getString("firstname");
                            String secondname = userIebcData.getString("secondname");
                            String lastname = userIebcData.getString("lastname");
                            String gender = userIebcData.getString("gender");

                            String fullName = firstname + ' ' + secondname + ' ' + lastname;
                            mUserIebcDetails.setVisibility(View.VISIBLE);
                            mFullNameEditext.setText(fullName);
                            mCountyEditext.setText(userIebcData.getString("county"));
                            mCounstituencyEditext.setText(userIebcData.getString("constituency"));
                            mWardEditext.setText(userIebcData.getString("ward"));

                            if (gender.equals("male"))
                                mGender.check(R.id.radio_male);
                            else
                                mGender.check(R.id.radio_female);


                        } else {
                            customeDailog("FAILED!!!", message);

                        }

//                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressBar();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.id_validation_button:
                getUserIebcData();
                break;

            case R.id.confirm_button:
                Intent regIntent = new Intent(SignUpActivity.this, MemberRegistrationActivity.class);
                regIntent.putExtra(USER_ID, idNumber);
                startActivity(regIntent);
                finish();
                break;

            case R.id.loginbtn:
                Intent intent = new Intent(this, LogInActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    public void showSuccesAlert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        alertDialogBuilder.setTitle("Success!!!");
        alertDialogBuilder.setIcon(R.drawable.ic_success);
        alertDialogBuilder.setMessage("User validated successfully");
        alertDialogBuilder.setCancelable(true);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void customeDailog(String header, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        alertHeaderText.setText(header);
        alertMessage.setText(message);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        okBtn.setVisibility(View.VISIBLE);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        };

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 10000);

    }


}
