package com.umt.partyapp.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class VoteActivityViewModel extends ViewModel {
    private final MutableLiveData<Boolean> supportedPetition = new MutableLiveData<Boolean>();
    public void setSupportPetition(Boolean value) {
        supportedPetition.setValue(value);
    }
    public LiveData<Boolean> getSupportedPetition() {
        return supportedPetition;
    }
}
