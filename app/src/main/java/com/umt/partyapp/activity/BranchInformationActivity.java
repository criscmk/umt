package com.umt.partyapp.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.adapters.BranchMembersRecyclerAdapter;
import com.umt.partyapp.adapters.GroupMemberAdapter;
import com.umt.partyapp.apiresponse.BranchMemberDao;
import com.umt.partyapp.apiresponse.BranchMembersResponseDao;
import com.umt.partyapp.model.GroupMemberDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.retrofitinterface.RetrofitClient;
import com.umt.partyapp.util.PaginationAdapterCallback;
import com.umt.partyapp.util.PaginationScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchInformationActivity extends AppCompatActivity implements PaginationAdapterCallback {

    private static final String TAG = "BranchInformation";

    private EditText editText;
    private TextView textView, txtError;
    private Button btnRetry;

    Toolbar androidToolbar;
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    LinearLayoutManager linearLayoutManager;
    private NestedScrollView nestedScrollView;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    CoordinatorLayout mRootLayout;
    LinearLayout errorLayout;

    private BranchMembersRecyclerAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private boolean isLoading = false;
    private boolean isLastPage = false;

    private static final int PAGE_START = 1;
    private int branchId = 1, pageSize = 15, currentPage = PAGE_START;
    private static int TOTAL_PAGES = 2;

    private List<BranchMemberDao> membersArrayList;

    String token = "Bearer c08ca783-6037-49ba-995f-d949cdb210e1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_information);
        initToolbar();
        initInstances();
//        initData();
    }

    private void initToolbar() {
        androidToolbar = (Toolbar) findViewById(R.id.toolbar_android);
        setSupportActionBar(androidToolbar);
    }

    private void initInstances() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayoutAndroidExample);
        //mDrawerToggle = new ActionBarDrawerToggle(ParallaxHeaderAndroidListView.this, mDrawerLayout, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRootLayout = (CoordinatorLayout) findViewById(R.id.coordinatorRootLayout);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayoutAndroidExample);
        mCollapsingToolbarLayout.setTitle("Love Branch");

        nestedScrollView = findViewById(R.id.nested_scroll_view);
        recyclerView = findViewById(R.id.member_recyclerview);
        progressBar = findViewById(R.id.member_main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);

        adapter = new BranchMembersRecyclerAdapter(this);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        loadFirstPage();

        btnRetry.setOnClickListener(view -> loadFirstPage());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ");

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
        currentPage = PAGE_START;

//        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().fetchBranchMembers(token, branchId, pageSize,currentPage);

        getBranchMembers().enqueue(new Callback<BranchMembersResponseDao>() {
            @Override
            public void onResponse(Call<BranchMembersResponseDao> call, Response<BranchMembersResponseDao> response) {
                hideErrorView();

                if (response.isSuccessful()) {
//                    Log.i(TAG, "onResponse: " + response.body().toString());

                    Integer total = response.body().getTotal();
                    TOTAL_PAGES = (int) Math.ceil(total / pageSize);

                    Log.i(TAG, String.valueOf(TOTAL_PAGES));

//                     Got data. Send it to adapter
                    List<BranchMemberDao> results = fetchResults(response);
                    Log.i(TAG, "onResponse: " + results.toString());
                    progressBar.setVisibility(View.GONE);
                    adapter.addAll(results);

                    if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;
                }
            }

            @Override
            public void onFailure(Call<BranchMembersResponseDao> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private Call<BranchMembersResponseDao> getBranchMembers() {
        return RetrofitAuthenticatedClient.getmInstance(getApplicationContext()).getApi().fetchBranchMembers(branchId, pageSize, currentPage);

    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);

        getBranchMembers().enqueue(new Callback<BranchMembersResponseDao>() {
            @Override
            public void onResponse(Call<BranchMembersResponseDao> call, Response<BranchMembersResponseDao> response) {
//                Log.i(TAG, "onResponse: " + currentPage
//                        + (response.raw().cacheResponse() != null ? "Cache" : "Network"));

                adapter.removeLoadingFooter();
                isLoading = false;

                List<BranchMemberDao> results = fetchResults(response);
                adapter.addAll(results);

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<BranchMembersResponseDao> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }


    private List<BranchMemberDao> fetchResults(Response<BranchMembersResponseDao> response) {
        BranchMembersResponseDao branchMembersResponseDao = response.body();
        return branchMembersResponseDao.getBranchMemberDao();
    }


    @Override
    public void retryPageLoad() {
        loadNextPage();
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

//    private void initData() {
//        mListView =(ListView)findViewById(R.id.list);
//
//        dataModels= new ArrayList<>();
//        dataModels.add(new GroupMemberDto("Kitkat",  "October 31, 2013", true));
//        dataModels.add(new GroupMemberDto("Marshmallow", "October 5, 2015" ,false));
//        dataModels.add(new GroupMemberDto("Kitkat",  "October 31, 2013", false));
//        dataModels.add(new GroupMemberDto("Kitkat",  "October 31, 2013", false));
//        dataModels.add(new GroupMemberDto("Kitkat",  "October 31, 2013", false));
//        adapter= new GroupMemberAdapter(getApplicationContext(),dataModels);
//        mListView.setAdapter(adapter);
//
//
//
//
//    }

}