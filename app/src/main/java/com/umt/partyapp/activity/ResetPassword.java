package com.umt.partyapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.model.ResetPasswordDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.retrofitinterface.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassword extends AppCompatActivity {

    private EditText code, newPassword, confirmNewPassword;
    private ProgressBar progressBar;
    private Button resetPasswordBtn;
    private String phoneNumber;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);


        code = findViewById(R.id.reset_password_verification_code);
        newPassword = findViewById(R.id.new_pasword);
        confirmNewPassword = findViewById(R.id.confirm_new_password);

        progressBar = findViewById(R.id.reset_password_progress);
        resetPasswordBtn = findViewById(R.id.submit_reset);

        userId = getIntent().getStringExtra(ForgotPassword.USER_ID);
        phoneNumber = getIntent().getStringExtra(ForgotPassword.USER_PHONE);

        resetPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });
    }

    public void resetPassword() {
        String resetCode = code.getText().toString().trim();
        String newPwd = newPassword.getText().toString().trim();
        String confirmPwd = confirmNewPassword.getText().toString().trim();

        if (resetCode.isEmpty()) {
            code.setError("Reset code must not be empty");
            return;
        }

        if (newPwd.isEmpty()) {
            newPassword.setError("Enter new password");
            return;
        }

        if (confirmPwd.isEmpty()) {
            confirmNewPassword.setError("Please confirm new password!");
            return;
        }

        if (!confirmPwd.equals(newPwd)) {
            confirmNewPassword.setError("Password mismatch!");
            return;
        }

        ResetPasswordDto forgortPasswordDto = new ResetPasswordDto(userId, phoneNumber, resetCode, newPwd, confirmPwd);

        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().ResetPassword(forgortPasswordDto);

        showProgressBar();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgressBar();

                try {
                    if (response.isSuccessful()) {
                        String res = response.body().string();

                        Log.i("response", res);

                        JSONObject jsonObject = new JSONObject(res);

                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");

                        if (status) {
                            customeDailog("SUCCESS!!!", message);
                            Intent toLoginIntent = new Intent(ResetPassword.this, LogInActivity.class);
                            startActivity(toLoginIntent);
                            finish();
                        } else {
                            customeDailog("ERROR!!!", message);
                        }

                    } else {
                        Log.e("error", response.errorBody().toString());

                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void customeDailog(String header, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        alertHeaderText.setText(header);
        alertMessage.setText(message);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // Hide after some seconds
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        };

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 2000);

    }

}