package com.umt.partyapp.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.umt.partyapp.R;

import com.umt.partyapp.model.UserRegDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.retrofitinterface.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberRegistrationActivity extends AppCompatActivity {

    private EditText editTextemail, editTextphoneNumber, editTextpassword, editTextconfirmPassword;
    private TextInputLayout inputLayoutemail, inputLayoutphoneNumber, inputLayoutpassword, inputLayoutconfirmPassword;
    ProgressBar progressBar;
    Button buttonregisterMember;
    String email, phoneNumber, password, confirmPassword, idNumber;
    JSONObject jsonObject, jsonObjectuserDetails, jsonObjectresponseData, jsonObjecterrors;
    public CountryCodePicker ccp;
    EditText editTextCarrierNumber;
    public final static String UserDetails="data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_registration);
        initializewidget();
        //get data from the signup activity

        idNumber = getIntent().getStringExtra(SignUpActivity.USER_ID);
        Log.d("useruid", idNumber);

        buttonregisterMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateTextFilds();

            }
        });

    }

    //initialize widgets
    private void initializewidget() {
        editTextemail = (EditText) findViewById(R.id.member_reg_email);

        editTextpassword = (EditText) findViewById(R.id.member_reg_pasword);
        editTextconfirmPassword = (EditText) findViewById(R.id.member_reg_confirm_password);
        inputLayoutemail = (TextInputLayout) findViewById(R.id.email);

        inputLayoutpassword = (TextInputLayout) findViewById(R.id.password);
        inputLayoutconfirmPassword = (TextInputLayout) findViewById(R.id.confirmpassword);
        progressBar = (ProgressBar) findViewById(R.id.register_progress);
        buttonregisterMember = (Button) findViewById(R.id.member_register_button);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        editTextCarrierNumber = (EditText) findViewById(R.id.editText_carrierNumber);
        ccp.registerCarrierNumberEditText(editTextCarrierNumber);
        ccp.setNumberAutoFormattingEnabled(true);
    }

    private void validateTextFilds() {
        email = editTextemail.getText().toString().trim();
        password = editTextpassword.getText().toString().trim();
        confirmPassword = editTextconfirmPassword.getText().toString().trim();
        boolean isValid = false;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!email.matches(emailPattern)) {
            editTextemail.setError("Invalid email address");
            return;
        }
        if (!ccp.isValidFullNumber()) {
            editTextCarrierNumber.setError("invalid phone number");
            return;
        }
        if (password.isEmpty()) {
            editTextpassword.setError("password is required");
            editTextpassword.requestFocus();
            return;
        }

        if (confirmPassword.isEmpty()) {
            editTextconfirmPassword.setError("confirm is required");
            editTextconfirmPassword.requestFocus();
            return;
        }
        if (!password.equals(confirmPassword)) {
            editTextconfirmPassword.setError("Password Doesnt Match");
            editTextconfirmPassword.requestFocus();
            return;
        }


        registerMember();
    }

    private void registerMember() {
        progressBar.setVisibility(View.VISIBLE);


        phoneNumber = ccp.getFullNumberWithPlus();


        UserRegDto userReg = new UserRegDto(email,idNumber,phoneNumber,password,confirmPassword);


        Call<ResponseBody> call = RetrofitClient.getmInstance().getApi().RegisterUser(userReg);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                String body = null;
                try {

                    if (response.isSuccessful()) {
                        body = response.body().string();
                        Log.i("responce", body);
                        //Toast.makeText(MemberRegistrationActivity.this, "responce"+body, Toast.LENGTH_LONG).show();
                        //Toast.makeText()

                        if (response.body() != null) {

                            try {
                                parseRegData(body);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.i("error", t.toString());
            }
        });


    }

    private void parseRegData(String response) throws JSONException {

        jsonObject = new JSONObject(response);
        Boolean status = Boolean.valueOf(jsonObject.getString("status"));

        if (!status) {
            showSuccesAlert("Alert", jsonObject.getString("message"));

            JSONArray jsonArray = jsonObject.getJSONArray("errors");

            Log.i("errorsfound", jsonArray.toString());
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject error = jsonArray.getJSONObject(i);
                String field = error.getString("field");
                String message = error.getString("message");
                if (field.equals("email")) {
                    editTextemail.setError(message);
                    editTextemail.requestFocus();

                }
                if (field.equals("phoneNumber")) {
                    editTextCarrierNumber.setError(message);
                    editTextCarrierNumber.requestFocus();

                }


            }


        } else if (status) {
            Log.i("responce", response.toString());
            showSuccesAlert("Success", jsonObject.getString("message"));
            jsonObjectuserDetails = jsonObject.getJSONObject("data");
            String userId = jsonObjectuserDetails.getString("userId");
            String memberNumber = jsonObjectuserDetails.getString("memberNumber");


            Log.i("responce2",jsonObjectuserDetails.toString());

            Intent intent = new Intent(MemberRegistrationActivity.this,VerificationActivity.class);

            intent.putExtra(UserDetails,jsonObjectuserDetails.toString());
            startActivity(intent);
        }

    }

    public void showSuccesAlert(String header, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, viewGroup, false);
        Button okBtn = (Button) dialogView.findViewById(R.id.btn_ok);
        TextView alertHeaderText = (TextView) dialogView.findViewById(R.id.alert_header);
        TextView alertMessage = (TextView) dialogView.findViewById(R.id.alert_message);

        alertHeaderText.setText(header);
        alertMessage.setText(message);
        okBtn.setVisibility(View.VISIBLE);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
