package com.umt.partyapp.activity;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.umt.partyapp.R;
import com.umt.partyapp.adapters.GroupMemberAdapter;
import com.umt.partyapp.model.DataModel;
import com.umt.partyapp.model.GroupMemberDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DemoActivity extends AppCompatActivity {

    private EditText editText;
    private TextView textView;
    Toolbar androidToolbar;
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    CoordinatorLayout mRootLayout;
    List<GroupMemberDto>  memberList;
    ListView mListView;
  RecyclerView recyclerView;
    ArrayList<GroupMemberDto> dataModels;
    ListView listView;
    private static GroupMemberAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        initToolbar();
        initData();
        initInstances();
        initList();


    }
    private void initList() {

    }
    private void initToolbar() {
        androidToolbar = (Toolbar) findViewById(R.id.toolbar_android);
        setSupportActionBar(androidToolbar);
    }
    private void initInstances() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayoutAndroidExample);
        //mDrawerToggle = new ActionBarDrawerToggle(ParallaxHeaderAndroidListView.this, mDrawerLayout, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
       mListView=(ListView)findViewById(R.id.list);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRootLayout = (CoordinatorLayout) findViewById(R.id.coordinatorRootLayout);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayoutAndroidExample);
        mCollapsingToolbarLayout.setTitle("Love Branch");
    }
    private void initData() {
        listView=(ListView)findViewById(R.id.list);
        dataModels= new ArrayList<>();
        dataModels.add(new GroupMemberDto("Kitkat",  "October 31, 2013", true));
        dataModels.add(new GroupMemberDto("Lollipop","November 12, 2014",false));
        dataModels.add(new GroupMemberDto("Marshmallow", "October 5, 2015" ,false));
        adapter= new GroupMemberAdapter(getApplicationContext(),dataModels);
        listView.setAdapter(adapter);


    }
}