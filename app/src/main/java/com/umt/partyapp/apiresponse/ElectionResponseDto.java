package com.umt.partyapp.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectionResponseDto {

    @SerializedName("data")
    @Expose
    ElectionDto electionDto;
    @SerializedName("status")
    @Expose
    Integer status;
    @SerializedName("message")
    @Expose
    String message;

    public ElectionDto getElectionDto() {
        return electionDto;
    }

    public void setElectionDto(ElectionDto electionDto) {
        this.electionDto = electionDto;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
