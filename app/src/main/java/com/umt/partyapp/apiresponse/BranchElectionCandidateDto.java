package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchElectionCandidateDto implements Parcelable {
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("branchCandidateSubmissionId")
    @Expose
    private Long branchCandidateSubmissionId;
    @SerializedName("voteCount")
    @Expose
    private Long voteCount;
    @SerializedName("timeSubmitted")
    @Expose
    private String timeSubmitted;
    @SerializedName("userId")
    @Expose
    private Long userId;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("candidateName")
    @Expose
    private String candidateName;
    public final static Parcelable.Creator<BranchElectionCandidateDto> CREATOR = new Creator<BranchElectionCandidateDto>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BranchElectionCandidateDto createFromParcel(Parcel in) {
            return new BranchElectionCandidateDto(in);
        }

        public BranchElectionCandidateDto[] newArray(int size) {
            return (new BranchElectionCandidateDto[size]);
        }

    };

    protected BranchElectionCandidateDto(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.branchCandidateSubmissionId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.voteCount = ((Long) in.readValue((Long.class.getClassLoader())));
        this.timeSubmitted = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.isActive = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.candidateName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BranchElectionCandidateDto() {
    }

    public BranchElectionCandidateDto(Long id, Long voteCount, String candidateName) {
        this.id = id;
        this.voteCount = voteCount;
        this.candidateName = candidateName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBranchCandidateSubmissionId() {
        return branchCandidateSubmissionId;
    }

    public void setBranchCandidateSubmissionId(Long branchCandidateSubmissionId) {
        this.branchCandidateSubmissionId = branchCandidateSubmissionId;
    }

    public Long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Long voteCount) {
        this.voteCount = voteCount;
    }

    public String getTimeSubmitted() {
        return timeSubmitted;
    }

    public void setTimeSubmitted(String timeSubmitted) {
        this.timeSubmitted = timeSubmitted;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(branchCandidateSubmissionId);
        dest.writeValue(voteCount);
        dest.writeValue(timeSubmitted);
        dest.writeValue(userId);
        dest.writeValue(isActive);
        dest.writeValue(candidateName);
    }

    public int describeContents() {
        return 0;
    }
}
