package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectionDto implements Parcelable {
    @SerializedName("levelName")
    @Expose
    private String levelName;
    @SerializedName("currentLeader")
    @Expose
    private String currentLeader = null;
    @SerializedName("candidateName")
    @Expose
    private String candidateName = null;
    @SerializedName("electionLevel")
    @Expose
    private String electionLevel;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("hasVoted")
    @Expose
    private Boolean hasVoted = false;
    @SerializedName("hasSubmitted")
    @Expose
    private Boolean hasSubmitted = false;
    @SerializedName("electionStatus")
    @Expose
    private String electionStatus;
    @SerializedName("electionId")
    @Expose
    private Long electionId;
    public final static Parcelable.Creator<ElectionDto> CREATOR = new Creator<ElectionDto>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ElectionDto createFromParcel(Parcel in) {
            return new ElectionDto(in);
        }

        public ElectionDto[] newArray(int size) {
            return (new ElectionDto[size]);
        }

    };

    protected ElectionDto(Parcel in) {

        this.electionLevel = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
        this.hasVoted = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.hasSubmitted = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.electionStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.electionId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.levelName = ((String) in.readValue((String.class.getClassLoader())));
        this.candidateName = ((String) in.readValue((String.class.getClassLoader())));
        this.currentLeader = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ElectionDto() {
    }

    public String getElectionLevel() {
        return electionLevel;
    }

    public void setElectionLevel(String electionLevel) {
        this.electionLevel = electionLevel;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getHasVoted() {
        return hasVoted;
    }

    public void setHasVoted(Boolean hasVoted) {
        this.hasVoted = hasVoted;
    }

    public String getElectionStatus() {
        return electionStatus;
    }

    public void setElectionStatus(String electionStatus) {
        this.electionStatus = electionStatus;
    }

    public Long getElectionId() {
        return electionId;
    }

    public void setElectionId(Long electionId) {
        this.electionId = electionId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getCurrentLeader() {
        return currentLeader;
    }

    public void setCurrentLeader(String currentLeader) {
        this.currentLeader = currentLeader;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public Boolean getHasSubmitted() {
        return hasSubmitted;
    }

    public void setHasSubmitted(Boolean hasSubmitted) {
        this.hasSubmitted = hasSubmitted;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(electionLevel);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
        dest.writeValue(hasVoted);
        dest.writeValue(hasSubmitted);
        dest.writeValue(electionStatus);
        dest.writeValue(electionId);
        dest.writeValue(levelName);
        dest.writeValue(candidateName);
        dest.writeValue(currentLeader);
    }

    public int describeContents() {
        return 0;
    }

}