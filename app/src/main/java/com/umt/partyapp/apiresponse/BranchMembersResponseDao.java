package com.umt.partyapp.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BranchMembersResponseDao {

    @SerializedName("data")
    @Expose
    private List<BranchMemberDao> branchMemberDao = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("errors")
    @Expose
    private Object errors;
    @SerializedName("total")
    @Expose
    private Integer total;


    public BranchMembersResponseDao() {
    }


    public BranchMembersResponseDao(List<BranchMemberDao> branchMemberDao, Boolean status, String message, Object errors, Integer total) {
        super();
        this.branchMemberDao = branchMemberDao;
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.total = total;
    }

    public List<BranchMemberDao> getBranchMemberDao() {
        return branchMemberDao;
    }

    public void setBranchMemberDao(List<BranchMemberDao> branchMemberDao) {
        this.branchMemberDao = branchMemberDao;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
