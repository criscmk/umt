package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectionCandidateDto implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("candidateSubmissionId")
    @Expose
    private Long candidateSubmissionId;
    @SerializedName("userId")
    @Expose
    private Long userId;
    @SerializedName("voteCount")
    @Expose
    private Long voteCount;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("timeSubmitted")
    @Expose
    private String timeSubmitted;
    @SerializedName("levelId")
    @Expose
    private Long levelId;
    @SerializedName("electionLevelId")
    @Expose
    private Long electionLevelId;
    @SerializedName("candidateName")
    @Expose
    private String candidateName;
    @SerializedName("levelName")
    @Expose
    private String levelName;
    public final static Parcelable.Creator<ElectionCandidateDto> CREATOR = new Creator<ElectionCandidateDto>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ElectionCandidateDto createFromParcel(Parcel in) {
            return new ElectionCandidateDto(in);
        }

        public ElectionCandidateDto[] newArray(int size) {
            return (new ElectionCandidateDto[size]);
        }

    }
            ;

    protected ElectionCandidateDto(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.candidateSubmissionId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.userId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.voteCount = ((Long) in.readValue((Long.class.getClassLoader())));
        this.isActive = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.timeSubmitted = ((String) in.readValue((String.class.getClassLoader())));
        this.levelId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.electionLevelId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.candidateName = ((String) in.readValue((String.class.getClassLoader())));
        this.levelName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ElectionCandidateDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCandidateSubmissionId() {
        return candidateSubmissionId;
    }

    public void setCandidateSubmissionId(Long candidateSubmissionId) {
        this.candidateSubmissionId = candidateSubmissionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Long voteCount) {
        this.voteCount = voteCount;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getTimeSubmitted() {
        return timeSubmitted;
    }

    public void setTimeSubmitted(String timeSubmitted) {
        this.timeSubmitted = timeSubmitted;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public Long getElectionLevelId() {
        return electionLevelId;
    }

    public void setElectionLevelId(Long electionLevelId) {
        this.electionLevelId = electionLevelId;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(candidateSubmissionId);
        dest.writeValue(userId);
        dest.writeValue(voteCount);
        dest.writeValue(isActive);
        dest.writeValue(timeSubmitted);
        dest.writeValue(levelId);
        dest.writeValue(electionLevelId);
        dest.writeValue(candidateName);
        dest.writeValue(levelName);
    }

    public int describeContents() {
        return 0;
    }

}