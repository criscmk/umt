package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchElectionCallResponseDto implements Parcelable {

    @SerializedName("coordinatorName")
    @Expose
    private String coordinatorName;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("electionState")
    @Expose
    private String electionState;
    @SerializedName("dateElected")
    @Expose
    private String dateElected;
    @SerializedName("electionMethod")
    @Expose
    private String electionMethod;
    @SerializedName("branchId")
    @Expose
    private Long branchId;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("calledBy")
    @Expose
    private String calledBy;
    @SerializedName("dateCalled")
    @Expose
    private String dateCalled;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("electionId")
    @Expose
    private Long electionId;
    @SerializedName("hasSupported")
    @Expose
    private Boolean hasSupportedPetition;
    @SerializedName("hasSubmitted")
    @Expose
    private Boolean hasSubmitted;
    @SerializedName("hasVoted")
    @Expose
    private Boolean hasVoted;


    public final static Parcelable.Creator<BranchElectionCallResponseDto> CREATOR = new Creator<BranchElectionCallResponseDto>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BranchElectionCallResponseDto createFromParcel(Parcel in) {
            return new BranchElectionCallResponseDto(in);
        }

        public BranchElectionCallResponseDto[] newArray(int size) {
            return (new BranchElectionCallResponseDto[size]);
        }

    };


    protected BranchElectionCallResponseDto(Parcel in) {
        this.coordinatorName = ((String) in.readValue((String.class.getClassLoader())));
        this.reason = ((String) in.readValue((String.class.getClassLoader())));
        this.isActive = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.dateElected = ((String) in.readValue((String.class.getClassLoader())));
        this.electionMethod = ((String) in.readValue((String.class.getClassLoader())));
        this.branchId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.calledBy = ((String) in.readValue((String.class.getClassLoader())));
        this.dateCalled = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
        this.electionId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.electionState = ((String) in.readValue((String.class.getClassLoader())));
        this.hasSubmitted = ((Boolean) in.readValue((String.class.getClassLoader())));
        this.hasVoted = ((Boolean) in.readValue((String.class.getClassLoader())));
        this.hasSupportedPetition = ((Boolean) in.readValue((String.class.getClassLoader())));
    }

    public BranchElectionCallResponseDto() {

    }

    public String getCoordinatorName() {
        return coordinatorName;
    }

    public void setCoordinatorName(String coordinatorName) {
        this.coordinatorName = coordinatorName;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getDateElected() {
        return dateElected;
    }

    public void setDateElected(String dateElected) {
        this.dateElected = dateElected;
    }

    public String getElectionMethod() {
        return electionMethod;
    }

    public void setElectionMethod(String electionMethod) {
        this.electionMethod = electionMethod;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getCalledBy() {
        return calledBy;
    }

    public void setCalledBy(String calledBy) {
        this.calledBy = calledBy;
    }

    public String getDateCalled() {
        return dateCalled;
    }

    public void setDateCalled(String dateCalled) {
        this.dateCalled = dateCalled;
    }

    public Long getElectionId() {
        return electionId;
    }

    public void setElectionId(Long electionId) {
        this.electionId = electionId;
    }

    public String getElectionState() {
        return electionState;
    }

    public void setElectionState(String electionState) {
        this.electionState = electionState;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getHasSupportedPetition() {
        return hasSupportedPetition;
    }

    public void setHasSupportedPetition(Boolean hasSupportedPetition) {
        this.hasSupportedPetition = hasSupportedPetition;
    }

    public Boolean getHasSubmitted() {
        return hasSubmitted;
    }

    public void setHasSubmitted(Boolean hasSubmitted) {
        this.hasSubmitted = hasSubmitted;
    }

    public Boolean getHasVoted() {
        return hasVoted;
    }

    public void setHasVoted(Boolean hasVoted) {
        this.hasVoted = hasVoted;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(coordinatorName);
        dest.writeValue(reason);
        dest.writeValue(isActive);
        dest.writeValue(dateElected);
        dest.writeValue(electionMethod);
        dest.writeValue(branchId);
        dest.writeValue(calledBy);
        dest.writeValue(dateCalled);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
        dest.writeValue(electionId);
        dest.writeValue(electionState);
        dest.writeValue(hasSubmitted);
        dest.writeValue(hasVoted);
        dest.writeValue(hasSupportedPetition);
    }

    public int describeContents() {
        return 0;
    }
}
