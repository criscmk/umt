package com.umt.partyapp.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BranchElectionDao {

    @SerializedName("coordinatorName")
    @Expose
    private String coordinatorName;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("electionState")
    @Expose
    private String electionState;
    @SerializedName("dateElected")
    @Expose
    private String dateElected;
    @SerializedName("electionMethod")
    @Expose
    private String electionMethod;
    @SerializedName("branchId")
    @Expose
    private Long branchId;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("calledBy")
    @Expose
    private String calledBy;
    @SerializedName("dateCalled")
    @Expose
    private String dateCalled;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("electionId")
    @Expose
    private Long electionId;
    @SerializedName("hasSupported")
    @Expose
    private Boolean hasSupportedPetition;
    @SerializedName("hasSubmitted")
    @Expose
    private Boolean hasSubmitted;
    @SerializedName("hasVoted")
    @Expose
    private Boolean hasVoted;

    public BranchElectionDao() {
    }


    public BranchElectionDao(String coordinatorName, Boolean isActive, String electionState, String dateElected, String electionMethod, Long branchId, Boolean hasSupportedPetition) {
        super();
        this.coordinatorName = coordinatorName;
        this.isActive = isActive;
        this.electionState = electionState;
        this.dateElected = dateElected;
        this.electionMethod = electionMethod;
        this.branchId = branchId;
        this.hasSupportedPetition = hasSupportedPetition;
    }

    public String getCoordinatorName() {
        return coordinatorName;
    }

    public void setCoordinatorName(String coordinatorName) {
        this.coordinatorName = coordinatorName;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getDateElected() {
        return dateElected;
    }

    public void setDateElected(String dateElected) {
        this.dateElected = dateElected;
    }

    public String getElectionMethod() {
        return electionMethod;
    }

    public void setElectionMethod(String electionMethod) {
        this.electionMethod = electionMethod;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getElectionState() {
        return electionState;
    }

    public void setElectionState(String electionState) {
        this.electionState = electionState;
    }

    public Boolean getHasSupportedPetition() {
        return hasSupportedPetition;
    }

    public void setHasSupportedPetition(Boolean hasSupportedPetition) {
        this.hasSupportedPetition = hasSupportedPetition;
    }

    public Boolean getHasSubmitted() {
        return hasSubmitted;
    }

    public void setHasSubmitted(Boolean hasSubmitted) {
        this.hasSubmitted = hasSubmitted;
    }

    public Boolean getHasVoted() {
        return hasVoted;
    }

    public void setHasVoted(Boolean hasVoted) {
        this.hasVoted = hasVoted;
    }


}
