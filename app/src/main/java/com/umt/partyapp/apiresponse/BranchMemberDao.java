package com.umt.partyapp.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchMemberDao {

    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("date_joined")
    @Expose
    private String dateJoined;
    @SerializedName("admin")
    @Expose
    private Boolean admin;

    /**
     * No args constructor for use in serialization
     *
     */
    public BranchMemberDao() {
    }

    /**
     *
     * @param dateJoined
     * @param fullName
     * @param admin
     */
    public BranchMemberDao(String fullName, String dateJoined, Boolean admin) {
        super();
        this.fullName = fullName;
        this.dateJoined = dateJoined;
        this.admin = admin;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

}
