package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectionResultsResponseDto implements Parcelable {

    @SerializedName("voteCount")
    @Expose
    private Integer voteCount;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("candidateName")
    @Expose
    private String candidateName;
    public final static Parcelable.Creator<ElectionResultsResponseDto> CREATOR = new Creator<ElectionResultsResponseDto>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ElectionResultsResponseDto createFromParcel(Parcel in) {
            return new ElectionResultsResponseDto(in);
        }

        public ElectionResultsResponseDto[] newArray(int size) {
            return (new ElectionResultsResponseDto[size]);
        }

    };

    protected ElectionResultsResponseDto(Parcel in) {
        this.voteCount = ((Integer) in.readValue((Long.class.getClassLoader())));
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.candidateName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ElectionResultsResponseDto() {
    }

    public ElectionResultsResponseDto(Integer voteCount, Long id, String candidateName) {
        this.voteCount = voteCount;
        this.id = id;
        this.candidateName = candidateName;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(voteCount);
        dest.writeValue(id);
        dest.writeValue(candidateName);
    }

    public int describeContents() {
        return 0;
    }

}