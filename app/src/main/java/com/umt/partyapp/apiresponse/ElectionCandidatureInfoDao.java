package com.umt.partyapp.apiresponse;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectionCandidatureInfoDao implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("userId")
    @Expose
    private Long userId;
    @SerializedName("slogan")
    @Expose
    private String slogan;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("fileUrl")
    @Expose
    private Object fileUrl;
    @SerializedName("imageUrl")
    @Expose
    private Object imageUrl;
    @SerializedName("candidateName")
    @Expose
    private String candidateName;
    public final static Parcelable.Creator<ElectionCandidatureInfoDao> CREATOR = new Creator<ElectionCandidatureInfoDao>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ElectionCandidatureInfoDao createFromParcel(Parcel in) {
            return new ElectionCandidatureInfoDao(in);
        }

        public ElectionCandidatureInfoDao[] newArray(int size) {
            return (new ElectionCandidatureInfoDao[size]);
        }

    }
            ;

    protected ElectionCandidatureInfoDao(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.userId = ((Long) in.readValue((Long.class.getClassLoader())));
        this.slogan = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.fileUrl = ((Object) in.readValue((Object.class.getClassLoader())));
        this.imageUrl = ((Object) in.readValue((Object.class.getClassLoader())));
        this.candidateName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ElectionCandidatureInfoDao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(Object fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Object getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Object imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(slogan);
        dest.writeValue(description);
        dest.writeValue(fileUrl);
        dest.writeValue(imageUrl);
        dest.writeValue(candidateName);
    }

    public int describeContents() {
        return 0;
    }

}