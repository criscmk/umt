package com.umt.partyapp.handlers;


import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;

import kotlinx.android.parcel.Parcelize;


@Parcelize
public class ChatHandler extends Handler{
    private AppReceiver appReceiver;
    public ChatHandler(AppReceiver receiver) {
        appReceiver = receiver;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        appReceiver.onReceiveResult(msg);
    }


    public interface AppReceiver {
        void onReceiveResult(Message message);
    }
}
