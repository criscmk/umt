package com.umt.partyapp.model;

public class GroupMemberDto {
    private String description;
    private Boolean cordinator;
    private String name;
    private String image;

    public GroupMemberDto(String name, String description, Boolean cordinator) {
        this.name = name;
        this.description = description;
        this.cordinator = cordinator;
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCordinator() {
        return cordinator;
    }

    public void setCordinator(Boolean cordinator) {
        this.cordinator = cordinator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }










}