package com.umt.partyapp.model;

public class ContactInvitationDisplay {
    String displayName;
    String phoneNumber;
    String email;

    public ContactInvitationDisplay() {
    }

    public ContactInvitationDisplay(String displayName, String phoneNumber, String email) {
        this.displayName = displayName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
