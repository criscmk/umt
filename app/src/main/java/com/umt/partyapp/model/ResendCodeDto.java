package com.umt.partyapp.model;

public class ResendCodeDto {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ResendCodeDto(String userId) {
        this.userId = userId;
    }



}
