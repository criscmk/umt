package com.umt.partyapp.model;

public class VoteDto {
    Long electionId;
    Long candidateId;

    public VoteDto(Long electionId, Long candidateId) {
        this.electionId = electionId;
        this.candidateId = candidateId;
    }

    public Long getElectionId() {
        return electionId;
    }

    public void setElectionId(Long electionId) {
        this.electionId = electionId;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }
}
