package com.umt.partyapp.model;

public class UserLoginDto {
    String password;
    String username;
    final String client_id = "member_app";
    final String client_secret = "Zq8j#Y_eVYpNT@ZP";
    final String grant_type = "password";


    public UserLoginDto(String username, String password) {
        this.username = username;
        this.password = password;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
