package com.umt.partyapp.model;

public class MemberlistDto {
    private String name;
    private String date;
    private String role;

    public MemberlistDto(String name, String date, String role) {
        this.name = name;
        this.date = date;
        this.role = role;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }




}
