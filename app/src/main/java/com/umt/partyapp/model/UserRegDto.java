package com.umt.partyapp.model;

public class UserRegDto {

    private String email;
    private String phoneNumber;
    private String password;
    private String confirmPassword;
    private String idNumber;

    public UserRegDto(String email, String idNumber, String phoneNumber, String password, String confirmPassword) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.idNumber = idNumber;
    }
    public String getEmail() {
        return email;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }


    public void setEmail(String email) {
        this.email = email;
    }
    public String getIdNumber() {
        return idNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }




}
