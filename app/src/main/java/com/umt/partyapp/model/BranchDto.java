package com.umt.partyapp.model;

public class BranchDto {
    private String name;
    private String description;
    private Long id;
    private boolean expanded = false;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public boolean isExpanded() {
        return expanded;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

}
