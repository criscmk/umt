package com.umt.partyapp.model;

public class BranchElectionPetitionSupportVote {
    Long electionId;
    String voteStatus;

    public BranchElectionPetitionSupportVote(Long electionId, String voteStatus) {
        this.electionId = electionId;
        this.voteStatus = voteStatus;
    }

    public void setElectionId(Long electionId) {
        this.electionId = electionId;
    }

    public void setVoteStatus(String voteStatus) {
        this.voteStatus = voteStatus;
    }
}
