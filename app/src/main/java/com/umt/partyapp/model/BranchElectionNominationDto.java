package com.umt.partyapp.model;

public class BranchElectionNominationDto {
    Long electionId;

    public BranchElectionNominationDto(Long electionId) {
        this.electionId = electionId;
    }

    public Long getElectionId() {
        return electionId;
    }

    public void setElectionId(Long electionId) {
        this.electionId = electionId;
    }
}
