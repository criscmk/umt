package com.umt.partyapp.model;

import java.io.Serializable;

public class MessageDto implements Serializable{
	private long id;
	private String date;
	private String content;
	private String username;
	private String profileImage;
	private boolean fromMe;
	private boolean showTime = true;
	private String roomId;



	public MessageDto(long id, String content, boolean fromMe, String date) {
		this.id = id;
		this.date = date;
		this.content = content;
		this.fromMe = fromMe;

	}

	public MessageDto(long id, String content, boolean fromMe, boolean showTime, String username,String profileImage,String messageId,String date) {
		this.id = id;
		this.date = date;
		this.content = content;
		this.fromMe = fromMe;
		this.username = username;
		this.profileImage = profileImage;
		this.roomId = roomId;
		this.messageId = messageId;
		this.showTime = showTime;

	}
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	private String messageId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(Long String) {
		this.roomId = roomId;
	}

	public long getId() {
		return id;
	}

	public String getDate() {
		return date;
	}

	public String getContent() {
		return content;
	}

	public boolean isFromMe() {
		return fromMe;
	}

	public boolean isShowTime() {
		return showTime;
	}
}