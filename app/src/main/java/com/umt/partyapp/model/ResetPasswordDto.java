package com.umt.partyapp.model;

public class ResetPasswordDto {

    String userId;
    String phoneNumber;
    String code;
    String password;
    String confirmPassword;


    public ResetPasswordDto(String userId, String phoneNumber, String code, String password, String confirmPassword) {
        this.userId = userId;
        this.phoneNumber = phoneNumber;
        this.code = code;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
