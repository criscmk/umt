package com.umt.partyapp.model;

public class AccessToken {
    private String accessToken;
    private Integer expiresIn;
    private Long accessTokenTimeCreated;

    public AccessToken(String accessToken, Integer expiresIn, Long accessTokenTimeCreated) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.accessTokenTimeCreated = accessTokenTimeCreated;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getAccessTokenTimeCreated() {
        return accessTokenTimeCreated;
    }

    public void setAccessTokenTimeCreated(Long accessTokenTimeCreated) {
        this.accessTokenTimeCreated = accessTokenTimeCreated;
    }
}
