package com.umt.partyapp.model;

public class UserLoginResponceDto {
    private String Access_Token = "";
    private String Refresh_Token = "";
    private Integer Expires_In = 0;

    public UserLoginResponceDto(String access_Token, String refresh_Token, Integer expires_In) {
        Access_Token = access_Token;
        Refresh_Token = refresh_Token;
        Expires_In = expires_In;
    }



    public String getAccess_Token() {
        return Access_Token;
    }

    public void setAccess_Token(String access_Token) {
        Access_Token = access_Token;
    }

    public String getRefresh_Token() {
        return Refresh_Token;
    }

    public void setRefresh_Token(String refresh_Token) {
        Refresh_Token = refresh_Token;
    }

    public Integer getExpires_In() {
        return Expires_In;
    }

    public void setExpires_In(Integer expires_In) {
        Expires_In = expires_In;
    }



}
