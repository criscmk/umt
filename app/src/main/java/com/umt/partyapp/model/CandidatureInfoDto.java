package com.umt.partyapp.model;

public class CandidatureInfoDto {
    String slogan;
    String description;
    String imageUrl;
    String fileUrl;

    public CandidatureInfoDto(String slogan, String description) {
        this.slogan = slogan;
        this.description = description;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
