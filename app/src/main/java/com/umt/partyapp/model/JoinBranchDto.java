package com.umt.partyapp.model;

public class JoinBranchDto {
    private Long branchId;

    public JoinBranchDto(Long branchId) {
        this.branchId = branchId;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }


}
