package com.umt.partyapp.model;

import java.util.ArrayList;

public class SmsInviteContactsDto {

    ArrayList<InviteViaSmsDto> contacts;

    public ArrayList<InviteViaSmsDto> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<InviteViaSmsDto> contacts) {
        this.contacts = contacts;
    }
}
