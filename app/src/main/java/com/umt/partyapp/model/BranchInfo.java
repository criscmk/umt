package com.umt.partyapp.model;

public class BranchInfo {
    private String branchName;
    private String branchDescription;
    private boolean expanded;

    public BranchInfo(String branchName, String branchDescription) {
        this.branchName = branchName;
        this.branchDescription = branchDescription;
        this.expanded = false;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchDescription() {
        return branchDescription;
    }

    public void setBranchDescription(String branchDescription) {
        this.branchDescription = branchDescription;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }


}
