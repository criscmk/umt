package com.umt.partyapp.model;

public class ForumsDto {
    String name;
    String description;
    String time;
    String Counter;
    private boolean expanded;


    public ForumsDto(String name, String description, String time,String counter) {
        this.name = name;
        this.description = description;
        this.time = time;
        Counter = counter;
        this.expanded = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCounter() {
        return Counter;
    }

    public void setCounter(String counter) {
        Counter = counter;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

}
