package com.umt.partyapp.model;

public class BranchElectionReasonDto {
    String reason;

    public BranchElectionReasonDto(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
