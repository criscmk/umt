package com.umt.partyapp.model;

public class InviteViaSmsDto {

    String phoneNumber;

   public InviteViaSmsDto(String phoneNumber) {
       this.phoneNumber = phoneNumber;
   }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
