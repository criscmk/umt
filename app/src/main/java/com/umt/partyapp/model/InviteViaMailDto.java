package com.umt.partyapp.model;

public class InviteViaMailDto {

    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
