package com.umt.partyapp.sessionmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.umt.partyapp.activity.LogInActivity;
import com.umt.partyapp.model.AccessToken;
import com.umt.partyapp.model.UserLoginResponceDto;

import java.sql.Ref;


public class Sessionmangementsharedpref {

    private static final String SHARED_PREF_NAME = "sessionPref";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String REFRESH_TOKEN = "refreshToken";
    private static final String IS_LOGGED_IN = "isLoggedIn";
    private static final String ACCESS_TOKEN_TIME_CREATED = "accessTokenTimeCreated";
    private static final String EXPIRES_IN = "expiresIn";


    private static Sessionmangementsharedpref mInstance;
    private static Context mCtx;

    private Activity activity;

    public Sessionmangementsharedpref(Context context) {
        mCtx = context;
    }

    public static synchronized Sessionmangementsharedpref getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Sessionmangementsharedpref(context);
        }
        return mInstance;
    }


    public void userLogin(UserLoginResponceDto userCredentials) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, userCredentials.getAccess_Token());
        editor.putString(REFRESH_TOKEN, userCredentials.getRefresh_Token());
        editor.putLong(ACCESS_TOKEN_TIME_CREATED, System.currentTimeMillis());
        editor.putInt(EXPIRES_IN, userCredentials.getExpires_In());
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.apply();
    }

    public void updateAccessToken(String accessToken, Integer expiresIn) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, accessToken);
        editor.putInt(EXPIRES_IN, expiresIn);
        editor.putLong(ACCESS_TOKEN_TIME_CREATED, System.currentTimeMillis());
        editor.apply();
    }

    public AccessToken getStoredAccessToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new AccessToken(sharedPreferences.getString(ACCESS_TOKEN, null),
                sharedPreferences.getInt(EXPIRES_IN, 0),
                sharedPreferences.getLong(ACCESS_TOKEN_TIME_CREATED, 0L));

    }

    public String getRefreshToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(REFRESH_TOKEN, null);
    }


    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences.getString(REFRESH_TOKEN, null) != null);
    }

    //this method will give the logged in user
    public UserLoginResponceDto getUserCredentials() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new UserLoginResponceDto(

                sharedPreferences.getString(ACCESS_TOKEN, null),
                sharedPreferences.getString(REFRESH_TOKEN, null),
                sharedPreferences.getInt(EXPIRES_IN, 0)

        );
    }
    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        Intent intent = new Intent(mCtx, LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mCtx.startActivity(intent);

    }
}
