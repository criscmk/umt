package com.umt.partyapp.util;

import android.content.Context;
import android.util.Log;

import com.umt.partyapp.model.AccessToken;
import com.umt.partyapp.sessionmanager.Sessionmangementsharedpref;

public class TokenManager {

    private Context context;

    public TokenManager(Context context) {
        this.context = context;

    }

    public String getValidAccessToken(Context context) {

        Sessionmangementsharedpref sessionmangementsharedpref = new Sessionmangementsharedpref(context);
        AccessToken accessToken = sessionmangementsharedpref.getStoredAccessToken();

        Log.d("oo", accessToken.getExpiresIn().toString());
        Log.d("oo", (accessToken.getAccessTokenTimeCreated() + (accessToken.getExpiresIn() * 1000))+"");

        Boolean accessTokenValid = (accessToken.getAccessTokenTimeCreated() + (accessToken.getExpiresIn() * 1000)) > System.currentTimeMillis();

        String accessTokenValue = accessTokenValid ? accessToken.getAccessToken() : null;
        return accessTokenValue;
    }
}
