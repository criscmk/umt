package com.umt.partyapp.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetBehavior;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.umt.partyapp.R;

public class BottomSheetDialog {
    private BottomSheetBehavior mBehavior;
    private android.support.design.widget.BottomSheetDialog mBottomSheetDialog;
    private Activity activity;

    public BottomSheetDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {
        mBottomSheetDialog = new android.support.design.widget.BottomSheetDialog(activity, R.style.AppBottomSheetDialogTheme);
        mBottomSheetDialog.setContentView(R.layout.sheet_basic);
        mBottomSheetDialog.setCancelable(true);


        mBottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        ((EditText) mBottomSheetDialog.findViewById(R.id.et_manifesto)).setText(R.string.middle_lorem_ipsum);
        (mBottomSheetDialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        (mBottomSheetDialog.findViewById(R.id.bt_save)).setOnClickListener(v -> Toast.makeText(activity, "Details clicked", Toast.LENGTH_SHORT).show());

        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }
}
