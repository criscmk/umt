package com.umt.partyapp.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.ListBranchActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CustomAlerts {

    private Context context;



    public CustomAlerts(Context context) {
        this.context = context;

    }
    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void SweetAlert(Context context, String title, String message, int icon) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, icon);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
    }

}
