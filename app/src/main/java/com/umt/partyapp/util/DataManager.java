package com.umt.partyapp.util;

import com.umt.partyapp.model.BranchInfo;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    private static DataManager dataInstance = null;

    private List<BranchInfo> branches = new ArrayList<>();

    public static DataManager getInstance() {
        if (dataInstance == null) {
            dataInstance = new DataManager();
            dataInstance.initializeBranches();
        }
        return dataInstance;
    }

    public List<BranchInfo> getBranches() {
        return branches;
    }

    public void initializeBranches() {
        branches.add(new BranchInfo("Tana River Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Taita/Taveta Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Karen Ward Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Langata Ward Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Westlands Ward Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Rongai Ward Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Kajiado Ward Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
        branches.add(new BranchInfo("Simba Salon Branch", "Created on 6th of July to address the economic and socioeconomic issues that people of this ward face each and everyday."));
    }
}
