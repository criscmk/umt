package com.umt.partyapp.util;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.umt.partyapp.R;

public class InputDialog {
    Activity activity;
    Dialog dialog;

    public InputDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.enter_phone_number);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final EditText phoneNumber = (EditText) dialog.findViewById(R.id.ed_phone_number);
        final Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit_number);
        final Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel_enter_number);

        btnSubmit.setOnClickListener(v -> {
            String number = phoneNumber.getText().toString().trim();


        });
    }


}
