package com.umt.partyapp.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    public static final int Horizontal_List = LinearLayoutManager.HORIZONTAL;
    public static final int Vertical_List = LinearLayoutManager.VERTICAL;

    private Drawable itemDivider;
    private int listOrientation;
    private Context context;
    private int margin;
    private int left_margin;


    public DividerItemDecoration(int orientation, Context context,  int margin, int left_margin) {
        this.context = context;
        this.margin = margin;
        this.left_margin = left_margin;

        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        itemDivider = a.getDrawable(0);
        a.recycle();

        setOrientation(orientation);
    }

    private void setOrientation(int orientation) {
        if (listOrientation != Horizontal_List && listOrientation != Vertical_List) {
            throw new IllegalArgumentException("Invalid");
        }
        listOrientation = orientation;


    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent,
                           @NonNull RecyclerView.State state) {

        if (listOrientation == Vertical_List) {
            drawVerticalRecycler(c, parent);
        } else {
            drawHorizontalRecycler(c, parent);
        }

//        super.onDrawOver(c, parent, state);
    }

    private void drawVerticalRecycler(Canvas c, RecyclerView parent) {

        final int left = dpToPx(left_margin);
        final int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + itemDivider.getIntrinsicHeight();

            itemDivider.setBounds(left + dpToPx(margin), top, right - dpToPx(margin), bottom);
            itemDivider.draw(c);
        }
    }


    private void drawHorizontalRecycler(Canvas c, RecyclerView parent) {

        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            final int left = child.getRight() + params.rightMargin;
            final int right = left + itemDivider.getIntrinsicHeight();

            itemDivider.setBounds(left, top + dpToPx(margin), right, bottom - dpToPx(margin));
            itemDivider.draw(c);
        }
    }

    private int dpToPx(int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (listOrientation == Vertical_List) {
            outRect.set(0, 0, 0, itemDivider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, itemDivider.getIntrinsicWidth(), 0);
        }
    }
}
