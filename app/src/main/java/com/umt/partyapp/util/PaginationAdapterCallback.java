package com.umt.partyapp.util;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}
