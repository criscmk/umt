package com.umt.partyapp.interfaces;

import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;

public interface BranchElectionInterface {

    void dataToFragment(BranchElectionCallResponseDto branchElection);

    void currentFragment(int fragmentPosition);
}
