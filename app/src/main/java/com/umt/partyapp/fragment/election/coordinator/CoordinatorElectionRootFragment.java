package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CoordinatorElectionRootFragment extends Fragment {
    private static final String TAG = "Coordinator";

    private String electionStatus;
    Boolean hasSupportedPetition = false;
    Boolean hasSubmitted = false;
    Boolean hasVoted = false;

    private BranchElectionCallResponseDto branchElectionDao;
    private LoadingDialog loadingDialog;

    private Bundle bundle;

    public CoordinatorElectionRootFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_coordinator_election_root_fragment, container, false);

        loadingDialog = new LoadingDialog(getActivity());
        branchElectionDao = new BranchElectionCallResponseDto();

        getBranchElectionInfo();
        return rootView;
    }

    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.e(TAG, "load data here");

        }else{
            Log.e(TAG, "fragment is no longer visible");

        }
    }

    public void getBranchElectionInfo() {

        loadingDialog.showDialog();

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getBranchElection();

        call.enqueue(new Callback<ResponseBody>() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {

                    String s;
                    int status;
                    try {

                        s = response.body().string();
                        JSONObject jsonObject = new JSONObject(s);
                        status = Integer.parseInt(jsonObject.getString("status"));
                        Log.d(TAG, "status " + status);

                        if (status == 200) {
                            JSONObject electionData = new JSONObject(jsonObject.getString("data"));

                            Gson gson = new Gson();

                            branchElectionDao = gson.fromJson(String.valueOf(electionData), BranchElectionCallResponseDto.class);
                            electionStatus = branchElectionDao.getElectionState();
                            hasSupportedPetition = branchElectionDao.getHasSupportedPetition();
                            hasSubmitted = branchElectionDao.getHasSubmitted();
                            hasVoted = branchElectionDao.getHasVoted();

                            setActiveFragment(electionStatus);
                            Log.d(TAG, "id " + branchElectionDao.getElectionId());
                            Log.d(TAG, "name " + electionData.toString());
                        }


                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                t.printStackTrace();
                t.getMessage();
                Log.d(TAG, t.getMessage());
                loadingDialog.hideDialog();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setActiveFragment(String branchElectionStatus) {

        switch (branchElectionStatus) {
            case "closed":
                loadFragment(new CoordinatorElectionClosedFragment());
                break;
            case "support":
                if (hasSupportedPetition)
                    loadFragment(new CoordinatorsSupportTalliesFragment());
                else
                    loadFragment(new CoordinatorElectionCallFragment());
                break;
            case "nomination":
                if (hasSubmitted)
                    loadFragment(new CoordinatorElectionCandidatesFragment());
                else
                    loadFragment(new CoordinatorElectionSubmitYourself());
                break;
            case "voting":
                if (hasVoted)
                    loadFragment(new CoordinatorElectionTalliesFragment());
                else
                    loadFragment(new CoordinatorElectionCandidatesFragment());
                break;
            default:

                break;

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        bundle = new Bundle();
        bundle.putParcelable("election", branchElectionDao);
        fragment.setArguments(bundle);
        transaction.replace(R.id.root_frame, fragment);
        transaction.commit();
    }
}