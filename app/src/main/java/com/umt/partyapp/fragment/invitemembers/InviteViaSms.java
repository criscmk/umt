package com.umt.partyapp.fragment.invitemembers;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.InviteMembers;
import com.umt.partyapp.model.ContactInvitationDisplay;
import com.umt.partyapp.model.InviteViaSmsDto;
import com.umt.partyapp.model.SmsInviteContactsDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.LimitColumn;
import com.wafflecopter.multicontactpicker.MultiContactPicker;
import com.wafflecopter.multicontactpicker.RxContacts.PhoneNumber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InviteViaSms extends Fragment {

    private static final String TAG = "InviteViaSms";

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    public static final int CONTACT_PICKER_REQUEST_SMS = 992;

    private ArrayList<ContactResult> results = new ArrayList<>();
    private HashMap<Integer, String> selectedContacts = new HashMap<>();
    private InviteMembers inviteMembers;
    private AppCompatEditText phone;
    private ChipGroup chipGroup;
    private ProgressBar progressBar;
    private Button btnInvite;

    public InviteViaSms() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inviteMembers = (InviteMembers) getActivity();
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_invite_via_sms, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.submit_invitation_progress_bar);
        phone = view.findViewById(R.id.phone_number);
        ImageButton btnFetchContacts = view.findViewById(R.id.btn_load_contacts_sms);
        btnInvite = view.findViewById(R.id.btn_submit_invite_sms);
        chipGroup = view.findViewById(R.id.user_phonenumber_chipgroup);

        btnFetchContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadContacts();
            }
        });


        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvites();
                //Log.d("invites", newContactList.toString());
            }
        });


        phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("Done", "Enter pressed");

                    String chipText = v.getText().toString();

                    if (chipText.length() > 0) {
                        updateChips(new ContactInvitationDisplay("", chipText, ""));
                        phone.getText().clear();
                    }

                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void updateContactChipGroup(ArrayList<ContactInvitationDisplay> updatedContacts) {

        for (int i = 0; i <= updatedContacts.size() - 1; i++) {

            updateChips(updatedContacts.get(i));
        }
    }

    public void updateChips(ContactInvitationDisplay contactInvitationDisplay) {
        int currentIndex = chipGroup.getChildCount();
        //Update selectedContact

        //Check if contact already added
        boolean contactExists = selectedContacts.containsValue(contactInvitationDisplay.getPhoneNumber());

        if (!contactExists) {
            String chipDisplay = contactInvitationDisplay.getDisplayName() == "" ? contactInvitationDisplay.getPhoneNumber() :
                    contactInvitationDisplay.getDisplayName() + " : " + contactInvitationDisplay.getPhoneNumber();

            selectedContacts.put(currentIndex, contactInvitationDisplay.getPhoneNumber());
            Chip myChip = addNewChip(chipDisplay, chipGroup);
            chipGroup.addView(myChip);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void sendInvites() {

        Set<Integer> selectedContactsKeys = selectedContacts.keySet();
        SmsInviteContactsDto smsInviteContactsDto = new SmsInviteContactsDto();
        ArrayList<InviteViaSmsDto> invites = new ArrayList<>();

        for (Integer key : selectedContactsKeys) {
            InviteViaSmsDto inviteViaSmsDto = new InviteViaSmsDto(selectedContacts.get(key));
            invites.add(inviteViaSmsDto);
        }

        smsInviteContactsDto.setContacts(invites);
        Log.d(TAG, smsInviteContactsDto.getContacts().get(0).getPhoneNumber());

        progressBar.setVisibility(View.VISIBLE);
        btnInvite.setAlpha(0f);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().sendInvitations(smsInviteContactsDto);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                btnInvite.setAlpha(1f);

                if (response.isSuccessful()) {
                    Log.d(TAG, response.body().toString());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnInvite.setAlpha(1f);
                Log.d("error", t.toString());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private Chip addNewChip(String text, ChipGroup chipGroup) {
        Chip chip = new Chip(getContext());
        chip.setText(text);
        chip.setCloseIconVisible(true);
        chip.setChipIconTintResource(R.color.grey_20);
        chip.setClickable(false);
        chip.setCheckable(false);
        chip.setChipBackgroundColorResource(R.color.umt_white);
        chip.setChipIcon(getResources().getDrawable(R.drawable.ic_phone));
        chip.setIconStartPaddingResource(R.dimen.spacing_medium);

        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipGroup.removeView(chip);
                int selectedContactIndex = (v.getId()) - 1;
                selectedContacts.remove(selectedContactIndex);

                Log.d(TAG, String.valueOf(v.getId()));
            }
        });
        return chip;

    }

    public void onReceiveContactsResults(ArrayList<ContactResult> results) {
        Log.i("Contact Name", String.valueOf(selectedContacts));
        if (results.size() > 0) {

            Log.d("MyTag", results.get(0).getDisplayName());
            Log.d("list", results.toString());
            Log.d("number", results.get(0).getPhoneNumbers().get(0).getNumber());

            ArrayList<ContactInvitationDisplay> updatedContacts = new ArrayList<>();
            for (int i = 0; i <= results.size() - 1; i++) {

                List<PhoneNumber> selectedPhoneNumbers = results.get(i).getPhoneNumbers();
                Log.d("list", selectedPhoneNumbers.size() + "");
                for (int j = 1; j < selectedPhoneNumbers.size(); j++) {
                    PhoneNumber phoneNumber = selectedPhoneNumbers.get(j);
                    ContactInvitationDisplay contactInvitationDisplay = new ContactInvitationDisplay(results.get(i).getDisplayName(), phoneNumber.getNumber(), "");
                    updatedContacts.add(contactInvitationDisplay);
                }
            }
            updateContactChipGroup(updatedContacts);

        }

    }


    public void loadContacts() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            new MultiContactPicker.Builder(inviteMembers) //Activity/fragment context
                    .theme(R.style.MyCustomPickerTheme) //Optional - default: MultiContactPicker.Azure
                    .hideScrollbar(false) //Optional - default: false
                    .showTrack(true) //Optional - default: true
                    .searchIconColor(Color.WHITE) //Option - default: White
                    .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
                    .handleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary)) //Optional - default: Azure Blue
                    .bubbleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary)) //Optional - default: Azure Blue
                    .bubbleTextColor(Color.WHITE) //Optional - default: White
                    .setTitleText("Select Contacts") //Optional - default: Select Contacts
                    .setSelectedContacts(results) //Optional - will pre-select contacts of your choice. String... or List<ContactResult>
                    .setLoadingType(MultiContactPicker.LOAD_ASYNC) //Optional - default LOAD_ASYNC (wait till all loaded vs stream results)
                    .limitToColumn(LimitColumn.PHONE) //Optional - default NONE (Include phone + email, limiting to one can improve loading time)
                    .setActivityAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                            android.R.anim.fade_in,
                            android.R.anim.fade_out) //Optional - default: No animation overrides
                    .showPickerForResult(CONTACT_PICKER_REQUEST_SMS);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(inviteMembers,
                    Manifest.permission.READ_CONTACTS)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Read Contacts permission");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setMessage("Please enable access to contacts.");
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(
                                new String[]
                                        {Manifest.permission.READ_CONTACTS}
                                , PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                });
                builder.show();
            } else {
                ActivityCompat.requestPermissions(inviteMembers,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST_READ_CONTACTS);
            }

//                Toast.makeText(getApplicationContext(), "Remember to go into settings and enable the contacts permission.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts();
                } else {
                    Toast.makeText(getContext(), "You have disabled a contacts permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
}