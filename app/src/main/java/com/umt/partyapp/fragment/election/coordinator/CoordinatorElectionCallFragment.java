package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.umt.partyapp.R;
import com.umt.partyapp.activity.VoteActivity;
import com.umt.partyapp.activity.VoteActivityViewModel;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.model.BranchElectionPetitionSupportVote;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorElectionCallFragment extends Fragment {
    private static final String TAG = "support";

    private VoteActivityViewModel viewModel;

    TextView txtElectionCounter;
    TextView tvCoordinator, tvCalledBy, tvReason, tvDays, tvHours, tvMinutes, tvSeconds;
    long diff;
    long oldLong;
    long NewLong;

    Button btnYes, btnNo;
    ProgressBar progressBar;
    BranchElectionCallResponseDto branchElectionDao;
    Long electionId;

    Boolean votedPetition = true;

    private LoadingDialog loadingDialog;
    private VoteActivity voteActivity;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public CoordinatorElectionCallFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_coord_elec_call, container, false);

        txtElectionCounter = rootView.findViewById(R.id.timer);
        tvCoordinator = rootView.findViewById(R.id.coordinatorView);
        tvCalledBy = rootView.findViewById(R.id.calledBy);
        tvReason = rootView.findViewById(R.id.tvReason);
        btnYes = rootView.findViewById(R.id.btnCallForElectionYes);
        btnNo = rootView.findViewById(R.id.btnCallForElectionNo);
        progressBar = rootView.findViewById(R.id.submit_call_support_progress_bar);

        CoordinatorLayout baseLayout = rootView.findViewById(R.id.base_layout);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        loadingDialog = new LoadingDialog(getActivity());

        voteActivity = (VoteActivity) getActivity();

        btnYes.setOnClickListener(v -> supportPetition("YES"));
        btnNo.setOnClickListener(v -> supportPetition("NO"));

        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();
        if (bundle != null) {
            branchElectionDao = bundle.getParcelable("election");

            electionId = branchElectionDao.getElectionId();
            tvCoordinator.setText(branchElectionDao.getCoordinatorName());
            tvCalledBy.setText(branchElectionDao.getCalledBy());
            tvReason.setText(branchElectionDao.getReason());

            SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");
            String now = String.valueOf(LocalDateTime.now());
            String oldTime = getDate(now);
//                            String NewTime = "2020-11-29, 12:00";
            String NewTime = getDate(branchElectionDao.getEndDate());

            Log.d(TAG, "date " + now);
            Date oldDate, newDate;

            try {
                oldDate = formatter.parse(oldTime);
                newDate = formatter.parse(NewTime);
                oldLong = oldDate.getTime();
                NewLong = newDate.getTime();
                diff = NewLong - oldLong;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            MyCount counter = new MyCount(diff, 1000);
            counter.start();

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public void supportPetition(String vote) {

        BranchElectionPetitionSupportVote myVote = new BranchElectionPetitionSupportVote(electionId, vote);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().supportElectionPetition(myVote);

        btnYes.setVisibility(View.GONE);
        btnNo.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String res = null;
                    int status;

                    try {
                        Log.d(TAG, "response3 ");
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));
                        Log.d(TAG, "response " + jsonObject.toString());

                        if (status == 200) {
                            JSONObject data = new JSONObject(jsonObject.getString("data"));

                            Gson gson = new Gson();

                            branchElectionDao = gson.fromJson(String.valueOf(data), BranchElectionCallResponseDto.class);

                            showSuccessAlertDialog("ELECTION PETITION", "You have voted successfully");
                            votedForPetition(branchElectionDao);

                        } else {
                            btnYes.setVisibility(View.VISIBLE);
                            btnNo.setVisibility(View.VISIBLE);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                t.getMessage();

                progressBar.setVisibility(View.GONE);
                btnYes.setVisibility(View.VISIBLE);
                btnNo.setVisibility(View.VISIBLE);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }

    private void showSuccessAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> {

        });
        builder.show();
    }

    public void votedForPetition(BranchElectionCallResponseDto electionObj) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        Fragment fragment = new CoordinatorsSupportTalliesFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("election", electionObj);
        fragment.setArguments(bundle);

        transaction.replace(R.id.root_frame, fragment);
        transaction.commit();
    }


    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            txtElectionCounter.setText("PETITION CLOSED!");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;

            String days = String.valueOf(TimeUnit.MILLISECONDS.toDays(millis) % 365);
            String hours = String.valueOf(TimeUnit.MILLISECONDS.toHours(millis) % 24);
            String minutes = String.valueOf(TimeUnit.MILLISECONDS.toMinutes(millis) % 60);
            String seconds = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) % 60);

            tvDays.setText(days);
            tvHours.setText(hours);
            tvMinutes.setText(minutes);
            tvSeconds.setText(seconds);

        }
    }


}
