package com.umt.partyapp.fragment.election.womanrep;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.VoteActivity;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.util.LoadingDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

@RequiresApi(api = Build.VERSION_CODES.O)
public class WomanRepSubmitYourself extends Fragment {

    private static final String TAG = "woman_rep_submit";

    private Button btnSubmitYourself;
    private TextView tvCandidateName, tvDays, tvHours, tvMinutes, tvSeconds, tvSubmitYourselfMsg;

    long diff;
    String dateFrom = null;
    String dateTo;

    private LoadingDialog loadingDialog;
    private ElectionDto electionDto;
    private Bundle bundle;

    private VoteActivity voteActivity;

    private NumberFormat numberFormat = new DecimalFormat("00");

    public WomanRepSubmitYourself() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_submit_yourself, container, false);

        voteActivity = (VoteActivity) getActivity();
        loadingDialog = new LoadingDialog(voteActivity);

        CoordinatorLayout timerLayout = rootView.findViewById(R.id.nominations_timer_base_layout);

        tvDays = timerLayout.findViewById(R.id.tv_days);
        tvHours = timerLayout.findViewById(R.id.tv_hours);
        tvMinutes = timerLayout.findViewById(R.id.tv_minutes);
        tvSeconds = timerLayout.findViewById(R.id.tv_seconds);
        btnSubmitYourself = rootView.findViewById(R.id.btn_mca_submit_nomination);
        tvCandidateName = rootView.findViewById(R.id.tv_mca_name);
        tvSubmitYourselfMsg = rootView.findViewById(R.id.election_msg);

        btnSubmitYourself.setOnClickListener(v -> submit());
        initFragment();

        return rootView;
    }

    public void submit() {
        Log.d(TAG, "submitNomination: button");
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        Fragment fragment = new WomanRepCandidatureForm();

        bundle = new Bundle();
        bundle.putParcelable("election", electionDto);
        fragment.setArguments(bundle);

        transaction.replace(R.id.woman_rep_root_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void initFragment() {
        bundle = getArguments();
        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            dateTo = electionDto.getEndDate();

            String name = electionDto.getCandidateName();
            String electionMsg = electionDto.getLevelName() + " County senatorial election";
            Log.d(TAG, "initFragment: " + name);

            tvCandidateName.setText(name);
            tvSubmitYourselfMsg.setText(electionMsg);

            diff = voteActivity.getDateTimeDiff(dateFrom, dateTo);
            MyCount counter = new MyCount(diff, 1000);
            counter.start();
        }

    }


    public class MyCount extends CountDownTimer {

        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            Log.d(TAG, "onTick: timer called");
        }

        @Override
        public void onFinish() {
//            txtElectionCounter.setText("Done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }
}