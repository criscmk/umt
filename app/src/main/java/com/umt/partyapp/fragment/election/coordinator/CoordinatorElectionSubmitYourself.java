package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.model.BranchElectionNominationDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorElectionSubmitYourself extends Fragment {
    private static final String TAG = "nominations";
    TextView txtElectionCounter, tvCoordinator, tvDays, tvHours, tvMinutes, tvSeconds;
    Button btnSubmit;
    ProgressBar progressBar;
    long diff;
    long oldLong;
    long newLong;

    Long electionId;
    private BranchElectionCallResponseDto branchElectionDao;
    private LoadingDialog loadingDialog;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public CoordinatorElectionSubmitYourself() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_coordinator_election_submit_yourself, container, false);

        CoordinatorLayout baseLayout = rootView.findViewById(R.id.nominations_timer_base_layout);

//        txtElectionCounter = rootView.findViewById(R.id.timer_to_decision);
        tvCoordinator = rootView.findViewById(R.id.tvCoordinatorName);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        btnSubmit = rootView.findViewById(R.id.btn_submit_yourself);
        progressBar = rootView.findViewById(R.id.submit_yourself_progress_bar);

        loadingDialog = new LoadingDialog(getActivity());

        btnSubmit.setOnClickListener(v -> submitNomination());

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();
        if (bundle != null) {
            branchElectionDao = bundle.getParcelable("election");

            electionId = branchElectionDao.getElectionId();
            tvCoordinator.setText(branchElectionDao.getCoordinatorName());


            SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");
            String now = String.valueOf(LocalDateTime.now());
            String oldTime = getDate(now);
            String newTime = getDate(branchElectionDao.getEndDate());

            Log.d(TAG, "date " + newTime);
            Date oldDate, newDate;

            try {
                oldDate = formatter.parse(oldTime);
                newDate = formatter.parse(newTime);
                oldLong = oldDate.getTime();
                newLong = newDate.getTime();
                diff = newLong - oldLong;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            MyCount counter = new MyCount(diff, 1000);
            counter.start();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }

    public void submitNomination() {

        BranchElectionNominationDto nomination = new BranchElectionNominationDto(electionId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().submitNomination(nomination);

        progressBar.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String res = null;
                    int status;

                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));
                        Log.d(TAG, "response " + jsonObject.toString());

                        if (status == 200) {
                            showSuccessAlertDialog("ELECTION NOMINATION", "Nomination submitted successfully");

                        } else {
                            btnSubmit.setVisibility(View.VISIBLE);
                            showSuccessAlertDialog("ERROR!", "Nomination submission failed !");

                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                t.getMessage();

                progressBar.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showSuccessAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> {

            nextFragment(branchElectionDao);
        });

        builder.show();
    }

    public void nextFragment(BranchElectionCallResponseDto electionObj) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        Fragment fragment = new CoordinatorElectionCandidatesFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("election", electionObj);
        fragment.setArguments(bundle);

        transaction.replace(R.id.root_frame, fragment);
        transaction.commit();

    }


    public class MyCount extends CountDownTimer {

        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            Log.d(TAG, "onTick: timer called");
        }

        @Override
        public void onFinish() {
//            txtElectionCounter.setText("Done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Log.d(TAG, "onTick: timer called");

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }
}