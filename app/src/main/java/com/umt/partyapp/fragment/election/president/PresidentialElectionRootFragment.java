package com.umt.partyapp.fragment.election.president;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.apiresponse.ElectionResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class PresidentialElectionRootFragment extends Fragment {
    private static final String TAG = "president";

    private String electionStatus = "closed";
    private Boolean hasSubmitted = false;
    private Boolean hasVoted = false;

    private ElectionDto electionDto;
    private LoadingDialog loadingDialog;

    private Bundle bundle;


    public PresidentialElectionRootFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_presidential_root, container, false);

        electionDto = new ElectionDto();

        loadingDialog = new LoadingDialog(getActivity());

        getWardElectionInfo();

        Log.d(TAG, "onCreateView: ");

        return rootView;
    }

/*    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.e(TAG, "load data here");
//            viewDialog = new ViewDialog(getActivity());
//            getWardElectionInfo();
        } else {
            Log.e(TAG, "fragment is no longer visible");

        }
    }*/

    public void getWardElectionInfo() {
        Log.d(TAG, "method called");
        loadingDialog.showDialog();

        Call<ElectionResponseDto> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getPresidentialElection();

        call.enqueue(new Callback<ElectionResponseDto>() {

            @Override
            public void onResponse(@NotNull Call<ElectionResponseDto> call, @NotNull Response<ElectionResponseDto> response) {
                loadingDialog.hideDialog();

                if (response.isSuccessful()) {

                    ElectionResponseDto electionResponseDto = response.body();

                    int status = electionResponseDto.getStatus();

                    if (status == 200) {
                        electionDto = electionResponseDto.getElectionDto();
                        electionStatus = response.body().getElectionDto().getElectionStatus();

                        hasSubmitted = electionDto.getHasSubmitted();
                        hasVoted = electionDto.getHasVoted();

                        Log.d(TAG, "onResponse: " + electionStatus);
                        setActiveFragment();
                    }

                } else {
                    electionDto.setCurrentLeader("Denis Bii");
                    electionDto.setEndDate("2020-12-22T13:23:06.412+00:00");
                    electionDto.setCandidateName("Kiprono Bii");
                    electionDto.setElectionId((long) 1);
                    electionDto.setElectionStatus("voting");
                    setActiveFragment();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ElectionResponseDto> call, @NotNull Throwable t) {
                t.printStackTrace();
                t.getMessage();
                Log.d(TAG, t.getMessage());
                loadingDialog.hideDialog();

            }
        });

    }


    public void setActiveFragment() {

        switch (electionStatus) {
            case "closed":
                loadFragment(new PresidentialElectionClosed());
                break;
            case "nomination":
                if (hasSubmitted){
                    loadFragment(new PresidentialCandidatesFragment());
                } else {
                    loadFragment(new PresidentialSubmitYourself());
                }

                break;
            case "voting":
                loadFragment(new PresidentialCandidatesFragment());
                break;
            default:

                break;

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        bundle = new Bundle();
        bundle.putParcelable("election", electionDto);
        fragment.setArguments(bundle);
        transaction.replace(R.id.presidential_root_frame, fragment);
        transaction.commit();
    }
}