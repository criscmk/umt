package com.umt.partyapp.fragment.election.coordinator;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umt.partyapp.R;
import com.umt.partyapp.adapters.BranchElectionCandidatesAdapter;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.apiresponse.BranchElectionCandidateDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorElectionCandidatesFragment extends Fragment {
    private static final String TAG = "branch_candidates";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relativeLayout;
    private CardView cvHasVotedMsg;
    private TextView votingStatus, votingTimerMsg, tvDays, tvHours, tvMinutes, tvSeconds;
    private Button vote;
    Boolean isVotingOpen, hasVoted = false;
    Long electionId;
    long diff;
    long oldLong;
    long newLong;

    private BranchElectionCallResponseDto branchElectionDao;
    private BranchElectionCandidatesAdapter adapter;
    private CoordinatorElectionCandidatesViewModel mViewModel;
    private LoadingDialog loadingDialog;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public static CoordinatorElectionCandidatesFragment newInstance() {
        return new CoordinatorElectionCandidatesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_coordinator_election_candidates_fragment, container, false);
        recyclerView = view.findViewById(R.id.list_candidates);
        votingStatus = view.findViewById(R.id.election_voting_status);
        votingTimerMsg = view.findViewById(R.id.voting_timer_msg);
        cvHasVotedMsg = view.findViewById(R.id.has_voted_message);
        relativeLayout = view.findViewById(R.id.base_layout_container);

        CoordinatorLayout baseLayout = view.findViewById(R.id.voting_timer_base_layout);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        loadingDialog = new LoadingDialog(getActivity());

        adapter = new BranchElectionCandidatesAdapter(getContext(), CoordinatorElectionCandidatesFragment.this);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        initFragment();

        return view;
    }

    public void initFragment() {

        Bundle bundle = getArguments();
        if (bundle != null) {

            loadingDialog.showDialog();

            branchElectionDao = bundle.getParcelable("election");
            electionId = branchElectionDao.getElectionId();
//            hasVoted = branchElectionDao.getHasVoted();

            String voting = branchElectionDao.getElectionState();
            isVotingOpen = voting.equals("voting");

            if (isVotingOpen) {
                votingStatus.setVisibility(View.VISIBLE);
                votingTimerMsg.setText("Time until voting closes");
            } else {
                votingTimerMsg.setText("Time until voting starts");
            }

//            if (hasVoted){
//                cvHasVotedMsg.setVisibility(View.VISIBLE);
//                relativeLayout.setVisibility(View.GONE);
//            }

            SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");
            String now = String.valueOf(LocalDateTime.now());
            String oldTime = getDate(now);
            String newTime = getDate(branchElectionDao.getEndDate());

            Log.d(TAG, "date " + newTime);
            Date oldDate, newDate;

            try {
                oldDate = formatter.parse(oldTime);
                newDate = formatter.parse(newTime);
                oldLong = oldDate.getTime();
                newLong = newDate.getTime();
                diff = newLong - oldLong;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            MyCount counter = new MyCount(diff, 1000);
            counter.start();

            getCandidates(isVotingOpen, electionId);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CoordinatorElectionCandidatesViewModel.class);
        // TODO: Use the ViewModel
    }

//    public void getBranchElectionInfo() {
//        viewDialog.showDialog();
//        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getBranchElection();
//
//        call.enqueue(new Callback<ResponseBody>() {
//
//
//            @Override
//            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
//
//                if (response.isSuccessful()) {
//
//                    String s;
//                    int status;
//                    try {
//
//                        s = response.body().string();
//                        JSONObject jsonObject = new JSONObject(s);
//                        status = Integer.parseInt(jsonObject.getString("status"));
//                        Log.d(TAG, "status " + status);
//
//                        if (status == 200) {
//                            JSONObject electionData = new JSONObject(jsonObject.getString("data"));
//
//                            Gson gson = new Gson();
//
//                            electionPetition = gson.fromJson(String.valueOf(electionData), BranchElectionCallResponseDto.class);
//                            electionId = electionPetition.getElectionId();
//
//                            String voting = electionPetition.getElectionState();
//                            boolean isVotingOpen = voting.equals("voting");
//
//                            if (isVotingOpen) {
//                                votingStatus.setVisibility(View.VISIBLE);
//                                votingTimer.setVisibility(View.VISIBLE);
//
//                                SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");
//                                String now = String.valueOf(LocalDateTime.now());
//                                String oldTime = getDate(now);
////                            String NewTime = "2020-11-29, 12:00";
//                                String newTime = getDate(electionPetition.getEndDate());
//
//                                Log.d(TAG, "date " + newTime);
//                                Date oldDate, newDate;
//
//                                try {
//                                    oldDate = formatter.parse(oldTime);
//                                    newDate = formatter.parse(newTime);
//                                    oldLong = oldDate.getTime();
//                                    NewLong = newDate.getTime();
//                                    diff = NewLong - oldLong;
//                                } catch (ParseException e) {
//                                    e.printStackTrace();
//                                }
//                                MyCount counter = new MyCount(diff, 1000);
//                                counter.start();
//                            }
//
//                            getCandidates(isVotingOpen, electionId);
//                        }
//
//
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
//                t.printStackTrace();
//                t.getMessage();
//                viewDialog.hideDialog();
//            }
//        });
//
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }

    public void getCandidates(Boolean vote, Long election) {

        Map<String, Long> params = new HashMap<>();
        params.put("electionId", electionId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getCandidates(params);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONArray data = jsonObject.getJSONArray("data");

                            Gson gson = new Gson();

                            Type type = new TypeToken<List<BranchElectionCandidateDto>>() {
                            }.getType();
                            List<BranchElectionCandidateDto> candidates = gson.fromJson(String.valueOf(data), type);
                            adapter.addAll(candidates, vote, election);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                loadingDialog.hideDialog();
            }
        });
    }

    public void nextFragment() {
        cvHasVotedMsg.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.GONE);
    }

    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

//            votingTimer.setText("done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }
}