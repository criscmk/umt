package com.umt.partyapp.fragment.election.mp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.util.LoadingDialog;


public class MpElectionClosed extends Fragment {

    private static final String TAG = "mp_closed";

    private TextView tvDateElected;
    private TextView tvNextElection;
    private TextView tvMpName;
    private TextView tvNoCurrentLeader;
    private TextView tvElectionClosedInfo;
    private TextView tvCurrentLeader;
    private LinearLayout lyLeaderInfo;

    private LoadingDialog loadingDialog;
    private ElectionDto electionDto;

    public MpElectionClosed() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_election_closed, container, false);

        tvDateElected = rootView.findViewById(R.id.mca_date_elected);
        tvNextElection = rootView.findViewById(R.id.mca_next_election);
        tvMpName = rootView.findViewById(R.id.tv_mca_name);
        tvNoCurrentLeader = rootView.findViewById(R.id.tv_no_current_mca);
        tvCurrentLeader = rootView.findViewById(R.id.tv_current_leader);
        tvElectionClosedInfo = rootView.findViewById(R.id.tv_mca_election_info);
        lyLeaderInfo = rootView.findViewById(R.id.ly_ward_mca);

        initFragment();

        return rootView;
    }

    public void initFragment() {
        Bundle bundle;
        bundle = getArguments();

        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            String currentLeader = "";
            currentLeader = electionDto.getCurrentLeader();

            boolean hasCurrentLeader;
            hasCurrentLeader = !currentLeader.isEmpty();

            Log.d(TAG, "initFragment: " + electionDto.getCurrentLeader() + hasCurrentLeader);

            if (hasCurrentLeader) {
                tvMpName.setText(electionDto.getCurrentLeader());
                tvCurrentLeader.setText("Current Constituency Mp");
            } else {
                String noLeader = electionDto.getLevelName() + " Constituency does not have a Member of parliament yet.";

                lyLeaderInfo.setVisibility(View.GONE);
                tvNoCurrentLeader.setVisibility(View.VISIBLE);
                tvNoCurrentLeader.setText(noLeader);
            }
        }

    }
}