package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umt.partyapp.R;
import com.umt.partyapp.adapters.ElectionResultsAdapter;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.apiresponse.ElectionResultsResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorElectionTalliesFragment extends Fragment {
    private static final String TAG = "coordinator_tally";

    TextView tvTallyCounterMsg, tvDays, tvHours, tvMinutes, tvSeconds, pageTitle, electionLevelPlaceName;
    long diff;
    long oldLong;
    long newLong;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private ElectionResultsAdapter adapter;
    private Long electionId;
    private BranchElectionCallResponseDto branchElectionDao;
    private LoadingDialog loadingDialog;
    private CoordinatorLayout baseLayout;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public CoordinatorElectionTalliesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_coordinator_election_tallies, container, false);

        recyclerView = rootView.findViewById(R.id.list_candidates_tally);
        tvTallyCounterMsg = rootView.findViewById(R.id.tally_timer_msg);
        pageTitle = rootView.findViewById(R.id.election_stage);
        electionLevelPlaceName = rootView.findViewById(R.id.election_msg);

        baseLayout = rootView.findViewById(R.id.voting_timer_base_layout);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        adapter = new ElectionResultsAdapter(getContext());
        loadingDialog = new LoadingDialog(getActivity());

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        initFragment();

        return rootView;
    }


    public void initFragment() {
        Bundle bundle = getArguments();
        if (bundle != null) {

            loadingDialog.showDialog();

            branchElectionDao = bundle.getParcelable("election");
            electionId = branchElectionDao.getElectionId();

            Log.d(TAG, "initFragment: " + electionId);

            String placeName = "Karen Branch";
//            pageTitle.setText("");
            electionLevelPlaceName.setText(placeName);


           /* if (diff == 0) {
                baseLayout.setVisibility(View.GONE);
                tvTallyCounterMsg.setVisibility(View.GONE);
            }*/

            baseLayout.setVisibility(View.GONE);
            tvTallyCounterMsg.setVisibility(View.GONE);

//            MyCount counter = new MyCount(diff, 1000);
//            counter.start();

            getElectionTally();
        }
    }


    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }

    public void getElectionTally() {

        Map<String, Long> params = new HashMap<>();
        params.put("electionId", electionId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getElectionResults(params);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {

                    String res;
                    int status;

                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONArray data = jsonObject.getJSONArray("data");
                            Log.d(TAG, "data " + data.toString());
                            Gson gson = new Gson();

                            Type type = new TypeToken<List<ElectionResultsResponseDto>>() {
                            }.getType();
                            List<ElectionResultsResponseDto> results = gson.fromJson(String.valueOf(data), type);
                            adapter.addAll(results);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                loadingDialog.hideDialog();

            }
        });
    }


    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

//            txtElectionCounter.setText("done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }

}