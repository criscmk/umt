package com.umt.partyapp.fragment.election.mca;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.util.LoadingDialog;

@RequiresApi(api = Build.VERSION_CODES.O)
public class McaElectionClosed extends Fragment {

    private static final String TAG = "mcaElectionClosed";

    private TextView tvDateElected;
    private TextView tvNextElection;
    private TextView tvMcaName;
    private TextView tvNoMca;
    private TextView tvElectionClosedInfo;
    private LinearLayout lyMcaInfo;
    private LinearLayout lyResults;

    private LoadingDialog loadingDialog;
    private ElectionDto electionDto;

    public McaElectionClosed() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_election_closed, container, false);

        tvDateElected = rootView.findViewById(R.id.mca_date_elected);
        tvNextElection = rootView.findViewById(R.id.mca_next_election);
        tvMcaName = rootView.findViewById(R.id.tv_mca_name);
        tvNoMca = rootView.findViewById(R.id.tv_no_current_mca);
        tvElectionClosedInfo = rootView.findViewById(R.id.tv_mca_election_info);
        lyMcaInfo = rootView.findViewById(R.id.ly_ward_mca);
        lyResults = rootView.findViewById(R.id.results_ly);

        lyResults.setOnClickListener(v -> viewResults());

        initFragment();

        return rootView;
    }


    public void initFragment() {
        Bundle bundle;
        bundle = getArguments();

        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            String currentLeader = "";
            currentLeader = electionDto.getCurrentLeader();

            boolean hasCurrentMca;
            hasCurrentMca = !currentLeader.isEmpty();

            if (!hasCurrentMca) {
                lyMcaInfo.setVisibility(View.GONE);
                tvNoMca.setVisibility(View.VISIBLE);
            } else {
                tvMcaName.setText(electionDto.getCurrentLeader());
            }
        }

    }

    public void viewResults(){
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        Fragment fragment = new McaElectionTally();

        Bundle bundle = new Bundle();
        bundle.putParcelable("election", electionDto);
        fragment.setArguments(bundle);

        transaction.replace(R.id.mca_root_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}