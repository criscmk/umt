package com.umt.partyapp.fragment.election.senator;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umt.partyapp.R;
import com.umt.partyapp.activity.VoteActivity;
import com.umt.partyapp.adapters.ElectionCandidatesAdapter;
import com.umt.partyapp.apiresponse.ElectionCandidateDto;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.O)
public class SenatorCandidatesFragment extends Fragment {
    private static final String TAG = "senator_candidates";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relativeLayout;
    private CardView cvHasVotedMsg;
    private TextView votingStatus, votingTimerMsg, tvDays, tvHours, tvMinutes, tvSeconds, tvCountyName;
    private Button vote;
    Boolean isVotingOpen, hasVoted = false;
    Long electionId;
    long diff;
    long oldLong;
    long newLong;


    private ElectionDto electionDto;
    private ElectionCandidatesAdapter adapter;

    private LoadingDialog loadingDialog;
    private VoteActivity voteActivity;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public SenatorCandidatesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_candidates, container, false);

        voteActivity = (VoteActivity) getActivity();

        recyclerView = rootView.findViewById(R.id.list_candidates);
        votingStatus = rootView.findViewById(R.id.election_voting_status);
        votingTimerMsg = rootView.findViewById(R.id.voting_timer_msg);
        cvHasVotedMsg = rootView.findViewById(R.id.has_voted_message);
        relativeLayout = rootView.findViewById(R.id.base_layout_container);
        tvCountyName = rootView.findViewById(R.id.tv_place_name);

        CoordinatorLayout baseLayout = rootView.findViewById(R.id.voting_timer_base_layout);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        adapter = new ElectionCandidatesAdapter(getContext(), SenatorCandidatesFragment.this,"senator");

        loadingDialog = new LoadingDialog(getActivity());

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        initFragment();


        return rootView;
    }


    public void initFragment() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            electionId = electionDto.getElectionId();

            hasVoted = electionDto.getHasVoted();

            String dateTo = electionDto.getEndDate();
            String voting = electionDto.getElectionStatus();
            isVotingOpen = voting.equals("voting");

            String countyName = electionDto.getLevelName() + " " + "County";

            tvCountyName.setText(countyName);

            if (isVotingOpen) {
                votingStatus.setVisibility(View.VISIBLE);
                votingTimerMsg.setText("Time until voting closes");
            } else {
                votingTimerMsg.setText("Time until voting starts");
            }

            Log.d(TAG, "initFragment: " + isVotingOpen);

            diff = voteActivity.getDateTimeDiff(null, dateTo);
            MyCount count = new MyCount(diff, 1000);
            count.start();

            getCandidates(isVotingOpen, electionId);
        }
    }

    public void getCandidates(Boolean votingStatus, Long electionId) {
        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getSenatorialCandidates();

        loadingDialog.showDialog();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();

                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONArray data = jsonObject.getJSONArray("data");

                            Gson gson = new Gson();

                            Type type = new TypeToken<List<ElectionCandidateDto>>() {
                            }.getType();
                            List<ElectionCandidateDto> candidates = gson.fromJson(String.valueOf(data), type);
                            adapter.addAll(candidates, votingStatus, electionId, hasVoted);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                loadingDialog.showDialog();
            }
        });

    }

    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

//            votingTimer.setText("done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }
}