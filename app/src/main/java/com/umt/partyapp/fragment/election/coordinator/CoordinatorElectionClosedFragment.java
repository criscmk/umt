package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.umt.partyapp.R;
import com.umt.partyapp.activity.VoteActivity;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.model.BranchElectionReasonDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.umt.partyapp.MenuDrawerNews;


@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorElectionClosedFragment extends Fragment {
    private static final String TAG = "BranchElections";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    VoteActivity voteActivity;
    Button btnStartCallForElections, btnCancelCallForElection, btnCallForElections;
    LinearLayout lyResults;
    TextView electionExplanation, coordinatorName, dateElected, nextElection, tvElectionResults;
    TextInputLayout electionReason;
    EditText edElectionReason;
    private ProgressBar progress_bar;
    Long electionId;

    private BranchElectionCallResponseDto branchElectionDao;
    private Bundle bundle;

    public CoordinatorElectionClosedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        voteActivity = (VoteActivity) getActivity();
        View rootView = inflater.inflate(R.layout.fragment_coord_elec_closed, container, false);

        btnStartCallForElections = rootView.findViewById(R.id.btnStartCallForElection);
        btnCallForElections = rootView.findViewById(R.id.btnCallForElection);
        btnCancelCallForElection = rootView.findViewById(R.id.btnCancelCallForElection);
        electionExplanation = rootView.findViewById(R.id.electionExplanation);
        electionReason = rootView.findViewById(R.id.reason);
        edElectionReason = rootView.findViewById(R.id.reason_ed);
        coordinatorName = rootView.findViewById(R.id.tvCoordinatorName);
        dateElected = rootView.findViewById(R.id.dateElected);
        nextElection = rootView.findViewById(R.id.nextElection);
        progress_bar = rootView.findViewById(R.id.submit_call_progress_bar);
        lyResults = rootView.findViewById(R.id.results_ly);


        btnStartCallForElections.setOnClickListener(v -> {
            btnStartCallForElections.setVisibility(View.GONE);
            btnCallForElections.setVisibility(View.VISIBLE);
            electionExplanation.setVisibility(View.GONE);
            electionReason.setVisibility(View.VISIBLE);
            btnCancelCallForElection.setVisibility(View.VISIBLE);
        });

        btnCancelCallForElection.setOnClickListener(v -> {
            btnStartCallForElections.setVisibility(View.VISIBLE);
            btnCallForElections.setVisibility(View.GONE);
            electionExplanation.setVisibility(View.VISIBLE);
            electionReason.setVisibility(View.GONE);
            btnCancelCallForElection.setVisibility(View.GONE);

        });

        btnCallForElections.setOnClickListener(v -> {
            String reason = electionReason.getEditText().getText().toString().trim();
            if (reason.isEmpty()) {
                edElectionReason.setError("Reason is mandatory");
                electionReason.requestFocus();
                return;
            }

            showConfirmDialog(reason);
        });

        lyResults.setOnClickListener(v -> viewResults());


//        getBranchElectionInfo();

        return rootView;


    }


    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = getArguments();
        if (bundle != null) {
            branchElectionDao = bundle.getParcelable("election");

            electionId = branchElectionDao.getElectionId();

            String electedDate = getDateElected(branchElectionDao.getDateElected());
            String nextElectionDate = getNextElectionDate(electedDate);

            Log.d(TAG, "date time " + electedDate + " " + nextElectionDate);

            coordinatorName.setText(branchElectionDao.getCoordinatorName());
            dateElected.setText(getString(R.string.date_elected, electedDate));
            nextElection.setText(getString(R.string.next_election, nextElectionDate));
        }
    }

    public void viewResults() {
        if (electionId == null) {
            showSuccessAlertDialog("ATTENTION!!!", "There is no election results yet because an election is yet to be held.");
        } else {
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();

            Fragment fragment = new CoordinatorElectionTalliesFragment();

            bundle = new Bundle();
            bundle.putParcelable("election", branchElectionDao);
            fragment.setArguments(bundle);

            transaction.replace(R.id.root_frame, fragment);
            transaction.commit();
        }

    }


    public String getDateElected(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");
        Log.d(TAG, "date " + dateArray[0]);

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = dateObj.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        return electedDate;
    }


    public String getNextElectionDate(String date) {
        LocalDate dateObj = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        LocalDate next = dateObj.plusYears(1);

        String nextElectionDate = next.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));

        return nextElectionDate;
    }

    private void showConfirmDialog(String reason) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Call for Branch Coordinator Elections ?");
        builder.setMessage("Are you sure you want to call for election of a branch coordinator?");
        builder.setPositiveButton("Yes", (dialogInterface, i) -> submitCallForElections(reason));
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void showAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Support", (dialogInterface, i) -> {

        });
        builder.setNegativeButton(R.string.CANCEL, (dialogInterface, i) -> {
            //getActivity().startActivity(new Intent(getContext(), MenuDrawerNews.class));

        });

        builder.show();
    }

    private void showSuccessAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> {
        });
       /* builder.setNegativeButton(R.string.CANCEL, (dialogInterface, i) -> {
            //getActivity().startActivity(new Intent(getContext(), MenuDrawerNews.class));

        });*/

        builder.show();
    }


    private void submitCallForElections(String reason) {

        progress_bar.setVisibility(View.VISIBLE);
        btnCallForElections.setVisibility(View.GONE);
        btnCancelCallForElection.setVisibility(View.GONE);

        String title = "Operation Failed!";
        String message = "Failed to call for an election";

        BranchElectionReasonDto electionReasonDto = new BranchElectionReasonDto(reason);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().callForElection(electionReasonDto);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                progress_bar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    String s, responseMessage;
                    int status;

                    try {
                        s = response.body().string();
                        JSONObject jsonObject = new JSONObject(s);
                        status = Integer.parseInt(jsonObject.getString("status"));
                        responseMessage = jsonObject.getString("message");
                        Log.d(TAG, "status " + status);
                        Log.d(TAG, "message " + responseMessage);

                        if (status == 200) {
                            electionReason.setVisibility(View.GONE);
                            JSONObject electionPetition = new JSONObject(jsonObject.getString("data"));
                            Log.d(TAG, "onResponse: " + electionPetition.toString());
                            Gson gson = new Gson();

                            BranchElectionCallResponseDto branchElectionDao = gson.fromJson(String.valueOf(electionPetition), BranchElectionCallResponseDto.class);
                            showSuccessAlertDialog("Success", "Election petition called successfully");
                            nextFragment(branchElectionDao);

                        } else {
                            showSuccessAlertDialog(title, message);
                            btnCallForElections.setVisibility(View.VISIBLE);
                            btnCancelCallForElection.setVisibility(View.VISIBLE);
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    showSuccessAlertDialog(title, message);
                    btnCallForElections.setVisibility(View.VISIBLE);
                    btnCancelCallForElection.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                t.printStackTrace();
                t.getMessage();

                showSuccessAlertDialog(title, message);
                progress_bar.setVisibility(View.GONE);
                btnCallForElections.setVisibility(View.VISIBLE);
                btnCancelCallForElection.setVisibility(View.VISIBLE);

            }
        });
    }

    private void nextFragment(BranchElectionCallResponseDto electionObj) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        Fragment fragment = new CoordinatorElectionCallFragment();

        bundle = new Bundle();
        bundle.putParcelable("election", electionObj);
        fragment.setArguments(bundle);

        transaction.replace(R.id.root_frame, fragment);
        transaction.commit();

    }
}
