package com.umt.partyapp.fragment.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umt.partyapp.R;
import com.umt.partyapp.adapters.ForumsAdapter;
import com.umt.partyapp.model.ForumsDto;
import com.umt.partyapp.util.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class forumsFragment extends Fragment {
    View rootView;
    boolean isFabHide = false;
    RecyclerView recyclerView;
    private List<ForumsDto> forumList;
    LinearLayoutManager linearLayoutManager;


    public forumsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_forums, container, false);
        recyclerView  = (RecyclerView) rootView.findViewById(R.id.forums_recyclerview);
        recyclerView.addItemDecoration(new DividerItemDecoration(LinearLayoutManager.VERTICAL, getContext(), 16, 46));

        ForumsAdapter forumsAdapter = new ForumsAdapter(getContext(),forumList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(forumsAdapter);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forumList =  new ArrayList<>();
        forumList.add(new ForumsDto(" Political Citizens’ rights and services Elections and Security","Hello everyone.Welcome on board","5 seconds","50"));
        forumList.add(new ForumsDto(" Culture, Values and Leadership","Carlton joined","45 minutes","10"));
        forumList.add(new ForumsDto(" Health Education And Sports","Hello everyone.Welcome on board","2 hours","20"));
        forumList.add(new ForumsDto(" Land use, agriculture, Infrastructure Economy, Water and ","Hello everyone.Welcome on board","Yesterday","0"));
    }

    private void initComponent() {

    }



}