package com.umt.partyapp.fragment.election.mp;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.apiresponse.ElectionResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class MpRootFragment extends Fragment {

    private static final String TAG = "mp";

    private String electionStatus = "closed";
    private Boolean hasSubmitted = false;
    private Boolean hasVoted = false;


    private ElectionDto electionDto;
    private LoadingDialog loadingDialog;

    private Bundle bundle;

    public MpRootFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mp_root, container, false);

        electionDto = new ElectionDto();

        loadingDialog = new LoadingDialog(getActivity());
        getElectionInfo();

        return rootView;
    }

    public void getElectionInfo() {
        Log.d(TAG, "method called");
        loadingDialog.showDialog();

        Call<ElectionResponseDto> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getMpElection();

        call.enqueue(new Callback<ElectionResponseDto>() {

            @Override
            public void onResponse(@NotNull Call<ElectionResponseDto> call, @NotNull Response<ElectionResponseDto> response) {
                loadingDialog.hideDialog();

                if (response.isSuccessful()) {

                    ElectionResponseDto electionResponseDto = response.body();

                    int status = electionResponseDto.getStatus();

                    if (status == 200) {
                        electionDto = electionResponseDto.getElectionDto();
                        electionStatus = response.body().getElectionDto().getElectionStatus();

                        hasSubmitted = electionDto.getHasSubmitted();
                        hasVoted = electionDto.getHasVoted();

                        Log.d(TAG, "onResponse: " + electionStatus);
                        setActiveFragment();
                    }

                } else {
                    electionDto.setCurrentLeader("Denis Bii");
                    electionDto.setEndDate("2020-12-15T13:23:06.412+00:00");
                    electionDto.setCandidateName("Kiprono Bii");
                    electionDto.setElectionId((long) 1);
                    electionDto.setElectionStatus("nomination");
                    electionDto.setLevelName("Langata");
                    setActiveFragment();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ElectionResponseDto> call, @NotNull Throwable t) {
                t.printStackTrace();
                t.getMessage();
                Log.d(TAG, t.getMessage());
                loadingDialog.hideDialog();

            }
        });

    }


    public void setActiveFragment() {

        switch (electionStatus) {
            case "closed":
                loadFragment(new MpElectionClosed());
                break;
            case "nomination":
                if (hasSubmitted) {
                    loadFragment(new MpElectionCandidates());
                } else {
                    loadFragment(new MpSubmitYourself());
                }
                break;

            case "voting":
                loadFragment(new MpElectionCandidates());
                break;

            default:

                break;

        }

    }


    public void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        bundle = new Bundle();
        bundle.putParcelable("election", electionDto);
        fragment.setArguments(bundle);
        transaction.replace(R.id.mp_root_layout, fragment);
        transaction.commit();
    }
}