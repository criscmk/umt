package com.umt.partyapp.fragment.election.mca;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umt.partyapp.R;


public class McaElectionCandidates extends Fragment {
    private static final String TAG = "mca_candidates";

    public McaElectionCandidates() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mca_election_candidates, container, false);
    }
}