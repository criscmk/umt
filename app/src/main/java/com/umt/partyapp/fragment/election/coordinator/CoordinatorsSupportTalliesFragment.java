package com.umt.partyapp.fragment.election.coordinator;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.BranchElectionCallResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.O)
public class CoordinatorsSupportTalliesFragment extends Fragment {
    private final static String TAG = "petitionTallies";

    private SwipeRefreshLayout swipe_refresh;
    TextView tvPetitionElectionCounter, tvCoordinator, tvNoVotes, tvYesVotes, tvVotingStatus;
    ProgressBar yesVotesPb, noVotesPb;
    long diff;
    long oldLong;
    long NewLong;

    BranchElectionCallResponseDto branchElectionDao;
    Long electionId;

    Integer totalVotes;
    Integer yesVotes;
    Integer noVotes;

    private LoadingDialog loadingDialog;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public CoordinatorsSupportTalliesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cordinators_supporttallies, container, false);
        swipe_refresh = rootView.findViewById(R.id.swipe_refresh_layout);
        tvPetitionElectionCounter = rootView.findViewById(R.id.petition_timer_tally);
        tvVotingStatus = rootView.findViewById(R.id.petition_voting_status);
        tvCoordinator = rootView.findViewById(R.id.tv_coordinator_name);
        tvYesVotes = rootView.findViewById(R.id.tv_yes_votes);
        tvNoVotes = rootView.findViewById(R.id.tv_no_votes);
        yesVotesPb = rootView.findViewById(R.id.yes_votes_progress_bar);
        noVotesPb = rootView.findViewById(R.id.no_votes_progress_bar);

        loadingDialog = new LoadingDialog(getActivity());

        swipe_refresh.setOnRefreshListener(() -> pullAndRefresh());

//        getBranchElectionInfo();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();
        if (bundle != null) {
            branchElectionDao = bundle.getParcelable("election");

            electionId = branchElectionDao.getElectionId();
            tvCoordinator.setText(branchElectionDao.getCoordinatorName());

            getPetitionTallies();

           /* SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd, HH:mm");
            String now = String.valueOf(LocalDateTime.now());
            String oldTime = getDate(now);
//                            String NewTime = "2020-11-29, 12:00";
            String NewTime = getDate(branchElectionDao.getEndDate());

            Log.d(TAG, "date " + now);
            Date oldDate, newDate;

            try {
                oldDate = formatter.parse(oldTime);
                newDate = formatter.parse(NewTime);
                oldLong = oldDate.getTime();
                NewLong = newDate.getTime();
                diff = NewLong - oldLong;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            CoordinatorElectionCallFragment.MyCount counter = new CoordinatorElectionCallFragment.MyCount(diff, 1000);
            counter.start();*/

        }
    }


    public void getPetitionTallies() {

        Log.d(TAG, "electionId " + electionId);

        Map<String, Long> params = new HashMap<String, Long>();
        params.put("electionId", electionId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getPetitionTallies(params);
        loadingDialog.showDialog();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = jsonObject.getInt("status");

                        if (status == 200) {
                            JSONObject tallyData = jsonObject.getJSONObject("data");

                            yesVotes = tallyData.getInt("yesVotes");
                            noVotes = tallyData.getInt("noVotes");

                            totalVotes = yesVotes + noVotes;
                            int yesVotesPercentage = (int) ((yesVotes * 100.0f) / totalVotes);
                            int noVotesPercentage = (int) ((noVotes * 100.0f) / totalVotes);

                            yesVotesPb.setProgress(yesVotesPercentage);
                            noVotesPb.setProgress(noVotesPercentage);

                            String yesVotesStr = yesVotesPercentage + "%";
                            String noVotesStr = noVotesPercentage + "%";

                            tvYesVotes.setText(yesVotesStr);
                            tvNoVotes.setText(noVotesStr);


                            Log.d(TAG, "votes " + totalVotes);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                t.getMessage();
                loadingDialog.hideDialog();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDate(String date) {
        String[] dateArray = date.split("\\.");
        dateArray = dateArray[0].split("T");

        LocalDate dateObj = LocalDate.parse(dateArray[0], formatter);
        String electedDate = (dateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", " + dateArray[1]);
        return electedDate;
    }

    private void pullAndRefresh() {
        swipeProgress(true);
        new Handler().postDelayed(() -> {
            getPetitionTallies();
            swipeProgress(false);
        }, 3000);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(() -> swipe_refresh.setRefreshing(show));
    }

    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvPetitionElectionCounter.setText("done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = (TimeUnit.MILLISECONDS.toDays(millis)) + " Day "
                    + (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)) + ":")
                    + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)) + ":"
                    + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            tvPetitionElectionCounter.setText(/*context.getString(R.string.ends_in) + " " +*/ hms);
        }
    }
}