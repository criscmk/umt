package com.umt.partyapp.fragment.election.mca;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.umt.partyapp.R;
import com.umt.partyapp.apiresponse.ElectionCandidatureInfoDao;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.model.CandidatureInfoDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class McaCandidatureForm extends Fragment {

    private static final String TAG = "form";

    private Button btnPay, btnUploadCv;
    private ProgressBar progressBar;
    private TextView tvSlogan, tvManifesto;
    private LinearLayout lySlogan, lyManifesto;

    private long electionId;
    private Boolean sloganClicked = false;
    private String oldSlogan;
    private String oldManifesto;
    private String slogan;
    private String manifesto;

    private LoadingDialog loadingDialog;
    private ElectionCandidatureInfoDao electionProfile;
    private CandidatureInfoDto candidatureInfoDto;
    private ElectionDto electionDto;

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private FrameLayout bottom_sheet;

    Bundle bundle;

    public McaCandidatureForm() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_candidature_form, container, false);

        loadingDialog = new LoadingDialog(getActivity());

        progressBar = rootView.findViewById(R.id.mca_submission_progressbar);
        tvManifesto = rootView.findViewById(R.id.tv_manifesto);
        tvSlogan = rootView.findViewById(R.id.tv_slogan);
        lyManifesto = rootView.findViewById(R.id.ly_manifesto);
        lySlogan = rootView.findViewById(R.id.ly_slogan);
        btnPay = rootView.findViewById(R.id.btn_pay);
        btnUploadCv = rootView.findViewById(R.id.btn_upload_cv);

        bottom_sheet = rootView.findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);


        btnPay.setOnClickListener(v -> pay());
        btnUploadCv.setOnClickListener(v -> uploadCv());
        lySlogan.setOnClickListener(v -> {

            showBottomSheetDialog(true);
        });
        lyManifesto.setOnClickListener(v -> {

            showBottomSheetDialog(false);
        });


        initFragment();

        return rootView;
    }

    private void uploadCv() {

    }

    public void initFragment() {
        bundle = getArguments();
        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            electionId = electionDto.getElectionId();

            getElectionProfile();
        }
    }

    public void getElectionProfile() {
        loadingDialog.showDialog();

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().getCandidatureProfile();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();

                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        status = jsonObject.getInt("status");

                        if (status == 200) {
                            JSONObject data = jsonObject.getJSONObject("data");

                            oldSlogan = data.getString("slogan");
                            oldManifesto = data.getString("description");

                            tvSlogan.setText(oldSlogan);
                            tvManifesto.setText(oldManifesto);

                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.d(TAG, "onFailure: " + t.getMessage());
                loadingDialog.hideDialog();

            }
        });
    }


    public void createElectionProfile() {
        String slogan = tvSlogan.getText().toString().trim();
        String description = tvManifesto.getText().toString().trim();

        if (!slogan.isEmpty() && !description.isEmpty()) {
            candidatureInfoDto = new CandidatureInfoDto(slogan, description);

            loadingDialog.showDialog();

            Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().addCandidatureProfile(candidatureInfoDto);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        String res;
                        int status;

                        try {
                            res = response.body().string();
                            JSONObject jsonObject = new JSONObject(res);

                            status = jsonObject.getInt("status");

                            if (status == 200) {
                                JSONObject data = jsonObject.getJSONObject("data");

                                Gson gson = new Gson();
//                            Type type = new TypeToken<ElectionCandidatureInfoDao>() {}.getType();
//                            electionProfile = gson.fromJson(String.valueOf(jsonObject.getJSONObject("data")), type);

                                electionProfile = gson.fromJson(String.valueOf(data), ElectionCandidatureInfoDao.class);

                                submitNomination();

                            } else {

                                Toast.makeText(getContext(), "Network Error. Please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    loadingDialog.hideDialog();
                }
            });
        } else {
            Toast.makeText(getContext(), "Fill all fields", Toast.LENGTH_SHORT).show();
            if (slogan.isEmpty()) {
                tvSlogan.setError("Slogan is require", getActivity().getDrawable(R.drawable.ic_info_primary));
            }

            if (description.isEmpty()) {
                tvManifesto.setError("Slogan is require", getActivity().getDrawable(R.drawable.ic_info_primary));
            }
        }
    }


/*
    public void proceed() {
        progressBar.setVisibility(View.VISIBLE);

        new Handler().postDelayed(() -> {
            progressBar.setVisibility(View.GONE);
            nextFragment();
        }, 4000);
    }*/

    public void submitNomination() {
        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(getContext()).getApi().submitMcaNomination();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {
                    String res;
                    int status;

                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        status = jsonObject.getInt("status");

                        if (status == 200) {
                            JSONObject data = jsonObject.getJSONObject("data");

                            Gson gson = new Gson();
//                            Type type = new TypeToken<ElectionCandidatureInfoDao>() {}.getType();
//                            electionProfile = gson.fromJson(String.valueOf(jsonObject.getJSONObject("data")), type);

//                            electionProfile = gson.fromJson(String.valueOf(data), ElectionCandidatureInfoDao.class);

                            Toast.makeText(getContext(), "You have submitted yourself successfully", Toast.LENGTH_SHORT).show();
                            nextFragment();

                        } else {

                            Toast.makeText(getContext(), "Network Error. Please try again", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();

                loadingDialog.hideDialog();
            }
        });
    }

    public void nextFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        fragmentManager.popBackStack();
        FragmentTransaction transaction = fragmentManager
                .beginTransaction();

        Fragment fragment = new McaCandidatesFragment();
        bundle = new Bundle();
        bundle.putParcelable("election", electionDto);
        fragment.setArguments(bundle);

        transaction.replace(R.id.mca_root_frame, fragment);
        transaction.commit();
    }


    private void showBottomSheetDialog(Boolean sloganActive) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);

        TextInputLayout sloganLayout = view.findViewById(R.id.slogan_layout);
        TextInputLayout manifestoLayout = view.findViewById(R.id.manifesto_layout);

        EditText etSlogan = view.findViewById(R.id.et_slogan);
        EditText etManifesto = view.findViewById(R.id.et_manifesto);


        if (sloganActive) {
            sloganLayout.setVisibility(View.VISIBLE);
            manifestoLayout.setVisibility(View.GONE);
            etSlogan.setText("YES WE CAN");
            etSlogan.requestFocus();

        } else {
            etManifesto.setText("Promise to listen more to your grievances.");
            etManifesto.requestFocus();
        }

        (view.findViewById(R.id.bt_close)).setOnClickListener(view1 -> mBottomSheetDialog.dismiss());

        (view.findViewById(R.id.bt_save)).setOnClickListener(v -> {
            String text = null;
            if (sloganActive) {
                String sloganText = etSlogan.getText().toString().trim();
                if (!sloganText.isEmpty() && !sloganText.equalsIgnoreCase(oldSlogan)) {
                    text = sloganText;
                }
            } else {
                String manifestoText = etManifesto.getText().toString().trim();
                if (!manifestoText.isEmpty() && !manifestoText.equalsIgnoreCase(oldManifesto)) {
                    text = manifestoText;
                }
            }

            if (text != null) {
                save(sloganActive, text);
            }

//            Toast.makeText(getContext(), "Details clicked", Toast.LENGTH_SHORT).show();
            mBottomSheetDialog.dismiss();

        });

        mBottomSheetDialog = new BottomSheetDialog(getContext(), R.style.AppBottomSheetDialogTheme);
        mBottomSheetDialog.setContentView(view);

        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();

        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    public void pay() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.enter_phone_number);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = 500;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.x = 20;

        final EditText phoneNumber = (EditText) dialog.findViewById(R.id.ed_phone_number);
        final Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit_number);
        final Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel_enter_number);

        phoneNumber.setFocusable(true);

        btnSubmit.setOnClickListener(v -> {
            String number = phoneNumber.getText().toString().trim();

            if (number.isEmpty()) {
                Toast.makeText(getContext(), "Please enter Mpesa number", Toast.LENGTH_SHORT).show();
            } else {
                createElectionProfile();
            }

            dialog.dismiss();
        });

        btnCancel.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void save(Boolean sloganActive, String text) {
        if (sloganActive) {

        } else {

        }
        loadingDialog.showDialog();

        new Handler().postDelayed(() -> {
            loadingDialog.hideDialog();

            if (sloganActive) {
                tvSlogan.setText(text);
            } else {
                tvManifesto.setText(text);
            }
        }, 4000);

    }
}