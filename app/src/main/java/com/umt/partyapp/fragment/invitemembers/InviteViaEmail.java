package com.umt.partyapp.fragment.invitemembers;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipDrawable;
import android.support.design.chip.ChipGroup;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umt.partyapp.R;
import com.umt.partyapp.activity.BranchInformation;
import com.umt.partyapp.activity.BranchInformationActivity;
import com.umt.partyapp.activity.InviteMembers;
import com.umt.partyapp.model.ContactInvitationDisplay;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.LimitColumn;
import com.wafflecopter.multicontactpicker.MultiContactPicker;
import com.wafflecopter.multicontactpicker.RxContacts.PhoneNumber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class InviteViaEmail extends Fragment {

    private static final String TAG = "InviteViaEmail";

    public static final int CONTACT_PICKER_REQUEST = 991;
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private ArrayList<ContactResult> results = new ArrayList<>();
    private HashMap<Integer,String> selectedContacts = new HashMap<>();
    private InviteMembers inviteMembers;
    private AppCompatEditText phone;
    private ChipGroup chipGroup;
    private ProgressBar progressBar;
    private Button btnSendInvites;


    public InviteViaEmail() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inviteMembers = (InviteMembers) getActivity();
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_invite_via_email, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton btnFetchContacts = view.findViewById(R.id.btn_load_contacts);
        phone = view.findViewById(R.id.phone);
        progressBar = view.findViewById(R.id.invite_progress_bar);
        btnSendInvites = view.findViewById(R.id.btn_submit_invite_emails);

        chipGroup = view.findViewById(R.id.email_user);

        btnFetchContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadContacts();
            }
        });

        btnSendInvites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvites();
            }
        });


        phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("Done", "Enter pressed");

                    String chipText = v.getText().toString();

                    if (chipText.length() > 0) {
                        updateChips(new ContactInvitationDisplay("","", chipText));
                        phone.getText().clear();
                    }

                }
                return false;
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

    }


    public void updateContactChipGroup(ArrayList<ContactInvitationDisplay> updatedContacts){

        for (int i = 0; i <= updatedContacts.size() - 1; i++) {

            updateChips(updatedContacts.get(i));
        }
    }

    public void updateChips(ContactInvitationDisplay contactInvitationDisplay){
        int currentIndex = chipGroup.getChildCount();
        //Update selectedContact

        //Check if contact already added
        boolean contactExists = selectedContacts.containsValue(contactInvitationDisplay.getEmail());

        if(!contactExists){
            String chipDisplay = contactInvitationDisplay.getDisplayName()==""?contactInvitationDisplay.getEmail():
                    contactInvitationDisplay.getDisplayName()+" : "+contactInvitationDisplay.getEmail();

            selectedContacts.put(currentIndex,contactInvitationDisplay.getEmail());
            Chip myChip = addNewChip(chipDisplay, chipGroup);
            chipGroup.addView(myChip);
        }

    }

    private void sendInvites() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, selectedContacts.toString());
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private Chip addNewChip(String text, ChipGroup chipGroup) {
        Chip chip = new Chip(getContext());
        chip.setText(text);
        chip.setCloseIconVisible(true);
        chip.setChipIconTintResource(R.color.grey_20);
        chip.setClickable(false);
        chip.setCheckable(false);
        chip.setChipBackgroundColorResource(R.color.umt_white);
        chip.setChipIcon(getResources().getDrawable(R.drawable.ic_email));
        chip.setIconStartPaddingResource(R.dimen.spacing_medium);

        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipGroup.removeView(chip);
                int selectedContactIndex = (v.getId())-1;
                selectedContacts.remove(selectedContactIndex);
            }
        });
        return chip;

    }

    public void loadContacts() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            new MultiContactPicker.Builder(inviteMembers) //Activity/fragment context
                    .theme(R.style.MyCustomPickerTheme) //Optional - default: MultiContactPicker.Azure
                    .hideScrollbar(false) //Optional - default: false
                    .showTrack(true) //Optional - default: true
                    .searchIconColor(Color.WHITE) //Option - default: White
                    .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
                    .handleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary)) //Optional - default: Azure Blue
                    .bubbleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary)) //Optional - default: Azure Blue
                    .bubbleTextColor(Color.WHITE) //Optional - default: White
                    .setTitleText("Select Contacts") //Optional - default: Select Contacts
                    .setSelectedContacts(results) //Optional - will pre-select contacts of your choice. String... or List<ContactResult>
                    .setLoadingType(MultiContactPicker.LOAD_ASYNC) //Optional - default LOAD_ASYNC (wait till all loaded vs stream results)
                    .limitToColumn(LimitColumn.EMAIL) //Optional - default NONE (Include phone + email, limiting to one can improve loading time)
                    .setActivityAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                            android.R.anim.fade_in,
                            android.R.anim.fade_out) //Optional - default: No animation overrides
                    .showPickerForResult(CONTACT_PICKER_REQUEST);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(inviteMembers,
                    Manifest.permission.READ_CONTACTS)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Read Contacts permission");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setMessage("Please enable access to contacts.");
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(
                                new String[]
                                        {Manifest.permission.READ_CONTACTS}
                                , PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                });
                builder.show();
            } else {
                ActivityCompat.requestPermissions(inviteMembers,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST_READ_CONTACTS);
            }

//                Toast.makeText(getApplicationContext(), "Remember to go into settings and enable the contacts permission.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts();
                } else {
                    Toast.makeText(getContext(), "You have disabled a contacts permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    public void onReceiveContactsResults(ArrayList<ContactResult> results) {
        if (results.size() > 0) {

            Log.d("results", results.toString());
            Log.d("results size", String.valueOf(results.size()));

            ArrayList<ContactInvitationDisplay> updatedContacts = new ArrayList<>();
            for (int i = 0; i <= results.size() - 1; i++) {

                List<String> selectedEmails = results.get(i).getEmails();
                Log.d("list", selectedEmails.size()+"");
                for (int j = 0;j<selectedEmails.size();j++){

                    ContactInvitationDisplay contactInvitationDisplay = new ContactInvitationDisplay(results.get(i).getDisplayName(),"", selectedEmails.get(j));
                    updatedContacts.add(contactInvitationDisplay);

                }

            }
            updateContactChipGroup(updatedContacts);
        }

    }

}