package com.umt.partyapp.fragment.election.womanrep;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umt.partyapp.R;
import com.umt.partyapp.activity.VoteActivity;
import com.umt.partyapp.adapters.ElectionResultsAdapter;
import com.umt.partyapp.apiresponse.ElectionDto;
import com.umt.partyapp.apiresponse.ElectionResultsResponseDto;
import com.umt.partyapp.retrofitinterface.RetrofitAuthenticatedClient;
import com.umt.partyapp.util.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.O)
public class WomanRepElectionTally extends Fragment {

    private static final String TAG = "woman_rep_tally";

    TextView tvTallyCounterMsg, tvDays, tvHours, tvMinutes, tvSeconds, pageTitle, electionLevelPlaceName;
    long diff;
    long oldLong;
    long newLong;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    private ElectionResultsAdapter adapter;
    private Long electionId;
    private ElectionDto electionDto;
    private LoadingDialog loadingDialog;
    private CoordinatorLayout baseLayout;
    private VoteActivity voteActivity;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    NumberFormat numberFormat = new DecimalFormat("00");

    public WomanRepElectionTally() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mca_election_tally, container, false);

        voteActivity = (VoteActivity) getActivity();

        recyclerView = rootView.findViewById(R.id.list_candidates_tally);
        tvTallyCounterMsg = rootView.findViewById(R.id.tally_timer_msg);
        pageTitle = rootView.findViewById(R.id.election_stage);
        pageTitle = rootView.findViewById(R.id.tv_place_name);

        baseLayout = rootView.findViewById(R.id.voting_timer_base_layout);

        tvDays = baseLayout.findViewById(R.id.tv_days);
        tvHours = baseLayout.findViewById(R.id.tv_hours);
        tvMinutes = baseLayout.findViewById(R.id.tv_minutes);
        tvSeconds = baseLayout.findViewById(R.id.tv_seconds);

        adapter = new ElectionResultsAdapter(voteActivity);
        loadingDialog = new LoadingDialog(voteActivity);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        initFragment();

        return rootView;
    }

    public void initFragment() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            electionDto = bundle.getParcelable("election");
            electionId = electionDto.getElectionId();

            String dateTo = electionDto.getEndDate();
            String placeName = electionDto.getLevelName() + " County";
            Log.d(TAG, "initFragment: " + electionDto.getLevelName());

            pageTitle.setText(R.string.senatorial_election_results);
            electionLevelPlaceName.setText(placeName);

            Log.d(TAG, "initFragment: " + electionId);
            Log.d(TAG, "initFragment: " + electionDto.getCurrentLeader());

            diff = voteActivity.getDateTimeDiff(null, dateTo);
            Log.d(TAG, "initFragment: " + diff);
            if (diff == 0) {
                baseLayout.setVisibility(View.GONE);
                tvTallyCounterMsg.setVisibility(View.GONE);
            }

            MyCount counter = new MyCount(diff, 1000);
            counter.start();

            getElectionTally();
        }
    }

    public void getElectionTally() {
        loadingDialog.showDialog();

        Map<String, Long> params = new HashMap<>();
        params.put("electionId", electionId);

        Call<ResponseBody> call = RetrofitAuthenticatedClient.getmInstance(voteActivity).getApi().getWomanRepElectionResults(params);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingDialog.hideDialog();
                if (response.isSuccessful()) {

                    String res;
                    int status;

                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);

                        Log.d(TAG, "onResponse: " + jsonObject.toString());
                        status = Integer.parseInt(jsonObject.getString("status"));

                        if (status == 200) {
                            JSONArray data = jsonObject.getJSONArray("data");
                            Log.d(TAG, "data " + data.toString());
                            Gson gson = new Gson();

                            Type type = new TypeToken<List<ElectionResultsResponseDto>>() {
                            }.getType();
                            List<ElectionResultsResponseDto> results = gson.fromJson(String.valueOf(data), type);
                            adapter.addAll(results);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                loadingDialog.hideDialog();

            }
        });

    }

    public class MyCount extends CountDownTimer {
        MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

//            txtElectionCounter.setText("done!");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished) % 365;
            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;

            tvDays.setText(numberFormat.format(days));
            tvHours.setText(numberFormat.format(hours));
            tvMinutes.setText(numberFormat.format(minutes));
            tvSeconds.setText(numberFormat.format(seconds));
        }
    }
}