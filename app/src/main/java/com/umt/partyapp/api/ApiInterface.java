package com.umt.partyapp.api;


import com.umt.partyapp.apiresponse.BranchListResponseDto;
import com.umt.partyapp.apiresponse.BranchMembersResponseDao;
import com.umt.partyapp.apiresponse.ElectionResponseDto;
import com.umt.partyapp.model.CandidatureInfoDto;
import com.umt.partyapp.model.CreateBranchDto;
import com.umt.partyapp.model.BranchElectionNominationDto;
import com.umt.partyapp.model.BranchElectionPetitionSupportVote;
import com.umt.partyapp.model.BranchElectionReasonDto;
import com.umt.partyapp.model.JoinBranchDto;
import com.umt.partyapp.model.SmsInviteContactsDto;
import com.umt.partyapp.model.UserRegDto;
import com.umt.partyapp.model.VerifyCodeDto;
import com.umt.partyapp.model.ForgortPasswordDto;
import com.umt.partyapp.model.ResendCodeDto;
import com.umt.partyapp.model.ResetPasswordDto;
import com.umt.partyapp.model.VoteDto;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("api/user/validate/identity/")
    Call<ResponseBody> getUserIebcData(@Query("idNumber") String IdNumber);

    @GET("oauth/token")
    Call<ResponseBody> getClientAccessToken(@Query("grant_type") String grantType, @Query("client_id") String clientId, @Query("client_secret") String clientSecret);


    @Headers("Content-Type: application/json")
    @POST("api/user/register")
    Call<ResponseBody> RegisterUser(@Body UserRegDto body);

    @Headers("Content-Type: application/json")
    @POST("api/phoneverification/validate")
    Call<ResponseBody> VerifyCode(@Body VerifyCodeDto body);

    //resend verification code
    @Headers("Content-Type: application/json")
    @POST("api/phoneverification/resend")
    Call<ResponseBody> ResendCode(@Body ResendCodeDto body);

    @Headers("Content-Type: application/json")
    @POST("api/password/request/reset")
    Call<ResponseBody> RequestPasswordReset(@Body ForgortPasswordDto forgortPasswordDto);

    @Headers("Content-Type: application/json")
    @POST("api/password/reset")
    Call<ResponseBody> ResetPassword(@Body ResetPasswordDto resetPasswordDto);

    @GET("api/branch/all")
    Call<BranchListResponseDto> getWardBranches(@Query("pageSize") int pageSize,
                                                @Query("currentPage") int currentPage);

    @POST("api/branch/create")
    Call<ResponseBody> CreateBranch(@Body CreateBranchDto createBranchDto);

    @POST("api/branch/join")
    Call<ResponseBody> JoinBranch(@Body JoinBranchDto joinBranchDto);

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("oauth/token")
    Call<ResponseBody> UserLogin(@Field("username") String username,
                                 @Field("password") String password,
                                 @Field("client_id") String client_id,
                                 @Field("client_secret") String client_secret,
                                 @Field("grant_type") String grant_type);

    @GET("api/branch/members")
    Call<BranchMembersResponseDao> fetchBranchMembers(@Query("branchId") int branchId,
                                                      @Query("pageSize") int pageSize,
                                                      @Query("currentPage") int currentPage);

    @POST("api/branch/invite")
    Call<ResponseBody> sendInvitations(@Body SmsInviteContactsDto smsInviteContactsDto);

    /*
    ===================Branch election end points=============================
    * */

    @GET("api/branch-election/status")
    Call<ResponseBody> getBranchElection();

    @POST("api/branch-election/call")
    Call<ResponseBody> callForElection(@Body BranchElectionReasonDto electionReasonDto);

    @POST("api/branch-election-petition")
    Call<ResponseBody> supportElectionPetition(@Body BranchElectionPetitionSupportVote electionPetitionSupportVote);

    @POST("api/branch-coordinator-submission")
    Call<ResponseBody> submitNomination(@Body BranchElectionNominationDto branchElectionNominationDto);

    @GET("api/branch-election-petition")
    Call<ResponseBody> getPetitionTallies(@QueryMap Map<String, Long> params);

    @GET("api/branch-coordinator-submission")
    Call<ResponseBody> getCandidates(@QueryMap Map<String, Long> params);

    @POST("api/branch-election-voting")
    Call<ResponseBody> vote(@Body VoteDto voteDto);

    @GET("api/branch-election-voting")
    Call<ResponseBody> getElectionResults(@QueryMap Map<String, Long> params);

    //=============================END============================================

    //====================General election endpoints=============================
    @GET("api/election")
    Call<ResponseBody> getCandidatureProfile();

    @POST("api/election")
    Call<ResponseBody> addCandidatureProfile(@Body CandidatureInfoDto body);

    @PUT("api/election")
    Call<ResponseBody> updateCandidateProfile(@Body CandidatureInfoDto body);

    //==========================END======================================================

    //========================Ward election end points============================

    @GET("api/mca-election/status")
    Call<ElectionResponseDto> getWardElection();

    @POST("api/mca-candidate-submission")
    Call<ResponseBody> submitMcaNomination();

    @GET("api/mca-candidate-submission")
    Call<ResponseBody> getMcaCandidates();

    @POST("api/mca-election-vote")
    Call<ResponseBody> mcaVote(@Body VoteDto voteDto);

    @GET("api/mca-election-vote")
    Call<ResponseBody> getMcaElectionResults(@QueryMap Map<String, Long> params);


    //==========================END===============================================


    //===========================Mp election end points===========================

    @GET("api/mp-election/status")
    Call<ElectionResponseDto> getMpElection();

    @POST("api/mp-candidate-submission")
    Call<ResponseBody> submitMpNomination();

    @GET("api/mp-candidate-submission")
    Call<ResponseBody> getMpCandidates();

    @POST("api/mp-election-vote")
    Call<ResponseBody> mpVote(@Body VoteDto voteDto);

    @GET("api/mp-election-vote")
    Call<ResponseBody> getMpElectionResults(@QueryMap Map<String, Long> params);


    //=======================END=================================================


    //===========================Woman Rep election end points===========================

    @GET("api/wr-election/status")
    Call<ElectionResponseDto> getWomanRepElection();

    @POST("api/wr-candidate-submission")
    Call<ResponseBody> submitWomanNomination();

    @GET("api/wr-candidate-submission")
    Call<ResponseBody> getWomanRepCandidates();

    @POST("api/wr-election-vote")
    Call<ResponseBody> womanRepVote(@Body VoteDto voteDto);

    @GET("api/wr-election-vote")
    Call<ResponseBody> getWomanRepElectionResults(@QueryMap Map<String, Long> params);

    //=======================END=======================================================

    //===========================Senatorial election end points===========================

    @GET("api/senator-election/status")
    Call<ElectionResponseDto> getSenatorialElection();

    @POST("api/senator-candidate-submission")
    Call<ResponseBody> submitSenatorialNomination();

    @GET("api/senator-candidate-submission")
    Call<ResponseBody> getSenatorialCandidates();

    @POST("api/senator-election-vote")
    Call<ResponseBody> senatorialVote(@Body VoteDto voteDto);

    @GET("api/senator-election-vote")
    Call<ResponseBody> getSenatorElectionResults(@QueryMap Map<String, Long> params);

    //=======================END=======================================================

    //===========================Gubernatorial election end points===========================

    @GET("api/governor-election/status")
    Call<ElectionResponseDto> getGubernatorialElection();

    @POST("api/governor-candidate-submission")
    Call<ResponseBody> submitGubernatorialNomination();

    @GET("api/governor-candidate-submission")
    Call<ResponseBody> getGubernatorialCandidates();

    @POST("api/governor-election-vote")
    Call<ResponseBody> gubernatorialVote(@Body VoteDto voteDto);

    @GET("api/governor-election-vote")
    Call<ResponseBody> getGubernatorialElectionResults(@QueryMap Map<String, Long> params);

    //=======================END=======================================================

    //===========================President election end points===========================

    @GET("api/pl-election/status")
    Call<ElectionResponseDto> getPresidentialElection();

    @POST("api/pl-candidate-submission")
    Call<ResponseBody> submitPresidentialNomination();

    @GET("api/pl-candidate-submission")
    Call<ResponseBody> getPresidentialCandidates();

    @POST("api/pl-election-vote")
    Call<ResponseBody> presidentialVote(@Body VoteDto voteDto);

    @GET("api/pl-election-vote")
    Call<ResponseBody> getPresidentialElectionResults(@QueryMap Map<String, Long> params);

    //=======================END=======================================================
}
