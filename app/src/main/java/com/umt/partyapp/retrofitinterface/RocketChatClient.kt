package com.umt.partyapp.retrofitinterface

import chat.rocket.common.util.PlatformLogger
import chat.rocket.core.RocketChatClient
import chat.rocket.core.createRocketChatClient
import com.umt.partyapp.activity.ChatActivity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object RocketChatClientService {

    private fun RocketChatClientService(): RocketChatClient {
        val logger = object : PlatformLogger {
            override fun debug(s: String) {
                println(s)
            }

            override fun info(s: String) {
                println(s)
            }

            override fun warn(s: String) {
                println(s)
            }
        }

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build()


        return createRocketChatClient {
            httpClient = okHttpClient
            restUrl = "http://18.188.5.124:3000/"
            userAgent = "umt-chat"
            tokenRepository = ChatActivity.SimpleTokenRepository()
            platformLogger = logger
        }



    }

    val clientInstance: RocketChatClient by lazy {
        RocketChatClientService()
    }
}