package com.umt.partyapp.retrofitinterface;


import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.umt.partyapp.api.ApiInterface;
import com.umt.partyapp.sessionmanager.Sessionmangementsharedpref;
import com.umt.partyapp.util.Constants;
import com.umt.partyapp.util.TokenManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAuthenticatedClient {

    private static final String BaseUrl = "http://06fb49a82df4.ngrok.io/";
    private static RetrofitAuthenticatedClient mInstance;
    private Retrofit retrofit;

    private RetrofitAuthenticatedClient(Context context) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {

                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();
                                Log.d("intercept", "invoked");
                                //Get Access Token
                                String accessToken = new TokenManager(context).getValidAccessToken(context);


                                if (accessToken != null) {
                                    String authHeader = "bearer " + accessToken;

                                    Request.Builder requestBuilder = original.newBuilder()
                                            .addHeader("Authorization", authHeader)
                                            .method(original.method(), original.body());

                                    Request authenticatedRequest = requestBuilder.build();

                                    return chain.proceed(authenticatedRequest);

                                } else {
                                    //Request new one and update

                                    //get refreshToken || Log Out user
                                    Sessionmangementsharedpref sessionmangementsharedpref = new Sessionmangementsharedpref(context);
                                    String refreshToken = sessionmangementsharedpref.getRefreshToken();

                                    if (refreshToken != null) {
                                        synchronized (getmInstance(context)) {
                                            boolean status = refreshAccessToken(refreshToken, context);

                                            if (status) {
                                                String authHeader = "bearer " + new TokenManager(context).getValidAccessToken(context);
                                                Log.d("oo", "invoke 1");
                                                Request.Builder requestBuilder = original.newBuilder()
                                                        .addHeader("Authorization", authHeader)
                                                        .method(original.method(), original.body());

                                                Request authenticatedRequest = requestBuilder.build();

                                                return chain.proceed(authenticatedRequest);
                                                //return chain.proceed(requestBuilder.build());
                                            } else {
                                                sessionmangementsharedpref.logout();
                                                return new Response.Builder()
                                                        .code(401)
                                                        .body(ResponseBody.create(null, ""))
                                                        .protocol(Protocol.HTTP_2)
                                                        .message("Unauthorized")
                                                        .request(chain.request())
                                                        .build();
                                            }
                                        }

                                    } else {
                                        sessionmangementsharedpref.logout();
                                        return new Response.Builder()
                                                .code(401)
                                                .body(ResponseBody.create(null, ""))
                                                .protocol(Protocol.HTTP_2)
                                                .message("Unauthorized")
                                                .request(chain.request())
                                                .build();
                                    }


                                }


                            }
                        }
                )
                .build();


        retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitAuthenticatedClient getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RetrofitAuthenticatedClient(context);
        }

        return mInstance;
    }


    public ApiInterface getApi() {
        return retrofit.create(ApiInterface.class);
    }

    public Boolean refreshAccessToken(String refreshToken, Context context) {
        boolean status = false;
        RequestBody formBody = new FormBody.Builder()
                .add("client_id", Constants.CLIENT_ID)
                .add("client_secret", Constants.CLIENT_SECRET)
                .add("grant_type", Constants.REFRESH_TOKEN_GRANT_TYPE)
                .add("refresh_token", refreshToken)
                .build();

        Request tokenRequest = new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(BaseUrl + "oauth/token")
                .post(formBody)
                .build();


        try {
            Response response = new OkHttpClient().newBuilder().build().newCall(tokenRequest).execute();

            if (response.isSuccessful()) {
                String s = response.body().string();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(s);

                    Log.d("ooo", jsonObject.toString());
                    String access_token = jsonObject.getString("access_token");

                    Sessionmangementsharedpref sessionmangementsharedpref = new Sessionmangementsharedpref(context);

                    sessionmangementsharedpref.updateAccessToken(jsonObject.getString("access_token"), jsonObject.getInt("expires_in"));

                    status = true;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return status;
    }
}
