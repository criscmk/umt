package com.umt.partyapp.service

import android.app.IntentService
import android.content.Intent
import android.os.*
import chat.rocket.common.RocketChatException
import chat.rocket.common.model.ServerInfo
import chat.rocket.common.util.PlatformLogger
import chat.rocket.core.RocketChatClient
import chat.rocket.core.createRocketChatClient
import chat.rocket.core.internal.realtime.*
import chat.rocket.core.internal.realtime.socket.connect
import chat.rocket.core.internal.realtime.socket.model.State
import chat.rocket.core.internal.rest.chatRooms
import chat.rocket.core.internal.rest.loginWithEmail
import com.umt.partyapp.activity.ChatActivity
import com.umt.partyapp.chat.Callback
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import android.os.Handler as Handler
import com.umt.partyapp.handlers.ChatHandler as ChatHandler

class ChatListenerService : IntentService("ChatListenerService") {

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onHandleIntent(intent: Intent?) {


        var handler = intent?.getParcelableExtra<Messenger>("handler")
        //var handler = messenger.

        val logger = object : PlatformLogger {
            override fun debug(s: String) {
                println(s)
            }

            override fun info(s: String) {
                println(s)
            }

            override fun warn(s: String) {
                println(s)
            }
        }

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build()

        val client = createRocketChatClient {
            httpClient = okHttpClient
            restUrl = "http://18.188.5.124:3000/"
            userAgent = "umt-chat"
            tokenRepository = ChatActivity.SimpleTokenRepository()
            platformLogger = logger
        }



        val job = GlobalScope.launch(Dispatchers.IO) {
            val token = client.loginWithEmail("muthuicris@gmail.com", "Umt-##9876!chat\$")
//                logger.debug("Token: userId = ${token.userId} - authToken = ${token.authToken}")

            launch {
                val statusChannel = Channel<State>()
                client.addStateChannel(statusChannel)
                for (status in statusChannel) {
//                        logger.debug("CHANGING STATUS TO: $status")
                    if (status is State.Connected) {
                        logger.debug("Connected!")
                        client.subscribeSubscriptions { _,_ -> }
                        client.subscribeRooms { _, _ -> }
                        client.subscribeUserData { _, _ -> }
                        client.subscribeActiveUsers { _, _ -> }
                        client.subscribeTypingStatus("GENERAL") { _, _ -> }
                    }
                }
            }

            launch {


                for (room in client.roomsChannel) {

                    val msg = Message()
                    msg.obj = room.data
                    msg.what = 1
                    handler?.send(msg)

                    logger.debug("Room***: $room")
                    //println("last "+room.data.lastMessage?.message)
                }
            }


            launch {
                for (subscription in client.subscriptionsChannel) {
//                        logger.debug("Subscription: $subscription")
                }
            }

            launch {
                for (userData in client.userDataChannel) {
                    logger.debug("User Data: $userData")
                }
            }

            launch {
                for (activeUsers in client.activeUsersChannel) {
                    logger.debug("Active users: $activeUsers")
                }
            }


            launch {
                for (typingStatus in client.typingStatusChannel) {
                        logger.debug("Typing status: $typingStatus")
                }
            }


            launch {
                //            delay(10000)
//            client.setTemporaryStatus(UserStatus.Online())
//            delay(2000)
//            client.setDefaultStatus(UserStatus.Away())
//            client.setTypingStatus("GENERAL", "testing", true)
            }

            client.connect()
//                logger.debug("PERMISSIONS: ${client.permissions()}")

/*        client.sendMessage(roomId = "GENERAL",
                text = "Sending message from SDK to #general and @here with url https://github.com/RocketChat/Rocket.Chat.Kotlin.SDK/",
                alias = "TestingAlias",
                emoji = ":smirk:",
                avatar = "https://avatars2.githubusercontent.com/u/224255?s=88&v=4")
*/

            val rooms = client.chatRooms()
//                logger.debug("CHAT ROOMS: $rooms")
            val room = rooms.update.lastOrNull { room -> room.id.contentEquals("GENERAL") }
//                logger.debug("ROOM w: $room")
//                logger.debug("MESSAGES v: ${room?.messages()}")
//                logger.debug("HISTORY m: ${room?.history()}")
        }

        // simple old callbacks
        client.serverInfo(object : Callback<ServerInfo> {
            override fun onSuccess(data: ServerInfo) {
//                    logger.debug("SERVER INFO: $data")
            }

            override fun onError(error: RocketChatException) {
                error.printStackTrace()
            }
        })

        runBlocking {
            //delay(10000)
            //client.disconnect()
            job.join()
            //delay(2000)
            //  exitProcess(-1)
        }
    }

    private fun RocketChatClient.serverInfo(callback: Callback<ServerInfo>) {

    }
    
    //some comment here

}
